$("#firtsload").addClass("firsthide");
$("body").removeClass("allblur");

function redirect(url){
	window.location=url;
}
function loadDate(){

	$(".tgl").each(function(){
	  $(this).datepicker({
			changeMonth: true,
				changeYear: true,
				dateFormat: "yy-mm-dd"
		});
	});
}

function loadTime(){
	
	$(".jam").each(function(){
	$(this).timepicker({
					showPeriodLabels: false,
			});
	});
	
}
function loadUang(){
	
	$('.uang').bind('blur', function(e) {
    	$(this).val(idr($(this).val()));    
	});     
	$('.uang').bind('click', function(e) {
    	$(this).select();    
	});          
}
function siapload(){
	
	$('*').tooltip({
		track: true	
	});
	
	//modal
	$('.linkbox').bind('click', function(e) {
       e.preventDefault();
		var th=this;
		modaler(th);
	});   
	loadUang();
	loadDate();
	loadTime();
}

  var hideLoad=function(ider){
	if(ider==undefined){
			ider="mloading";	
		}
	$('#'+ider).slideUp(500);
	
	}
	
	var showLoad=function(classDiv,teks,ider){
		if(ider==undefined){
			ider="mloading";	
		}
		$('#'+ider).remove();
		if(teks==undefined){
			teks="Silahkan Tunggu, Server Sedang Memproses Data...";	
		}
		var strlod='<div id="'+ider+'" class="mloadings"><span class="fa fa-spinner fa-spin fa-speed-2x" style="font-size: 40px;color: #009FFF;"></span><br/> '+teks+'</div>';
		$("."+classDiv).prepend(strlod);
		$('#'+ider).slideDown(500);
	}
	var modaler=function(th){
				$('.modals').bPopup({
				contentContainer:'.conteks',
				speed: 450,
				modalClose: false,
				transition: 'slideIn',
				loadUrl: $(th).attr('href'),
				onOpen: function() {
					showLoad("grid-content");
					$('#title-modal').html($(th).attr('label'));	
				},
				loadCallback:function(){
						siapload();
					hideLoad();
					}
			});
	return false;
		}
		


function closed(){
	$('.modals').bPopup().close();
hideLoad();
}

function refreshed(cls,prm){
		var clstbl=(cls) ?  cls : 'grid-tabel';
		var clsprm=(prm) ?  prm : 'grid-params';
		
				$("."+clstbl).flexOptions({newp: 1,params:$('.'+clsprm).serializeArray()}).flexReload();
				
}
function uploadered(){	
	var uploaders = new qq.FileUploader({
					element: document.getElementById('filefoto'),
					action: 'model/file_uploader.php',			
					debug: true,
					onComplete: function(id, fileName, data){
						if(data['success']){
						var n='files/'+data['nama'];
						$('#foto').val(n);
						$('#srcfoto').attr('src',n)
						}
					}
				});			
	}
function del_data(urls,th,clstbl,clsprm){
	var cf=confirm("Apakah Anda Yakin Akan Menghapus Data Ini ?");
	if(cf){
			$(th).children().attr('src','assets/images/icon/load.gif');				
			$.ajax({
				url:urls,
				type:'post',
				success:function(sal){
					refreshed(clstbl,clsprm);
				}
			});
	}else{
		return false;
	}
}

function del_temp(urls,th,afterLoad){
	var cf=confirm("Apakah Anda Yakin Akan Menghapus Data Ini ?");
	if(cf){
		$.ajax({
			url:urls,
			type:'post',
			success:function(sal){
				if(afterLoad!=undefined){
					afterLoad();
				}
			}
		});
	}else{
		return false;
	}
}

// keuangan saldo
function get_saldo_akun(el){
	var thisval=$(el).val();
	var thiscontent=$(el).attr("id-saldo");
	$("#"+thiscontent).html('<i>loading saldo...</i>');
	$.ajax({
		url:'?act=keuangan&do=get_saldo_akun',
		type:'post',
		data:$.param({"kode":thisval}),
		success:function(sal){
			$("#"+thiscontent).html(idr(sal));
		}
	});
}

function saveData(idform,url,clstbl,clsprm,afterLoad,addlook){
	$('#'+idform+" [type='submit']").attr("disabled","disabled");
	var orihtml=$('#'+idform+" [type='submit']").html();
	$('#'+idform+" [type='submit']").html('<span class="fa fa-spinner fa-spin fa-speed-2x"></span>loading...');
	if(addlook!=true){
		showLoad("grid-content","Silahkan Tunggu, Sedang Menyimpan Data...");
	}
	$.ajax({
		url:url,
		type:'post',
		data:$('#'+idform).serializeArray(),
		success:function(data_result){		
				$('#'+idform+" [type='submit']").removeAttr("disabled");
				$('#'+idform+" [type='submit']").html(orihtml);
				if(addlook!=true){
						hideLoad();
					if(data_result!="failed"){
						$('.modals').bPopup().close();
						$('.conteks').html('');
						refreshed(clstbl,clsprm);
					}
				}
				if(afterLoad!=undefined){
					afterLoad(data_result);
				}
		}
	});
}


//format rupiah
function rupiah(angka){
    var rev     = parseInt(angka,10).toString().split('').reverse().join('');
    var rev2    = '';
    for(var i = 0; i < rev.length; i++){
        rev2  += rev[i];
        if((i + 1) % 3 === 0 && i !== (rev.length - 1)){
            rev2 += '.';
        }
    }
    return rev2.split('').reverse().join('');
}
//
// column width persen ke angka
function parsen(percent){
	screen_res = $('.content').width();
	col = parseInt((percent*(screen_res/100)));
	
	if (percent != 100){
		width = col-18;
	}else{
		width = col;
	}
	return width;
	//alert(width);
} 


// set cookie sideMenu On/Off
var sid=($.cookie("side")==0) ? $.cookie("side") : 1;

function setonof(val){
	// var ww=$(document).width();
	
	// if(ww<=942){
	// 	val=0;	
	// }

	if(val==1){
	
	sideon();
	switon();
	$(".nadnav").addClass("son");
	
	}else{
		
	$(".nadnav").removeClass("son");
	sideoff();
	switoff();
	}
	$.cookie("side",sid);	

}
function setSideOn(){
	// var ww=$(document).width();

	// if(ww>942){
		if(sid==0){
			sid=1;
		}else{
			sid=0;	
		}	
		setonof(sid);
	// }
}
function sideon(){   
if($("#bodi").hasClass("sideon")==false){
$("#bodi").addClass("sideon");
$(thisaktif).next('.sub-ul-menu').slideDown(500);
}
}
function sideoff(){   
if(sid==0){
if($("#bodi").hasClass("sideon")==true){
	$("#bodi").removeClass("sideon");
	$('.sub-ul-menu').hide(100);
}
}
}

var brk=0;
var thisaktif=null;
$(document).ready(function(e) {
						   
	// aktifkan side ketika load
	var ww=$(document).width();

	if(ww<=942){
		sid=0;
	}
	setonof(sid);
	// animasi shorcut
	$(".horz-box li").hide();
	var eeya=300;
	$(".horz-box li").each(function(){
		$(this).delay(eeya);
		eeya+=80;
		$(this).fadeIn(400).effect("bounce",800);	
	});
	
	$( ".content" ).hide();
	
	$( ".title-content" ).hide();
	$( ".content" ).fadeIn(1000);
	$( ".title-content" ).slideDown(500);
	
	// left menu open/close on hover
	if(ww>=767){
	
		$( ".left-menu" )
		  .mouseenter(function() {
			sideon();
		})
		  .mouseleave(function() {
			sideoff();
		});
	}
	// toogle menu
	 $('.sub-ul-menu li a').click(function(){
				$('.toogle-first li').removeClass('subtoogle-active');
				$(this).parent('li').addClass("subtoogle-active");
	});
	// toogle menu
	 $('.sub-menu-tiga>a').click(function(){
	 	if($(this).parent().find('.sub-ul-menu-tiga').is(":hidden")){
			$('.sub-ul-menu-tiga').hide(300);
	 	}
		 	$(this).parent().find('.sub-ul-menu-tiga').toggle(300);
	 	
	});
	// toogle menu
	 $('.sub-menu-empat>a').click(function(){
	 	if($(this).parent().find('.sub-ul-menu-empat').is(":hidden")){
			$('.sub-ul-menu-empat').hide(300);
	 	}
		 	$(this).parent().find('.sub-ul-menu-empat').toggle(300);
	 	
	});
	// toogle menu
	 $('.sub-menu-lima>a').click(function(){
	 	if($(this).parent().find('.sub-ul-menu-lima').is(":hidden")){
			$('.sub-ul-menu-lima').hide(300);
	 	}
		 	$(this).parent().find('.sub-ul-menu-lima').toggle(300);
	 	
	});
	// toogle menu
	 $('.sub-menu-enam>a').click(function(){
	 	if($(this).parent().find('.sub-ul-menu-enam').is(":hidden")){
			$('.sub-ul-menu-enam').hide(300);
	 	}
		 	$(this).parent().find('.sub-ul-menu-enam').toggle(300);
	 	
	});
 $('.accordion-heading').click(function(){
		var brks=$(this).children().children('.label-menu').html();
		$('.sub-ul-menu').slideUp();
		$('.accordion-heading').removeClass('toogle-active');
		$('.toogle-first').removeClass('tf-active');
		
		if(brk!=brks){
			thisaktif=this;
			$(this).next('.sub-ul-menu').slideDown(500);
			$(this).addClass('toogle-active');
			$(this).parent('.toogle-first').addClass('tf-active');
			brk=brks;
		}else{
			$(this).next('.sub-ul-menu').slideUp(500);
			brk='';
			$(this).addClass('toogle-active');
			$(this).parent('.toogle-first').addClass('tf-active');
			thisaktif=null;
		}
	});
	$(".tf-active").find(".accordion-toggle").click();	
	if(sid==0){
	$(".sub-ul-menu").hide();
	}
});


$(document).ready(function(){
	$(".dropdivmenu").each(function(){
		$(this).click(function(){
			var val=$(this).attr('id');
		
			if(val=="open"){
				
				$(this).next().hide();
				$(this).attr('id', '');	
			}else{
				$(".divmenu").hide();
				$(".dropdivmenu").attr('id', '');
				$(this).next().show();
				$(this).attr('id', 'open');
			}	
		});
	});	
	//Mouseup textarea false
	$(".divmenu").mouseup(function(){
		return false
	});
	$(".dropdivmenu").mouseup(function(){
		return false
	});
	
	
	//Textarea without editing.
	$(document).mouseup(function(){
		$(".divmenu").hide();
		$(".dropdivmenu").attr('id', '');	
	});
	
});
function switoff(){
	
	$(".switcher").css({
					left: 0});
				$(".switch-left").css({
					left: -35});
				$(".switch-right").css({
					left: 35});
	
}

function switon(){
	$(".switcher").css({
		left: 35});
	$(".switch-left").css({
		left: 0});
	$(".switch-right").css({
		left: 70});
				
}
	$(function() {
	$(".switcher").draggable({
		axis: 'x',
		containment: 'parent',
		drag: function(event, ui) {
			$(".switch-left").css({
					left: (35-ui.position.left)*-1
			});
			
			$(".switch-right").css({
					left: ui.position.left+35
			});
			
			if (ui.position.left > 35) {
				return false;
			} else {
			   
			}
		},
		stop: function(event, ui) {
			
			if (ui.position.left <= 17) {
		setSideOn();
			}
			
			if (ui.position.left > 17) {
	setSideOn();
		}
		}
	});

	});
function lostslide(t){
	switch(t){
		case 'l':
			$('.content-slide').removeClass('onleft');
			$('.content-slide').addClass('onright');
			
			$('.content-slide').html(datahtml);
			
		break;
		
		case 'r':
			$('.content-slide').removeClass('onright');
			
		break;
	}
}
var datahtml='';
function ajaxload(urls){
	$('#loadovers').addClass('lshow');
	
	$.ajax({
		url:urls,
		type:'post',
		success:function(data){
			datahtml=data;
			$('.content-slide').addClass('onleft');
			setTimeout("lostslide('l')",400);
			setTimeout("lostslide('t')",500);
			setTimeout("lostslide('r')",900);
			$('#loadovers').removeClass('lshow');
			setTimeout("siapload()",1200);
			
		}
	});
	
}

function ajaxprint(url,cls,prm){
		var clstbl=(cls) ?  cls : 'grid-tabel';
		var clsprm=(prm) ?  prm : 'grid-params';
		var filters=$('.'+clsprm).serialize();
		var searchs=$('.'+clstbl).parent().parent().find("select[name='qtype'],input").serialize();
		window.open(url+'&'+searchs+'&'+filters);
}

function lostslideup(t){
	switch(t){
		case 't':
			$(".dash-content").html(datahtml);
			$(".dash-content").removeClass("proses");
			$('#loaddash').removeClass('lshow');
		break;
	}
}
function dashload(urls){
	$('#loaddash').addClass('lshow');
	
	$.ajax({
		url:urls,
		type:'post',
		success:function(data){
			datahtml=data;
			$(".dash-content").addClass("proses");
			setTimeout("lostslideup('t')",800);
		}
	});
	
}
function setAjaxTitle(title){
	if(title!=""){
		$('#title-content').html(title).show(1000);
	}
}
// format rupiah

function idr(val){
    numeral.language('en', {
        delimiters: {
            thousands: '.',
            decimal: ','
        },
    abbreviations: {
        thousand: 'k',
        million: 'm',
        billion: 'b',
        trillion: 't'
    },
    ordinal : function (number) {
        return number === 1 ? 'er' : '�me';
    },
    currency: {
        symbol: '�'
    }
    });
	if(val==Infinity){
		val=0;
		}
	return numeral(val).format('0,0.00');
}

function dec(val){
    numeral.language('en', {
        delimiters: {
            thousands: '.',
            decimal: ','
        },
    abbreviations: {
        thousand: 'k',
        million: 'm',
        billion: 'b',
        trillion: 't'
    },
    ordinal : function (number) {
        return number === 1 ? 'er' : '�me';
    },
    currency: {
        symbol: '�'
    }
    });
    var angka=numeral().unformat(val);
    return angka;
}

// toggle divider

var togel=function(th,itr){
		var hd=$(th).attr('hide');
		if(hd==1){
				$(itr).toggle(200);
				$(th).attr('hide',"0");
				$(th).children('span').removeClass("fa-angle-up");
				$(th).children('span').addClass("fa-angle-down");
		}else{
				$(itr).toggle(200);
				$(th).attr('hide',"1");
				$(th).children('span').removeClass("fa-angle-down");
				$(th).children('span').addClass("fa-angle-up");
		}
}

// aktifkan menu
$(document).ready(function(){
	// slim scroll
	//$("#firstmenu").height();
	var hww=parseInt($(window).height());
	$('#firstmenu').slimscroll({
	   position: 'left',
	   height:hww-60,
	   disableFadeOut: true,
	   railVisible:true,
	   size:'10px'
	});
//	$(".slimScrollBar").hide();
	
	
	//menu ajax 
	$(".ajaxurl").each(function(){
		$(this).click(function(){
			ajaxload($(this).attr('href'));
				var ww=$(document).width();
				 if(ww<942){
					sid=0;
					setonof(sid);
				}
			return false;
		});					
	});
	
	siapload();
	// load dahboard
	ajaxload("?act=dashboard");
	// 
	$('#master_tahun').change(function(){
		var vals=$(this).val();
		$.ajax({
			url:'?act=bulan&do=ganti_tahun&id='+vals,
			success:function(){
				window.location="?";
			}
		})
	})
});