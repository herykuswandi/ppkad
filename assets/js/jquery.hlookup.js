
var settings = new Array();
// Hery Kuswandi Code's
(function(n) {
var    defaults = {
 	url:'?',
	url_detail:'?',
	title:'Title',
	onComplete: function(id){},
 }
		n.fn.hlookup = function(options,d){
//setting default value jika parameter tidak dilewatkan
sett = $.extend({}, defaults, options);
 var el = this;  //"this" adalah DOM object
 var id = $(el).attr('id');  //"this" adalah DOM object
 settings[id]=sett;
 $('.modals_'+id).remove();
		$('#ctrl'+id).remove();
 var btmodal='<a class="fa fa-search-plus fa-lg btn-primary ctrl-hlookup"  title="'+settings[id].title+'" href="javascript:void(0)" id="ctrl'+id+'" style="padding: 8px 5px;height: 14px;width: 15px !important;color: #fff;text-decoration: none;position: absolute;right: -8px;bottom: 0px;"></a>';
 var objmodal='<div class="modals_'+id+'  gocontent" style="display:none; ">   <div class="grid-title " ><div class="pull-left title-modals" id="title-modal_'+id+'"  >Judul Modal</div></div><div class="modal_wrapper conteks_'+id+'" style="margin-top:30px;"></div></div>';
 $(el).after(btmodal);
 $(el).after(objmodal);
 $(el).click(function(){
 	$(this).select();
 });
 
	 $(el).autocomplete({
				source: settings[id].url_detail,
				minLength: 1,
				select: function( event, ui ) {
				var sdata=ui.item;
				settings[id].onComplete(sdata);
			}
	});
	 
	 	/*jQuery(el).select2({
        minimumInputLength: 1,
        placeholder: 'Cari',
        ajax: {
            url: settings[id].url_detail,
            dataType: 'json',
            quietMillis: 100,
            data: function(term, page) {
                return {
                    term: term
                };
            },
            results: function(data, page ) {
                return { results: data }
            }
        },
        formatResult: function(data) { 
            return "<div class='select2-user-result'>" + data.label + "</div>"; 
        },
        formatSelection: function(sdata) { 
		
				settings[id].onComplete(sdata);
            return sdata.label; 
        },
        initSelection : function (element, callback) {
       			  /*var elvalue = jQuery(element).val();
			      jQuery.ajax({
						type:'post',
						url:ajaxurl,
						dataType:'json',
						data:jQuery.param({id:elvalue,action:'detail_country'}),
						success:function(data){
      					 callback( data );
						}
        			});
        },
        allowClear:true
    });
	 */
 $('#ctrl'+id).click(function(){
 
	 		$('.modals_'+id).bPopup({
				contentContainer:'.conteks_'+id,
				speed: 450,
				modalClose: false,
				transition: 'slideDown',
				loadUrl: settings[id].url+'&elid='+id,
				onOpen: function() {
					showLoad('conteks_'+id,"Silahkan Tunggu, Sedang Mengambil Data...","mload_"+id);
					$('#title-modal_'+id).html(settings[id].title);	
				},
				loadCallback:function(){
					hideLoad("mload_"+id);	
					
					}
			});
	});
	
},
n.fn.hclose=function(){
	var el = this;  //"this" adalah DOM object
 var id = $(el).attr('id');
	$('.modals_'+id).bPopup().close();
	
},
n.fn.hselect=function(vkey){
	var el = this;  //"this" adalah DOM object
	
 var id = $(el).attr('id');
 $(el).hclose();	
 
			$(el).addClass('ui-autocomplete-loading');
 	$.ajax({
			url:settings[id].url_detail+'&n=lookup&term='+vkey,
			dataType:'JSON',
			success:function(sdata){
				$(el).val(sdata[0].value);	
			$(el).removeClass('ui-autocomplete-loading');
			
				settings[id].onComplete(sdata[0]);
			}
		})
}
})(jQuery);