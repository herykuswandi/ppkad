<?php
class CRUD{	
	private $show = false;
	private $stop 	= false;
	private $con;

	function __construct($config){
		$this->con=$config;
		$konek = mysql_connect($config['host'],$config['user'],$config['pass']);
		if (!$konek) die ('Conecction Failure');
		$db = mysql_select_db($config['db']);
		if (!$db) die ('Database "<b>'.$config['db'].'</b>" not Found');
		
	}
	
	function dump($stop=false){
		$this->show 	= true;
		$this->stop	= $stop;
	}
	
	function exec_dump($sql){
		if ($this->show) {
			$this->show = false;
			echo "<span style='color:red; '><b>$sql</b></span><br>";
		}
		if ($this->stop) {
			$this->stop	= false;
			die();
		}
	}
	
	function err_exec($sql, $no, $error){
		if (ini_get('display_errors')) {
			echo '<body style="background:black"><br><br><br><br><br>
			<div style="color:#0F5;width:700px; border:1px groove #0F5; margin: auto;-webkit-border-radius: 10px;-moz-border-radius: 10px;padding:10px;min-height:150px;" >
				<h3> Error in Query</h3>
				<b>Query : '.$sql.';<br>Error &nbsp;&nbsp;: (' . $no . ') ' . $error.'</b>
			</div>
			<br>
			</body>';
			die();
		}
	}

	function query($sql, $group=''){
		$this->exec_dump($sql);
		$exe 	= mysql_query($sql) or die($this->err_exec($sql, mysql_errno(), mysql_error()));
		if ($exe != 1){
			$num	= mysql_num_rows($exe);
			if ($num > 0 ){
				while ($row=mysql_fetch_assoc($exe) ){
					if ($group == '') $data[] = $row;
					else $data[$row[$group]] = $row;
				}
			}else $data = array();
		} else $data = $exe;
		return $data;
	}
	
	function get_data($table,$where=1,$select='*', $group = ''){
		$sql 	= "select $select from $table where $where";
		return $this->query($sql, $group);
	}
	
	function getNextIncrementTable($table) {
	    $next_increment = 0;
        $qShowStatus = "SHOW TABLE STATUS LIKE '$table'";
        $qShowStatusResult = mysql_query($qShowStatus) or die("Query failed: " . mysql_error() . "<br/>" . $qShowStatus);
        $row = mysql_fetch_assoc($qShowStatusResult);
        $next_increment = $row['Auto_increment'];
        return $next_increment;
    }
	
	function updateOpt($rows, $table, $where) {
	    if (!is_array($rows))
            $rows = json_decode($rows, true);
        $set = '';
        if (is_array($rows))
            foreach ($rows as $kolom => $value) {
						$set .= "$kolom = '" . addslashes($value) . "',";
            }
        $set = substr($set, 0, -1);
        $simpan = mysql_query("update $table set $set $where");
        return $simpan;
    }

	function get_data_page($table,$page=1,$perpage,$where=1,$select='*'){
		$offset 			= ($page - 1) * $perpage;
		$sql 				= "select $select from $table where $where limit $offset, $perpage";
		$data				= $this->query($sql);
		$sql 				= "select count(*) as jml from $table where $where";
		$exe 				= mysql_query($sql) or die($this->err_exec($sql, mysql_errno(), mysql_error()));
		$jml					= mysql_fetch_assoc($exe);
		$hasil['data'] 	= $data;
		$hasil['jml']		= ceil($jml['jml']/$perpage); //jumlah halaman
		$hasil['page']	= $page;
		return $hasil;
	}
	
	function get_single_data($table,$where=1,$select='*'){
		$sql 	= "select $select from $table where $where limit 0,1";
		$this->exec_dump($sql);
		$exe 	= mysql_query($sql) or die($this->err_exec($sql, mysql_errno(), mysql_error()));
		$data	= mysql_fetch_assoc($exe);
		return $data;
	}
	// insert / update data with describe
	 function create($rows, $table,$describe=false) {
		if(!is_array($rows)){
			$rows=array();
		}
		if($describe){
			$newrows=$rows;
			$dscb=mysql_query("describe $table");
			while($ds=mysql_fetch_assoc($dscb)){
				$field[]=$ds['Field'];
			}
			$rows=array();
			foreach($field as $field){
				// condition for field password
				if($field=='password'){
					$pass=($_REQUEST["password"]);
					($pass=="") ? $pass="123456" : "" ;
					$md=md5(md5('!@#$%^&*()1234567890<>?,./').md5($pass).md5('h3ryidn4w5ukAydanSoraya@citridiasolution'));
					$desrows[$field]=$md;
				}else{
					$desrows[$field]=($_REQUEST[$field]);
				}
			}
			$rows=array_merge($desrows,$newrows);
		
		}
        
		foreach($rows as $keyFilt=>$valFilt){
				if($valFilt==''){
					unset($rows[$keyFilt]);
				}
			}
        if (!is_array($rows))
            $rows = json_decode($rows, true);
        $field = '';
        $values = '';
        if (is_array($rows)) {
		    foreach ($rows as $kolom => $value) {
				if ($kolom == '')
                    continue;
                $field .= "$kolom,";
				if($kolom=="password"){
	                $values .= "password('".addslashes($value)."'),";
				}else{
                	$values .= "'" . addslashes($value) . "',";
				}
            }
            $field = substr($field, 0, -1);
            $values = substr($values, 0, -1);
        }
        $sql=("insert into $table ($field) values ($values)");
		$this->exec_dump($sql);
		$exe = mysql_query($sql) or die($this->err_exec($sql, mysql_errno(), mysql_error()));
        return $exe;
    }
	
	
	 function multi_create($rows, $table) {
	 	if(count($rows)>0 && is_array($rows)){
		$value=array();
		foreach($rows as $key=>$row){
			$subval=array();
			$label=array();
			foreach($row as $fil=>$col){
				$label[]=$fil;
				$subval[]="'".$col."'";
			}
			$value[]="(".implode(",",$subval).")";
		}
		
		$lab=implode(',',$label);
		$val=implode(',',$value);
        $sql=("insert into $table ($lab) values $val");
		$this->exec_dump($sql);
		$exe = mysql_query($sql) or die($this->err_exec($sql, mysql_errno(), mysql_error()));
		}else{
			return false;
		}
	 }
	function update($rows, $table, $describe=false,$wr="",$fkey="id") {
		if(!is_array($rows)){
			$rows=array();
		}
	
		if($describe){
			$newrows=$rows;
			$dscb=mysql_query("describe $table");
			while($ds=mysql_fetch_assoc($dscb)){
				$field[]=$ds['Field'];
			}
			$desrows=array();
			foreach($field as $field){
				//condition for password oke..
				if(isset($_REQUEST[$field]))
				if($field=='password'){
					$pass=($_REQUEST["password"]);
					if($pass!=""){
					$md=md5(md5('!@#$%^&*()1234567890<>?,./').md5($pass).md5('h3ryidn4w5ukp1r4t35Spy@citridiasolution2014'));
						$desrows[$field]=$md;
					}
				}else{
					$desrows[$field]=($_REQUEST[$field]);
				}
			}
			unset($desrows['files']);
			$rows=array_merge($desrows,$newrows);
		}
		/*
		foreach($rows as $keyFilt=>$valFilt){
			if($valFilt==''){
				unset($rows[$keyFilt]);
			}
		}*/
        if (!is_array($rows))
            $rows = json_decode($rows, true);
        $set = '';
        if (is_array($rows))
            foreach ($rows as $kolom => $value) {
                if ($kolom == $fkey)
                    continue;
				if ($kolom == '')
                    continue;
				if($kolom=="password"){
                $set .= "$kolom = password('".$value."'),";
				}else{
					$set .= "$kolom = '" . addslashes($value) . "',";

				}
            }
        $set = substr($set, 0, -1);
        $sql = "update $table set $set where $fkey = '$rows[$fkey]' $wr";
		
		$this->exec_dump($sql);
		$exe = mysql_query($sql) or die($this->err_exec($sql, mysql_errno(), mysql_error()));
		return $exe;
		}


	// end describe
	
	// Format data : data[field] = value;
	function insert_data($table,$data){
		foreach ($data as $key => $val){
			$ar[] 	= ($val === null) ? "null" :"'".$val."'";
		}
		$key 	= implode('`,`',array_keys($data));
		$val 	= (is_array($ar)) ? implode(',',$ar) : $ar;			
		$sql 	= "insert into $table (`$key`) values ($val)";
		$this->exec_dump($sql);
		$exe 	= mysql_query($sql) or die($this->err_exec($sql, mysql_errno(), mysql_error()));
		return $exe;
		
	}
	
	// Format data : data[field] = value;
	function update_data($table,$data,$where=1){  
		foreach ($data as $key => $val){
			$ar[]	= ($val === null) ? $key." = null" : $key." = '".$val."'";			
		}
		$set 		= (is_array($ar)) ? implode(',',$ar) : $ar;
		$sql 		= "update $table set $set where $where"	;
		$this->exec_dump($sql);
		$exe 			= mysql_query($sql) or die($this->err_exec($sql, mysql_errno(), mysql_error()));
		return $exe;
	}
	
	
	
	function save_data($table,$data,$where='',$auto_create=false){
		if (trim($where) != ''){
			if ($auto_create){
				$ada 	= $this->get_single_data($table,$where);
				if (is_array($ada)) 	$res		= $this->update_data($table,$data,$where);
				else $res	= $this->insert_data($table,$data);
			} else  $res	= $this->update_data($table,$data,$where);
		} else {
			$res	= $this->insert_data($table,$data);
		}
		return $res;
	}
	
	function delete_data($table,$where=''){
		$sql 	= "delete from $table where $where; ";
		$this->exec_dump($sql);
		$res 	= mysql_query($sql) or die($this->err_exec($sql, mysql_errno(), mysql_error()));
		return $res;	
	}
	
	
	function get_a_value($table,$field='*',$where=1){
		$sql		= "select $field from $table where $where";
		$this->exec_dump($sql);
		$exe 	= mysql_query($sql) or die($this->err_exec($sql, mysql_errno(), mysql_error()));
		$row	= mysql_fetch_assoc($exe);
		return $row[$field];
	}
	
	function optimize($table) {
		$sql = "Optimize Table $table";
		$this->exec_dump($sql);
		$exe = mysql_query ($sql) or die($this->err_exec($sql, mysql_errno(), mysql_error()));
		return true;
	}
	
	/* For Flexi Grid
		sample $col :
		$col	= array(
			'id' => 'id',
			'cell' => array('{menu}', '{link}', '{position}', '<a href=\"?edit\">[ Edit {id} ]</a>')
		);	*/
	function get_data_flexi($table,$where=1,$select='*',$srt='id',$ord='asc'){
		$page			= $_REQUEST['page'];
		$perpage		= $_REQUEST['rp'] ;
		$qry			= (isset($_REQUEST['query'])) ? $_REQUEST['query'] : $_REQUEST['q'];
		$qtype			= $_REQUEST['qtype'];
		if ($_REQUEST['sortname']=='undefined' || $_REQUEST['sortname']=='') {
			$sortname	= $srt;
			$sortorder	= $ord;
			$sort 		= "order by $sortname $sortorder";		
		}else{
			$sortname	= $_REQUEST['sortname'];
			$sortorder	= ($_REQUEST['sortorder']=='undefined' || $_REQUEST['sortorder']=='') ? $ord : $_REQUEST['sortorder'];
			$sort 		= "order by $sortname $sortorder";		
		}		
		
		$offset 			= ($page - 1) * $perpage;		
		$where=str_replace('where','',$where);
		if($qtype!=""){
		$where2			= $where." and ($qtype like '$qry%') $sort";
		}else{
		$where2			= $where." $sort";
		}
		
		if($perpage>0){
		$sql 				= "select $select from $table where $where2 limit $offset, $perpage";
		}else{
		$sql 				= "select $select from $table where $where2";
		}
		$this->exec_dump($sql);
		
		$hasil['exe']		= mysql_query($sql) or die($this->err_exec($sql, mysql_errno(), mysql_error()));
		$sql 				= "select count(*) as jml from $table where $where";
		$exe 				= mysql_query($sql) or die($this->err_exec($sql, mysql_errno(), mysql_error()));
		$jml					= mysql_fetch_assoc($exe);
		$hasil['total']	= $jml['jml'];
		$hasil['page']	= $page;
		if ($col){
			foreach ($col['cell'] as $no=>$a) $col['cell'][$no]	= addslashes($a);
			$search	= array('{', '}');
			$replace	= array('$row[', ']');
			$id 			= str_replace($search, $replace, $col['id']);
			
			$cell		= implode('","', $col['cell']);			
			$cell		= str_replace($search, $replace, $cell);
			$tes			= '$cell = array("'.$cell.'");';
			
			while ($row = mysql_fetch_assoc($hasil['exe'])) {
				eval($tes);				
				$data['page'] 		= $hasil['page'];
				$data['total'] 		= $hasil['total'];
				$data['rows'][] 	= array(
					'id' => $row[$col['id']],
					'cell' => $cell
				); 
			}		
			$hasil 	= json_encode($data);
		}
		return $hasil;
	}
	
// khusus buat bos
   function getTree($table,$attr='',$parent_id=0) {
    $attr=str_replace('where','and',$attr);
        $exe = mysql_query("select * from $table where parent = '$parent_id' $attr");
        $i = 0;
        while ($record = mysql_fetch_assoc($exe)) {
            $data[$i] = $record;
         	
		    if (mysql_num_rows(mysql_query("select * from $table where parent='$record[id]' $attr")) > 0){
           $data[$i]['children'] = $this->getTree($table,$attr, $record[id]);
			}
            else{
				$data[$i][leaf] = true;
			}
			$i++;

        }

    
        return $data;

    }
	
    function getTreeId($table,$attr='',$parent_id=0) {

    $attr=str_replace('where','and',$attr);
        $exe = mysql_query("select * from $table where parent = '$parent_id' $attr");
        
        while ($record = mysql_fetch_assoc($exe)) {
            $data[] = $record['id'];
         	
		    if (mysql_num_rows(mysql_query("select * from $table where parent='$record[id]' $attr")) > 0){
           	$data[]=$this->getTreeId($table,$attr, $record[id]);
			}
        }

        return $data;
    }

	 function getSub($table,$attr='',$parent_id=0) {
		$data=$this->getTreeId($table,$attr, $parent_id);
		$result = array();
		$stack = array();
		$path = null;
		if(is_array($data)){
		reset($data);
		while (!empty($data)) {
			$key = key($data);
			$element = $data[$key];
			unset($data[$key]);
			if (is_array($element)) {
				if (!empty($data)) {
					$stack[] = array($data, $path);
				}
				$data = $element;
				$path .= $key . $separator;
			} else {
				$result[$path . $key] = $element;
			}
			if (empty($data) && !empty($stack)) {
				list($data, $path) = array_pop($stack);
			}
		}
		$result[]=$parent_id;
		}else{
		$result[]=$parent_id;	
		}
		
		return implode(',',$result);
    }
	
	function restoreData($name,$temp){
		$config=$this->con;
		$dbn = new mysqli ($config['host'],$config['user'],$config['pass'],$config['db']);
	//	$dir="../files/$name";
		$hasil=explode('.',$name);
		$nama_b = $hasil [count($hasil) - 1];
		
		if (in_array($nama_b,array("sql","txt"))){
		// move_uploaded_file($temp,"$dir");
		$fh = fopen($temp, 'r');
		$theData = fread($fh, filesize($temp));
		fclose($fh);
		
		$que=$dbn->multi_query("$theData");
		
		if($que){
			$data ="1";
		}else{
		
			$data ="2";
		}
	}else{
		$data ="0";
	}
	return $data;
	
	}
	
	static function backup_tables($tables,$opt='')
	{
  if($tables == '*')
  {
    $tables = array();
    $result = mysql_query('SHOW TABLES');
    while($row = mysql_fetch_row($result))
    {
		if($row[0]!='sek_data'){
		  $tables[] = $row[0];
		}
    }
  }
  else
  {
    $tables = is_array($tables) ? $tables : explode(',',$tables);
  }

  
	$alter=implode(',',$tables);
	$return='';
	$arrcons=array();
	  foreach($tables as $table)
	  {
		$result = mysql_query('SELECT * FROM '.$table);
		$num_fields = mysql_num_fields($result);
		
		$row2 = mysql_fetch_row(mysql_query('SHOW CREATE TABLE '.$table));
		//$return.= $row2[1].";\n\n";
		$strfok=strpos($row2[1],"CONSTRAINT");
		$endsfok=strpos($row2[1],"ENGINE");
		if($strfok>0){
			$return.= "\n\n".substr($row2[1],0,13).'IF NOT EXISTS'.substr($row2[1],12,($strfok-16)).substr($row2[1],$endsfok-3).";\n\n";
			$cons=substr($row2[1],$strfok,($endsfok-$strfok-3));
			$arrcons[$table]=$cons;
		}else{
			$return.= "\n\n".substr($row2[1],0,13).'IF NOT EXISTS'.substr($row2[1],12).";\n\n";
		}
	//	echo substr($row2[1],$strfok,$endsfok-$strfok);
		$return.="truncate table $table ;\n\n";
		for ($i = 0; $i < $num_fields; $i++) 
		{
		  while($row = mysql_fetch_row($result))
		  {
			$return.= 'INSERT INTO '.$table.' VALUES(';
			for($j=0; $j<$num_fields; $j++) 
			{
			  $row[$j] = addslashes($row[$j]);
			  //$row[$j] = ereg_replace("\n","\\n",$row[$j]);
			  if (isset($row[$j])) { $return.= "'".$row[$j]."'" ; } else { $return.= "''"; }
			  if ($j<($num_fields-1)) { $return.= ','; }
			}
			$return.= ");\n";
		  }
		}
		$return.="\n\n\n\n";
	  }
	  $tanggal=date("d-F-Y");
	//alter tabel untuk menambahkan constraint
	$constraint="";
	foreach($arrcons as $in=>$vl){
		$constraint.="ALTER TABLE $in";
			$fork=array();
			$fk=explode(',',str_replace("\n","",$vl));
			foreach($fk as $fk){
				$fork[]="\nADD ".$fk;
			}
		$constraint.=implode(',',$fork).";\n\n";
	}
	  header("Content-Disposition: attachment; filename=".$opt.".sql");
	  header("application/force-download");
	  echo $return.$constraint;
	 exit;
//	print_r($arrcons);
		;
	}

}
?>
