<?php

class Config{	
	public $crud;
	private $config;

		
	function __construct(){
		include('config/config.php');
		$this->config = $_CONFIG;
		$this->crud= new CRUD($this->config['db']);
		$this->form= new Yariform();
	}
	
	function get_config($part= 'all'){
		return ($part=='all') ? $this->config : $this->config['$part'];
	}

}
?>