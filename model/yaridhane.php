<?php

class Yaridhane extends Config{	
	function __construct(){
		parent::__construct();
	}
	function tglIndo($str){
		$wekwek=($str=="") ? "" : date("d-m-Y", strtotime($str));
		return $wekwek;
	}
	
	function tglan($str){
		$wekwek=($str=="") ? "" : date("Y-m-d", strtotime($str));
		return $wekwek;
	}
	function tglIndoSlash($str){
		$wekwek=($str=="") ? "" : date("d/m/y", strtotime($str));
		return $wekwek;
	}
	
	function rupiah($angka){
		$angka+=0;
		$wekwek=number_format($angka, 0, ",", ".");
		return $wekwek;
	}

	function render($page, $method = '__default'){
		// init
		$this->page=$page;
		$page=strtolower($page);
		$method = ($method=='') ? '__default' : strtolower($method);		
		// include controller
		$ctr_file='controller/'.$page.".php";
		if(file_exists($ctr_file)) {
			include_once($ctr_file);
		}else{
			die('Controller file not found !');
		}
		// cek dan panggil class di controller
		$ctr_class=ucfirst($page);
		if (class_exists($ctr_class, false)) {
				$ctr_obj = new $ctr_class($page,$method);
		}else{
			die('Class '.$page.' Not Found !');
		}			
		//cek dan panggil method yang sesuai di request
		if(method_exists($ctr_obj,$method)) {
			$ctr_obj->$method();
		} else {
			die('method '.$method.' Not Found !');
		}
	}
	
	function loadView($page,$method,$vars,$single=false){
		$pg='view/'.$page.'/'.$method.'.php';
		if(is_array($vars))
		foreach ($vars as $key=>$var){
				${$key}=$var;
		}
		if(!$isprint){
			if($single){
				if(file_exists($pg)){
					include($pg);
				}else{
					echo 'halaman file tidak di temukan';
				}
			}else{
				$_SESSION['sinad']['profil']=$this->crud->get_single_data('user',"id='$_SESSION[uniqsinad]'");	
				include('view/layout/header.php');
				if(file_exists($pg)){
					include($pg);
				}else{
					echo 'halaman file tidak di temukan';
				}
				include('view/layout/footer.php');
			}
			echo ($vars['grid']) ? $vars['grid'] :'';
			if(isset($vars['is_dashboard'])){
				$vars['title']='Dashboard';
			}
			echo "<script>setAjaxTitle('".$vars['title']."');</script>";
		}
	}
	
	    static function uploadFile($file,$auto_number="",$type = array('gif', 'pdf', 'doc', 'docx', 'xls', 'xlxs', 'jpg', 'png', 'tiff', 'zip', 'rar', 'mp3')) {

        //return array(success => false , message => "$code, $tipe, $file[name]");
        $POST_MAX_SIZE = ini_get('post_max_size');
        $mul = substr($POST_MAX_SIZE, -1);
        $mul = ($mul == 'M' ? 1048576 : ($mul == 'K' ? 1024 : ($mul == 'G' ? 1073741824 : 1)));
        if ($_SERVER['CONTENT_LENGTH'] > $mul * (int) $POST_MAX_SIZE && $POST_MAX_SIZE)
            return array(success => false, message => 'File terlalu besar');
        $exp = explode('.', strtolower($file['name']));
		$ext=$exp[count($exp)-1];
        if (!in_array($ext, $type))
            return array(success => false, message => 'Tipe file tidak di ijinkan ' . $ext);
        $uploaddir ='files/';
		$nama=date("d-M-Y G_i_s").'_'.$file['name'];
        $uploadfile =$uploaddir.$nama;
        if (file_exists($uploadfile))
            return array(success => false, message => 'File Sudah Ada');
        if (move_uploaded_file($file['tmp_name'], $uploadfile)) {
			return array(success => true,nama=>$nama);
		}
        else{
            return array(success => false, message => 'Gagal Mengupload file, kesalahan server');
		}
    }
	
	
	function render_grid($params,$class='grid-tabel',$gridparams='grid-params'){
	if(!$params['isprint']){
		if(is_array($params))
		foreach ($params[fields] as $no => $field){
			if (substr($field[width],-1) == '%')
				$params[fields][$no][width] = 'parsen('.substr($field[width], 0, -1).')';
			if ($field[filter] && !$field[hide])
				$filter[] = $field;
			if ($field[def_sort]){
				$def_sort = 'sortname: "'.$field[name].'",';
				$sort_order = 'sortorder: "'.$field[sort_order].'",';
			}
			$url = ($params[url] != '') ? $params[url] : '?'.$this->class;
		}
		if (is_array($params[form_filter])) foreach($params[form_filter] as $form_filter){
			$method = 'create_'.$form_filter[type];
			if (method_exists($this,$method)) $input[] = $this->$method($form_filter);
		}
		if($params[showpage]<=0){
		$params[showpage]=10;
		}
		
			$dblclick = ($params[dblclick] != '') ? $params[dblclick] : 'false';
		$form_filter = (is_array($input)) ? implode('&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',$input).'<br><br>' : '';
		$template = '<script>
				$(document).ready(function(){
					$(".'.$class.'").flexigrid({
						url: \''.$url.'\',
						dataType: \'json\',
						colModel : '.str_replace(array('"parsen(',')"'),array('parsen(',')'),json_encode($params[fields])).',
						searchitems : '.json_encode($filter).',
						'.$def_sort.' '.$sort_order.'
						usepager: true,
						useRp: true,
						params:$(".'.$gridparams.'").serializeArray(),
						rp: '.$params[showpage].',
						resizable: true,
						width: "auto",
						height: "auto",
						onDoubleClick:'.$dblclick.'
					});
				});
			</script>
		';
	return $template;

	}else{
	
		foreach ($params['fields'] as $no => $field){
			if ($field['filter'])
				$filter[] = $field;
		}		
		return array('fields'=>$params['fields'],'filter'=>$filter);
	}
	
	}
	
	
	function render_grid_lookup($params,$class='look-tabel',$gridparams='look-params'){
	if($params['add_data']==true){
	
	}
	$template ='';
$template .= '<br />
<div class="bgreen" style="padding: 5px 10px ; font-size:14px;display: inline;"><b>Info : </b>Dobel Klik Untuk Memilih Data / klik data kemudian tekan tombol pilih</div><table class="look-tabel" style="display: none; "></table>
<div class="nav-control">';
if($params['add_data']==true){
	
$template .= '<input type="button" class="btn-success" value="Tambah Baru" onClick="addmodal()">';

}
$template .= '<input type="button" class="btn-primary" value="Pilih" onClick="sel_'.$params[elid].'()">
<input type="button" value="Batal"  class="btn-danger" onClick="$(\'#'.$params[elid].'\').hclose();">
</div>
';
		if(is_array($params))
		foreach ($params[fields] as $no => $field){
			if (substr($field[width],-1) == '%')
				$params[fields][$no][width] = 'parsen('.substr($field[width], 0, -1).')';
			if ($field[filter])
				$filter[] = $field;
			if ($field[def_sort]){
				$def_sort = 'sortname: "'.$field[name].'",';
				$sort_order = 'sortorder: "'.$field[sort_order].'",';
			}
			$url = ($params[url] != '') ? $params[url] : '?'.$this->class;
			$url.='&elid='.$params[elid];
		}
		if (is_array($params[form_filter])) foreach($params[form_filter] as $form_filter){
			$method = 'create_'.$form_filter[type];
			if (method_exists($this,$method)) $input[] = $this->$method($form_filter);
		}
		if($params[showpage]<=0){
		$params[showpage]=10;
		}
		
			$dblclick = ($params[dblclick] != '') ? $params[dblclick] : false;
		$form_filter = (is_array($input)) ? implode('&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',$input).'<br><br>' : '';
		$template .= '<script>
		var sel_'.$params[elid].'=function(){
		var nad=$(".'.$class.' tr.trSelected").find("td").first().text();
	if(nad==undefined || nad==""){
	alert("Anda harus memilih (select) data di tabel terlebih dahulu !");	
	return false;
	}else{
		$("#'.$params[elid].'").hselect(nad);
		return false;
	}
	}
	';
	
if($params['add_data']==true){
$template .='
var tempadd=\'<div class="addmod_'.$params[elid].'  gocontent" style="display:none; "><div class="grid-title " ><div class="pull-left title-modals" id="addtitle_'.$params[elid].'"  >Judul Modal</div></div><div class="modal_wrapper addcont_'.$params[elid].'" style="margin-top:40px;"></div></div>\';
function addclose(){
	$(".addmod_'.$params[elid].'").bPopup().close();
	
}
function addrefresh(){
	refreshed("look-tabel");
}
function addclose(){
	$(".addmod_'.$params[elid].'").bPopup().close();
}
	function addmodal(){
			$(".addmod_'.$params[elid].'").bPopup({
					contentContainer:".addcont_'.$params[elid].'",
					speed: 450,
					modalClose: false,
					transition: "fade",
					loadUrl: "'.$params[add_url].'",
					onOpen: function() {
						showLoad("addcont_'.$params[elid].'","Silahkan Tunggu, Sedang Mengambil Data...","addloads_'.$params[elid].'");
						$("#addtitle_'.$params[elid].'").html("'.$params[add_title].'");	
					},
					loadCallback:function(){
						hideLoad("addloads_'.$params[elid].'");	
						siapload();
					}
				});
	}
';
}
$template .='				$(document).ready(function(){';

if($params['add_data']==true){
$template .='$(".addmod_'.$params[elid].'").remove();
 $("#'.$params[elid].'").after(tempadd);';
 }
 $template .='
 
					$(".'.$class.'").flexigrid({
						url: \''.$url.'\',
						dataType: \'json\',
						colModel : '.str_replace(array('"parsen(',')"'),array('parsen(',')'),json_encode($params[fields])).',
						searchitems : '.json_encode($filter).',
						'.$def_sort.' '.$sort_order.'
						usepager: true,
						useRp: true,
						params:$(".'.$gridparams.'").serializeArray(),
						rp: '.$params[showpage].',
						resizable: true,
						width: "auto",
						height: "auto",
						onDoubleClick:'.$dblclick.',
						singleSelect:true,
						afterload:function(){
					$(".fakeheight").hide();
						}
					});
				});
			</script>
		';
		echo $template;
	return $template;

	}
	function auto_field($table,$rows=array(),$hidden='',$mergeLeft=false){
	
			$newrows=$rows;
		$query = 'show columns from '.$table;
		$data = $this->crud->query($query);
		$arya=array();
		if(is_array($data))
		foreach ($data as $field){
			$label = ucwords(str_replace(array('id_','_','-'),' ',$field['Field']));
			$name = $field['Field'];
			$arya[$name]=array(display=>$label, name=>$name,align=> 'left', sortable=> true, filter=> true,width=>150);
		}
		if($mergeLeft){
		$arya=array_merge($rows,$arya);
		}
		else{
		$arya=array_merge($arya,$rows);
		}
		$hiddens=explode(',',$hidden);
		foreach($hiddens as $hidde){
			unset($arya[$hidde]);		
		}
		$arfield=array();
		foreach($arya as $aryas){
			$arfield[]=$aryas;
		}
		return $arfield;
	}
	
	
	function auto_form($rows=array(),$table='',$where='',$auto=false,$required='',$hiddens=array()){
		if ($auto==true){
			$newrows=$rows;
		$query = 'show columns from '.$table;
		$data = $this->crud->query($query);
		$perlu = explode(',',$required);
		
		if ($where!=''){
			$value = $this->crud->get_single_data($table,$where);
		}
		
		foreach ($data as $field) {
			$label = ucwords(str_replace(array('id_','_','-'),' ',$field['Field']));
			$name = $field['Field'];
			$class = (in_array($field['Field'],$perlu)) ? 'perlu' : '';
			preg_match('/^([a-zA-Z]+)\((.*)\)$/', $field['Type'], $type);
			
			$input[$field['Field']][label] = $label;
			$input[$field['Field']][name] = $name;
			$input[$field['Field']][value] = ($where!='') ? $value[$field['Field']] : '';
			$input[$field['Field']][_class] = $class;
			if($type[1]=='int'){
			$input[$field['Field']][_class]='numeric';	
			}
			if ($field['Key'] == 'PRI' || in_array($field['Field'],$hiddens)){	
					$input[$field['Field']][type] = 'hidden';
			}else{		
			switch ($type[1]) {
				case 'varchar' : 
					$exdef=explode('#',$field['Default']);
					if($exdef[0]=='combo'){
						$input[$field['Field']][type] = 'combo';
						if($exdef[1]=='d'){
						$input[$field['Field']][data]
 =$this->crud->get_data($exdef[2],$exdef[3],$exdef[4].' value,'.$exdef[5].' label');		
						}else{
						$aar=json_decode($exdef[2],true);
						$aars=array();
						foreach($aar as $aa=>$ar){
						$aars[]=array('label'=>$ar,'value'=>$aa);	
						}
						$input[$field['Field']][data]
 =$aars;
 						}
 					}else{
						if(strpos($field['Default'],'no_photo')){
						$input[$field['Field']][type] = 'file';
						
					}else{
						$input[$field['Field']][type] = 'text';
						$input[$field['Field']][size] = floor(intval($type[2]) / 5);
					}
					}
				break;
				case 'enum' : 
					$input[$field['Field']][type] = 'radio';
				default : 
					if ($field['Type']== 'text'){
						$input[$field['Field']][type] = 'textarea';
						$input[$field['Field']][cols] = 30;
						$input[$field['Field']][rows] = 7;
					} else if ($field['Type']== 'date'){
						$input[$field['Field']][type] = 'date';
					} else {
						$input[$field['Field']][type] = 'text';
						$input[$field['Field']][size] = 40;
					}
				
				break;
			}
			}
		}
			$rows=$input;
			if(is_array($newrows))
			foreach($newrows as $kiis=>$newrow){
				if(is_array($input[$kiis]))
				$rows[$kiis]=array_merge($input[$kiis],$newrow);
			}
		}
		$render=$this->form->render($rows);
		return $render;
	}
	
	function render_print($vars,$page='layout', $method = 'grid_print'){
		if(is_array($vars))
		foreach ($vars as $key=>$var){
				${$key}=$var;
		}
		$pg='view/'.$page.'/'.$method.'.php';
		if(file_exists($pg)){
			include($pg);
		}else{
			echo 'halaman file tidak di temukan';
		}
			
	}   
	// sisa stok inventory
	 function get_sisa_stok($kode) {
        $kdo=get_ses("kdo");
       $sisas = $this->crud->get_single_data('inventory', "kode='$kode' and kdo='$kdo' ", 'stok');
        if (isset($sisas['stok'])) {
  			return $sisas['stok'];
        } else {
			return 0;
        }
    } 
	
    function set_jurnal($kode, $debit = 0, $kredit = 0,$keterangan="",$kode_id="") {
        //$this->crud->dump();
        $rows = array();
        $jur=array();
        $tanggal=date('Y-m-d');
        $kdo=get_ses("kdo");
		
        $saldo = $this->crud->get_single_data('keu_saldo', "kode_akun='$kode' and kdo='$kdo' order by id desc", 'saldo_awal, saldo_akhir,id');
        if (isset($saldo['id'])) {
            $saldo_awal = $saldo['saldo_awal'];
            $saldo_akhir = $saldo['saldo_akhir'] + $debit - $kredit;
        } else {
            $saldo_awal = 0;
            $saldo_akhir = $debit - $kredit;
        }
        $arrsaldo=array("kdo"=>$kdo,"tanggal"=>$tanggal,"kode_akun"=>$kode,"saldo_awal"=>$saldo_awal,"debit"=>$debit,"kredit"=>$kredit,"saldo_akhir"=>$saldo_akhir);
       
	    $arrjurnal=array("kdo"=>$kdo,"tanggal"=>$tanggal,"kode_akun"=>$kode,"debit"=>$debit,"kredit"=>$kredit,"keterangan"=>$keterangan,"kode_id"=>$kode_id);
		
		$this->crud->create($arrsaldo, 'keu_saldo');
        $this->crud->create($arrjurnal, 'keu_jurnal');
    } 
	
	// akutansi
	function get_akun_tree($optiongroup=false,$kode_kelompok="",$kode_kategori="",$selected=""){
		$kdo=get_ses("kdo");
		$kode_kelompok=str_replace(',',"','",$kode_kelompok);
		$atr_kelompok=($kode_kelompok!="") ? " and kode IN ('$kode_kelompok')" : ""; 
        $kelompok=$this->crud->get_data("keu_akun_kelompok","kdo='$kdo' $atr_kelompok","*");
		
        $kode_kategori=str_replace(',',"','",$kode_kategori);
		$atr_kategori=($kode_kategori!="") ? " and k.kode IN ('$kode_kategori')" : ""; 
        $atr_akun_kategori=($kode_kategori!="") ? " and kode_kategori IN ('$kode_kategori')" : ""; 
		
		$atr_tabel="";
		if($kode_kelompok!="" && $kode_kategori==""){
			$atr_kategori.=" and k.kode_kelompok IN ('$kode_kelompok')";
	
			$atr_tabel_akun.=",keu_akun_kategori k,keu_akun_kelompok ak";
			$atr_akun_kategori.=" and k.kdo='$kdo' and ak.kdo='$kdo' and a.kode_kategori=k.kode and k.kode_kelompok=ak.kode and ak. kode IN ('$kode_kelompok')";
		}
	    $kategori=$this->crud->get_data("keu_akun_kategori k","k.kdo='$kdo' and k.hapus=0 $atr_kategori","*");
		
        $akun=$this->crud->get_data("keu_akun a $atr_tabel_akun","a.kdo='$kdo' and a.hapus=0 $atr_akun_kategori","*");
		$data_akun=array();
		$data_kelompok=array();
		$data_kategori=array();
			foreach($akun as $aku){
				$data_akun[$aku['kode_kategori']][]=$aku;
			}
			
			foreach($kategori as $kateg){
				$kateg['child']=array();
				if(isset($data_akun[$kateg['kode']])){
					$kateg['child']=$data_akun[$kateg['kode']];
				}
				$data_kategori[$kateg['kode_kelompok']][]=$kateg;
			}
			
			foreach($kelompok as $kelom){
				$kelom['child']=array();
				if(isset($data_kategori[$kelom['kode']])){
					$kelom['child']=$data_kategori[$kelom['kode']];
				}
				$data_kelompok[]=$kelom;
			}
		if($optiongroup){
			$html="";
				foreach($data_kelompok as $row_kelompok){
					$html.='<optgroup label="'.$row_kelompok['nama'].'">'; 
					$html.="</optgroup>";
					foreach($row_kelompok['child'] as $row_kategori){
						$html.='<optgroup label="&nbsp;&nbsp;'.$row_kategori['nama'].'">'; 
							foreach($row_kategori['child'] as $row_akun){
								$html.='<option value="'.$row_akun['kode'].'" '.(($selected==$row_akun['kode']) ? 'selected="selected"' : '').'>'; 
								$html.=$row_akun['nama'];
								$html.="</option>";
					
							}	
						$html.="</optgroup>";
					}
				}
			return $html;
		}else{
			
			return $data_kelompok;
		}
	}

}
?>