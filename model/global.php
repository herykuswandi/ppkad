<?php

function redirect($link){
	echo "<script>window.location='$link';</script>"; 
die();
}

function implodes($data=array(),$key="kode",$glue=","){
	$newarr=array();
	foreach($data as $da){
		$newarr[]=$da[$key];
	}
	return implode($glue,$newarr);
}
function get_ses($key,$part=""){
	$ses=$_SESSION['sinad'][$key];
	if($part!=""){
		$ses=$ses[$part];
	}
	return $ses;
}
function set_ses($key,$val){
	$_SESSION['sinad'][$key]=$val;
	return false;
}
function del_ses($key){
	unset($_SESSION['sinad'][$key]);
	return false;
}
function primatch($key,$typ='style'){
	if($_SESSION['level']=='supermasteradmin'){
		return ($typ=='style') ? '' : true;	
	}else{
		$akses=json_decode($_SESSION['akses'],true);
		if(!is_array($akses)){
		$akses=array();
		}
		if(in_array($key,$akses)){
			return ($typ=='style') ? '' : true;	
		}else{
		return ($typ=='style') ? ' style="display:none;"' : false;
		}
	}
}
function get_kode($angka,$digit=4){
	$nol='';
		if (strlen($angka) < $digit) {
            for ($i = 0; $i < ($digit - strlen($angka)); $i++) {
                $nol .= '0';
            }
            $next = $nol . $angka;
        }	else{
           $next = $angka;
 			}
	return $next;
}

function str_ucrep($s){
	$a="";
	$a=ucwords(str_replace("_"," ",$s));
	return $a;
}

function tglIndo($str){
	return ($str=="") ? "" : date("d-m-Y", strtotime($str));
}
function tglDunia($str){
	return ($str=="") ? "" : date("Y-m-d", strtotime($str));
}
function date2Ind($str) {
	setlocale (LC_TIME, 'indonesian');
	$date = strftime( "%d %B %Y", strtotime($str));
	
	return $date;
}
function jumhari($month, $year) 
{ 
	return $month == 2 ? ($year % 4 ? 28 : ($year % 100 ? 29 : ($year % 400 ? 28 : 29))) : (($month - 1) % 7 % 2 ? 30 : 31); 
} 

function rupiah($rp){
	$rp=($rp>0) ? $rp : 0;
	return number_format($rp,2,',','.');
}
function derupiah($rp){
if(strpos($rp,",")){
    $rq=str_replace('.', '', $rp);
    $rs=str_replace(',', '.', $rq);
    return $rs;
}else{
    return $rp;

}	
}
function header_excel($nama){
		 header("Content-type: application/vnd.ms-excel; name='excel'");
        header("Content-Disposition:filename=".$nama.".xls");
}
function header_print(){
	    echo "<script>window.print();</script>";
}

?>