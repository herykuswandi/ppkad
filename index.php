<?php
	session_start();
	
	if(isset ($_SESSION['uniqsinad'])){
		define('HOST_DIR', dirname(__FILE__));
		include_once('model/config.php');
		include_once('model/crud.php');
		include_once('model/yariform.php');
		include_once('model/yaridhane.php');
		include_once('model/global.php');
		include_once('model/excel_reader.php');
		include_once('model/fpdf.php');
		
		function stripslashes_deep($value){
		$value = is_array($value) ? array_map('stripslashes_deep', $value) : addslashes(stripslashes($value));
		return $value;
		}
		
		$_POST = array_map('stripslashes_deep', $_POST);
		$_GET = array_map('stripslashes_deep', $_GET);
		$_COOKIE = array_map('stripslashes_deep', $_COOKIE);
		$_REQUEST = array_map('stripslashes_deep', $_REQUEST);
		
		$yaridhane = new Yaridhane();
		if($_REQUEST['act']=="logout"){
		session_destroy();	
		echo '<script> window.location="login.php";</script>';	die();
		}
		$act		= ($_REQUEST['act']) ? $_REQUEST['act'] : 'layout';
		$do		= $_REQUEST['do'];
		$yaridhane->render($act, $do);	
		
	}else{
	// $_SESSION['sinad']=array(
	// 	'kode_user'=>"USR0001",
	// 	'kdo'=>"LA0001",
	// 	'simbol'=>"MR"
	// );
?>
	<script>
		window.location="login.php";
	</script>
<?php }?>