<script>
$(document).ready(function(){
		$( "#tab-organisasi" ).tabs();
});
</script>

<div id="tab-organisasi" style="position:inherit;">
	<ul>
		<li><a href="#urusan">Urusan</a></li>
		<li><a href="#bidang">Bidang</a></li>
		<li><a href="#unit">Unit</a></li>
		<li><a href="#sub-unit">Sub Unit</a></li>
	</ul>
	<div id="urusan">
		<table class="tabel-urusan" style="display: none; "></table>
		<div class="nav-control">
		   <a href="?act=organisasi&do=form_urusan" label="Tambah Urusan"  title="Tambah Urusan" class="linkbox">
		   		<button type="button"  class="btn-success" ><span class="fa fa-plus-circle"></span>Tambah</button>              
		   </a>
		</div>
	</div>
	<div id="bidang">
		<table class="tabel-bidang" style="display: none; "></table>
		<div class="nav-control">
		   <a href="?act=organisasi&do=form_bidang" label="Tambah Bidang"  title="Tambah Bidang" class="linkbox">
		   		<button type="button"  class="btn-success" ><span class="fa fa-plus-circle"></span>Tambah</button>              
		   </a>
		</div>
	</div>
	<div id="unit">
		<table class="tabel-unit" style="display: none; "></table>
		<div class="nav-control">
		   <a href="?act=organisasi&do=form_unit" label="Tambah Unit"  title="Tambah Unit" class="linkbox">
		   		<button type="button"  class="btn-success" ><span class="fa fa-plus-circle"></span>Tambah</button>              
		   </a>
		</div>
	</div>
	<div id="sub-unit">
		<table class="tabel-sub-unit" style="display: none; "></table>
		<div class="nav-control">
		   <a href="?act=organisasi&do=form_sub_unit" label="Tambah Sub Unit"  title="Tambah Sub Unit" class="linkbox">
		   		<button type="button"  class="btn-success" ><span class="fa fa-plus-circle"></span>Tambah</button>              
		   </a>
		</div>
	</div>
</div>


