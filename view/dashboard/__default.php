<div style="display:block; position:relative; z-index:1;"><div class="form-rows fheight-4 f-dashboard fbreak horz-box">
<crow>
<div class="short-box bgreen"  > 
	<div class="sb-left">
		<div class="h-icon fa fa-magic fa-5x">
		</div>
	</div>
	<div class="sb-right">
		<div class="h-label">
			<span><?=$transtoday['jumlah']?></span>
			<i class="h-info"></i>
		</div>
		
	</div>
	<div class="h-option" onclick="$('#info-trans').html('');$('#intrans_kode').val('');">
	Penerimaan Kas &nbsp;
		 <!-- <span class="fa fa-caret-down"> </span>-->
	</div>
	<div class="h-link">
		<div onclick="ajaxload('?act=transaksi&do=form')"><a href="javascript:void(0)"><span class="fa fa-plus" > </span> Tambah</a></div>
		<div onclick="ajaxload('?act=transaksi')"><a href="javascript:void(0)"><span class="fa fa-th-list"> </span> Lihat Data</a></div>
	</div>
	<div class="h-desc animated">
		<div class="desc-caret"><span class="fa fa-caret-up"> </span></div>
		<!-- <div class="over-grid-tahap" style="text-align: left; display: block;">
			
			<form method="post" style="width:100%" class="form-rows fheight-1 fbreak" >
			<crow>
			<label><tag>Nota / Customer :</tag>
			<input type="text" name="intrans_kode" id="intrans_kode" placeholder="Masukan Nota / Customer"  value=""  />
			</label>
			</crow>
			<div id="info-trans">
			</div>
			
			<div class="clear"></div>
			<div class="nav-control">
				<button type="button" style="margin-right:5%;" onclick="
			$(this).parent().parent().parent().parent().removeClass('flipInY');
			$(this).parent().parent().parent().parent().addClass('flipOutY');
			$(this).parent().parent().parent().parent().attr('isshow','0');
			" class="btn-primary " ><span class="fa fa-magic"></span>OK</button>
			</div>	
			</form>
		</div> -->
	</div>
	<div class="clear"></div>
</div>

</crow>

<crow>

<div class="short-box borange" > 
	<div class="sb-left">
		<div class="h-icon fa fa-puzzle-piece fa-5x">
		</div>
	</div>
	<div class="sb-right">
		<div class="h-label">
			<span><?=$tahaptoday['jumlah']?> </span>
			<i class="h-info"></i>
		</div>
	</div>

	<div class="h-option" onclick="$('#dash-thform input[type=\'text\'], #dash-thform select').tooltipster('hide');cleartahp();">
	Pengeluaran Kas
		 <!-- <span class="fa fa-caret-down"> </span>-->
	</div>
	<div class="h-link">
		<div onclick="ajaxload('?act=transaksi_tahap')"><a href="javascript:void(0)"><span class="fa fa-pencil-square-o"> </span> Pengerjaan</a></div>
		<div onclick="ajaxload('?act=transaksi_tahap&do=rekap')" ><a href="javascript:void(0)"><span class="fa fa-list"> </span> Lihat Rekap</a></div>
		
	</div>
	
	<div class="h-desc animated">
		<div class="desc-caret"><span class="fa fa-caret-up"> </span></div>
		<div class="over-grid-tahaps" style="text-align: left; display: none;">
			
		<form method="post" id="dash-thform"  style="width:100%" class="form-rows fheight-1 fbreak" >
			
		<input type='hidden' name='kode_trans' id="dash_kode_trans">
		<input type='hidden' name='id_tahapan_lama' id='id_tahapan_lama' >
			<crow>
			<label><tag>Nota / Customer :</tag>
			<input type="text" name="thrans_kode" id="thrans_kode" placeholder="Masukan Nota / Customer"  value=""  />
			</label>
			</crow>
			
			<div id="tahap-trans">
			</div>
			<crow>
			<label><tag>Status Sekarang</tag>
			<input type="text" name="tahapan_sekarang" readonly="" id="tahapan_aktif"  />
			</label>
			</crow>
			<crow>
			<label><tag>Status</tag>
			<select name='id_tahapan' id="dash_id_tahapan" style='width: 100%;' ></select>
			</label>
			</crow>
			<crow>
			<label><tag>Karyawan</tag>
			
			<select name='kode_karyawan' style='width: 100%;' >
			<?
			if(is_array($pegawai))
			foreach($pegawai as $pegaw){
			?>
			<option value="<?=$pegaw['kode']?>"><?=$pegaw['nama']?></option>
			<? }?>
			</select>
			</label>
			</crow>
			<crow>
			<label><tag>Tanggal Jam</tag>
			<div style="width: 100%;
clear: both;">
			<input type='text' class='tgl' name='tanggal' style='width: 40% !important;' value='<?=date("Y-m-d")?>' > <div style="float:left">&nbsp;/&nbsp;</div><input type='text' style='width: 40% !important;' class='jam' name='jam' value='<?=date("G:i:s")?>' >
			</div>
			</label>
			</crow>
			
			<crow>
			<a href="javascript:void(0)" style="color:#00AFFF; text-decoration:none;" onclick="do_ket_sebelum()">Lihat keterangan tahap sebelumnya <span class="fa fa-angle-double-right"></span></a>
			<div id="keterangan-sebelum" style="display:none">
			<table style="font-size:12px;">
			<tr><td colspan="3" valign="top">Tidak Ada Keterangan</td></tr>
			</table>
			</div>
			</crow>
			<crow>
			<label><tag>Keterangan</tag>
			
			<textarea name='keterangan' style='width: 100%;' ></textarea>
			</label>
			</crow>
			<div class="clear"></div>
			
			<div class="nav-control">
				<button type="button"  onclick="
			$(this).parent().parent().parent().parent().removeClass('flipInY');
			$(this).parent().parent().parent().parent().addClass('flipOutY');
			$(this).parent().parent().parent().parent().attr('isshow','0');
			" class="btn-warning " ><span class="fa fa-times"></span>Batal</button>
				<button type="submit" style="margin-right:5%;" class="btn-success " ><span  class="fa fa-floppy-o"></span>Simpan</button>
			</div>	
			</form>
		</div>
	</div>
	<div class="clear"></div>
</div>
</crow>

<crow>

<div class="short-box bblue" > 
	<div class="sb-left">
		<div class="h-icon fa fa-briefcase fa-5x">
		</div>
	</div>
	<div class="sb-right">
		<div class="h-label">
			<span><?=$ambiltoday['jumlah']?></span>
			<i class="h-info"></i>
		</div>
	</div>
	
	<div class="h-option" onclick="$('#ambil-trans').html('');$('#amtrans_kode').val('');">
		Pendapatan Bank
		 <!-- <span class="fa fa-caret-down"> </span>-->
	</div>
	<div class="h-link">
	
		<div onclick="ajaxload('?act=transaksi_bayar')"><a href="javascript:void(0)"><span class="fa fa-pencil-square-o"> </span> Pembayaran</a></div>
		<div onclick="ajaxload('?act=transaksi_bayar&do=rekap')" ><a href="javascript:void(0)"><span class="fa fa-list"> </span> Lihat Rekap</a></div>
	</div>
	
		<div class="h-desc animated">
		<div class="desc-caret"><span class="fa fa-caret-up"> </span></div>
		<div class="over-grid-tahap" style="text-align: left; display: block;">
			
			<div method="post" style="width:100%" class="form-rows fheight-1 fbreak" >
			<crow>
			<label><tag>Nota / Customer :</tag>
			<input type="text" name="amtrans_kode" id="amtrans_kode" placeholder="Masukan Nota / Customer"  value=""  />
			</label>
			</crow>
			<div id="ambil-trans">
			</div>
			
			<div class="clear"></div>
			
			
			</div>
		</div>
	</div>
	<div class="clear"></div>
</div>
</crow>
<crow>

<div class="short-box bviolet" > 
	<div class="sb-left">
		<div class="h-icon fa fa-usd fa-5x">
		</div>
	</div>
	<div class="sb-right">
		<div class="h-label">
			<span><? echo number_format($pendapatantoday['jumlah'])?></span>
			<i class="h-info"></i>
		</div>
	</div>
	
	<div class="h-option" onclick="$('#ambil-trans').html('');$('#amtrans_kode').val('');">
		Belanja Bank
		<!-- <span class="fa fa-caret-down"> </span>-->
	</div>
	<div class="h-link">
	<!-- 	<div onclick="ajaxload('?act=transaksi_bayar')"><a href="javascript:void(0)"><span class="fa fa-pencil-square-o"> </span> Pembayaran</a></div>
		<div onclick="ajaxload('?act=transaksi_bayar&do=rekap')" ><a href="javascript:void(0)"><span class="fa fa-list"> </span> Lihat Rekap</a></div>
	 --></div>
	
	<div class="h-desc animated">
		<div class="desc-caret"><span class="fa fa-caret-up"> </span></div>
		<div class="over-grid-tahap" style="text-align: left; display: block;">
			
			<div method="post" style="width:100%" class="form-rows fheight-1 fbreak" >
			<crow><label><tag>Akun   :</tag>
			<select name="kepen_akun"  class="pen_akun" id-saldo="id_kepen_akun" >
			<? echo $info_kas;?>
			</select>
			</label></crow>
			<span style="color:#093;">&nbsp;<b>Saldo :</b> <span id="id_kepen_akun"></span></span>

			<div class="clear"></div>
			<div class="nav-control">
				<button type="button" style="margin-right:5%;" onclick="
			$(this).parent().parent().parent().parent().removeClass('flipInY');
			$(this).parent().parent().parent().parent().addClass('flipOutY');
			$(this).parent().parent().parent().parent().attr('isshow','0');
			" class="btn-primary " ><span class="fa fa-magic"></span>OK</button>
			</div>	
						
			
			</div>
		</div>
	</div>

	<div class="clear"></div>
</div>
</crow>


	<div class="clear"></div>
</div>

</div>
<div class="dash-container">
<div id="loaddash" class="loadspiner"><span class="fa fa-spinner fa-spin fa-speed-2x" style="margin-top:20px;"></span><span class="labelt">
Silahkan Tunggu, Sedang Memproses Data...</span></div>
<div class="dash-content"  id="mydashboard" >


</div>

</div>

<script>
$(document).ready(function(){
	//dashload("?act=grafik&do=pendapatan");
	// $(".h-option").click(function(){
	// 	var isshow=$(this).next().next().attr('isshow');
	// 		$(this).next().next().removeClass("flipOutY");
	// 		$(this).next().next().removeClass("flipInY");
	// 	if(isshow=='1'){
	// 		$(this).next().next().addClass("flipOutY");
	// 		$(this).next().next().attr('isshow','0');
	// 	}else{
	// 		$(this).next().next().addClass("flipInY");
	// 		$(this).next().next().attr('isshow','1');
	// 	}
	// });

	$("#intrans_kode").hlookup({
		url:'?act=lookup&do=get_transaksi',
		url_detail:'?act=lookup&do=get_transaksi_detail',
		title: 'Data Transaksi',
		onComplete:function(data){
		$("#info-trans").html("<span class='baqua' style='margin-top: 5px;padding: 2px 10px;display: inline-block;'><span class='fa fa-spinner fa-spin'></span> Loading Detail...</span>");
			$.ajax({
				url:'?act=transaksi&do=info_transaksi&kode='+data.kode,
				success:function(htm){
					$("#info-trans").html(htm);
				}
			
			});
		}
	});	
	// tahapan
	$("#thrans_kode").hlookup({
		url:'?act=lookup&do=get_transaksi&case=tahapan',
		url_detail:'?act=lookup&do=get_transaksi_detail&case=tahapan',
		title: 'Data Transaksi',
		onComplete:function(data){
		$("#tahap-trans").html("<span class='baqua' style='margin-top: 5px;padding: 2px 10px;display: inline-block;'><span class='fa fa-spinner fa-spin'></span> Loading...</span>");
			$.ajax({
				url:'?act=transaksi_tahap&do=detail_tahap&kode='+data.kode+'&id_tahapan='+data.id_tahapan,
				type:'post',
				dataType: 'json',
				success:function(datahaps){
						$("#tahap-trans").html('');
						$("#dash_id_tahapan").html(datahaps.combo);
						$("#tahapan_aktif").val(datahaps.sekarang);
						$("#id_tahapan_lama").val(data.id_tahapan);
						$("#keterangan-sebelum table").html(datahaps.keterangan);
						$("#dash_kode_trans").val(data.kode);
				}
			
			});
				
		}
	});	
	
	$('#dash-thform input[type="text"], #dash-thform select').tooltipster({
        trigger: 'custom',
        onlyOne: false,
        position: 'top'
    });
	
	$('#dash-thform').validate({
		  rules: {
				thrans_kode: {
				required: true
			  },
				tahapan_sekarang: {
				required: true
			  },
				id_tahapan: {
				required: true
			  },
				kode_karyawan: {
				required: true
			  },
				tanggal: {
				required: true,
				date: true
			  },
				jam: {
				required: true
			  }
			},	
		    errorPlacement: function (error, element) {
          		  $(element).tooltipster('update', $(error).text());
				
				($(error).text()!='') ? $(element).tooltipster('show') : $(element).tooltipster('hide');
			},
			success: function(label,element) {
				$(element).tooltipster('hide');
			 },
		    submitHandler: function (form) { 
			
				saveData('dash-thform','?act=transaksi_tahap&do=ubah','','',function(){
						
						$('#dash-thform').parent().parent().removeClass('flipInY');
						$('#dash-thform').parent().parent().addClass('flipOutY');
						$('#dash-thform').parent().parent().attr('isshow','0');
				});
      	  	}
	  }); 
	  
	  // ambil
	  
	$("#amtrans_kode").hlookup({
		url:'?act=lookup&do=get_transaksi&case=ambil',
		url_detail:'?act=lookup&do=get_transaksi_detail&case=ambil',
		title: 'Data Transaksi ',
		onComplete:function(data){
		$("#ambil-trans").html("<span class='baqua' style='margin-top: 5px;padding: 2px 10px;display: inline-block;'><span class='fa fa-spinner fa-spin'></span> Loading Data...</span>");
			$.ajax({
				url:'?act=transaksi_bayar&do=ambil&kode='+data.kode,
				success:function(htm){
					$("#ambil-trans").html(htm);
				}
			
			});
		}
	});	

		  $(".pen_akun").on('change',function(){
	  		get_saldo_akun(this);
	  });
	  $(".pen_akun").change();
});

function do_ket_sebelum(){
	$("#keterangan-sebelum").toggle();
}
function cleartahp(){
		$("#tahap-trans").html('');
		$("#dash_id_tahapan").html('');
		$("#tahapan_aktif").val('');
		$("#id_tahapan_lama").val('');
		$("#keterangan-sebelum table").html('<tr><td colspan="3" valign="top">Tidak Ada Keterangan</td></tr>');
		$("#dash_kode_trans").val('');
		$("#thrans_kode").val('');
		
}
</script>