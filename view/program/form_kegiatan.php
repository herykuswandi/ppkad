<script>
$(document).ready(function(){


$("#look_urusan").keyup(function(){
	if($(this).val()=="")
	{
		$('#id_bidang').val('');
		$('#kode_bidang').val('');
		$('#nama_bidang').html('');

		$("#look_bidang").val('');
		
		$("#look_bidang").keyup();
		$("#look_bidang").attr('readonly','readonly');
	}
});

$("#look_bidang").keyup(function(){
	if($(this).val()=="")
	{

		$('#id_program').val('');
		$('#kode_program').val('');
		$('#nama_program').html('');
		$("#look_program").val('');
		$("#look_program").attr('readonly','readonly');

	}
});



function lukup_bidang(){
		$("#look_bidang").removeAttr('readonly');
	$("#look_bidang").hlookup({
		url:'?act=lookup&do=get_bidang&id_urusan='+$('#id_urusan').val(),
		url_detail:'?act=lookup&do=get_bidang_detail&id_urusan='+$('#id_urusan').val(),
		title: 'Data bidang dari urusan '+$('#nama_urusan').html(),
		onComplete:function(data){
					$('#id_bidang').val(data.id);
					// $('#kode_bidang').val(data.kode);
					$('#nama_bidang').html(data.nama);
		

					$('#id_program').val('');
					$('#kode_program').val('');
					$('#nama_program').html('');

					$("#look_program").val('');
					lukup_program();
		}
	});

}	

function lukup_program(){
			$("#look_program").removeAttr('readonly');
		$("#look_program").hlookup({
			url:'?act=lookup&do=get_program&id_bidang='+$('#id_bidang').val(),
			url_detail:'?act=lookup&do=get_program_detail&id_bidang='+$('#id_bidang').val(),
			title: 'Data program dari bidang '+$('#nama_bidang').html(),
			onComplete:function(data){
						$('#id_program').val(data.id);
						$('#kode_program').val(data.kode);
						$('#nama_program').html(data.nama);
			}
		});
}

	<?php if($id>0){?>
		lukup_bidang();
		lukup_program();
	<?php }?>
		$("#look_urusan").hlookup({
		url:'?act=lookup&do=get_urusan',
		url_detail:'?act=lookup&do=get_urusan_detail',
		title: 'Data Urusan',
		onComplete:function(data){
					$('#id_urusan').val(data.id);
					// $('#kode_urusan').val(data.kode);
					$('#nama_urusan').html(data.nama);

					$('#id_bidang').val('');
					$('#kode_bidang').val('');
					$('#nama_bidang').html('');

					$('#id_program').val('');
					$('#kode_program').val('');
					$('#nama_program').html('');
					$("#look_program").val('');
					$("#look_program").attr('readonly','readonly');

					$("#look_bidang").val('');
					lukup_bidang();
		}
	});

	

	$('#former input[type="text"], #former select').tooltipster({
        trigger: 'custom',
        onlyOne: false,
        position: 'top'
    });
	
	$('#former').validate({
		  rules: {
			nama: {
	        required: true
	      },
			kode_progam: {
	        required: true
	      },
			kode_kegiatan: {
	        required: true
	      },
			look_urusan: {
	        required: true
	      },
			look_program: {
	        required: true
	      }
	    },	
		    errorPlacement: function (error, element) {
            $(element).tooltipster('update', $(error).text());
			($(error).text()!='') ? $(element).tooltipster('show') : $(element).tooltipster('hide');
        },
			success: function(label,element) {
			$(element).tooltipster('hide');
		 },
		    submitHandler: function (form) { 
			saveData('former','?act=program&do=simpan_kegiatan','tabel-kegiatan','',function(data){
				if(data=="failed"){
					alert("Maaf data gagal di simpan. Kode kegiatan yang anda masukan sudah ada");
				}
			});
        }
	  }); 
	
    $("#kode_kegiatan").mask("99",{
		placeholder: '_',
    });

});
</script>

<form method="post" id="former" style="width:100%">
<input type="hidden" value="<?php echo $id;?>" name="id">

<crow class="no-border"><label><tag>Urusan :</tag>
<input type="text" id="look_urusan" placeholder="Kode / Nama Urusan" name="look_urusan" size="20" value="<?php echo $detail['kode_urusan']?>"  />
<input type="hidden" id="id_urusan" name="id_urusan" value="<?php echo $detail['id_urusan']?>"  />
</label></crow>

<crow class="row-ket"><label><tag>&nbsp;</tag>
<span id="nama_urusan"><?php echo $detail['nama_urusan']?></span>
</label></crow>


<crow  class="no-border"><label><tag>Bidang :</tag>
<input type="text" id="look_bidang" placeholder="Kode / Nama Bidang" name="look_bidang" size="20" value="<?php echo $detail['kode_bidang']?>"  readonly="readonly" />
<input type="hidden" id="id_bidang" name="id_bidang" value="<?php echo $detail['id_bidang']?>"  />
</label></crow>

<crow class="row-ket"><label><tag>&nbsp;</tag>
<span id="nama_bidang"><?php echo $detail['nama_bidang']?></span>
</label></crow>



<crow  class="no-border"><label><tag>Program :</tag>
<input type="text" id="look_program" placeholder="Kode / Nama Program" name="look_program" size="20" value="<?php echo $detail['kode_program']?>"  readonly="readonly" />
<input type="hidden" id="id_program" name="id_program" value="<?php echo $detail['id_program']?>"  />
</label></crow>

<crow class="row-ket"><label><tag>&nbsp;</tag>
<span id="nama_program"><?php echo $detail['nama_program']?></span>
</label></crow>


<crow><label><tag>Kode :</tag>
<div class="group-text">
	<!-- <input type="text" size="5" name="kode_urusan" id="kode_urusan" value="<?php echo $detail['kode_urusan']; ?>" readonly="readonly"> -->
	<!-- <input type="text" size="5" name="kode_bidang" id="kode_bidang" value="<?php echo $detail['kode_bidang']; ?>" readonly="readonly"> -->
	<input type="text" size="7" name="kode_program" id="kode_program" value="<?php echo $detail['kode_program']; ?>" readonly="readonly">
	<input type="text" size="5" name="kode_kegiatan" id="kode_kegiatan" value="<?php echo substr($detail['kode'],7); ?>" >
</div>

<!-- <input type="text" name="kode" id="kode_bidang" value="<?php echo $detail['kode'];?>"  /> -->
</label></crow>

<crow><label><tag>Nama  :</tag>
<input type="text" name="nama" value="<?php echo $detail['nama'];?>" size="40" />
</label></crow>

<div class="nav-control">
<button type="submit"  class="btn-primary " ><span class="fa fa-floppy-o"></span>Simpan</button>
<button type="reset"  class="btn-info " ><span class="fa fa-share-square-o"></span>Reset</button>
<button type="button"  class="btn-danger"   onClick="closed()"><span class="fa fa-times"></span>Batal</button>
</div>
</form>	