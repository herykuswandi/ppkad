<script>
$(document).ready(function(){

	$("#look_urusan").hlookup({
		url:'?act=lookup&do=get_urusan',
		url_detail:'?act=lookup&do=get_urusan_detail',
		title: 'Data Urusan',
		onComplete:function(data){
					$('#id_urusan').val(data.id);
					$('#kode_urusan').val(data.kode);
					$('#nama_urusan').html(data.nama);
		}
	});

	$('#former input[type="text"], #former select').tooltipster({
        trigger: 'custom',
        onlyOne: false,
        position: 'top'
    });
	
	$('#former').validate({
		  rules: {
			nama: {
	        required: true
	      },
			kode_bidang: {
	        required: true
	      },
			kode_urusan: {
	        required: true
	      },
			look_urusan: {
	        required: true
	      }
	    },	
		    errorPlacement: function (error, element) {
            $(element).tooltipster('update', $(error).text());
			($(error).text()!='') ? $(element).tooltipster('show') : $(element).tooltipster('hide');
        },
			success: function(label,element) {
			$(element).tooltipster('hide');
		 },
		    submitHandler: function (form) { 
			saveData('former','?act=organisasi&do=simpan_bidang','tabel-bidang','',function(data){
				if(data=="failed"){
					alert("Maaf data gagal di simpan. Kode bidang yang anda masukan sudah ada");
				}
			});
        }
	  }); 
	
    $("#kode_bidang").mask("99",{
		placeholder: '_',
    });

});
</script>

<form method="post" id="former" style="width:100%">
<input type="hidden" value="<?php echo $id;?>" name="id">

<crow  class="no-border"><label><tag>Urusan :</tag>
<input type="text" id="look_urusan" placeholder="Kode / Nama Urusan" name="look_urusan" size="20" value="<?php echo $detail['kode_urusan']?>"  />
<input type="hidden" id="id_urusan" name="id_urusan" value="<?php echo $detail['id_urusan']?>"  />
</label></crow>

<crow class="row-ket"><label><tag>&nbsp;</tag>
<span id="nama_urusan"><?php echo $detail['nama_urusan']?></span>
</label></crow>


<crow><label><tag>Kode :</tag>
<div class="group-text">
	<input type="text" size="5" name="kode_urusan" id="kode_urusan" value="<?php echo $detail['kode_urusan']; ?>" readonly="readonly">
	<input type="text" size="5" name="kode_bidang" id="kode_bidang" value="<?php echo substr($detail['kode'],5); ?>" >
</div>

<!-- <input type="text" name="kode" id="kode_bidang" value="<?php echo $detail['kode'];?>"  /> -->
</label></crow>

<crow><label><tag>Nama  :</tag>
<input type="text" name="nama" value="<?php echo $detail['nama'];?>" size="40" />
</label></crow>

<div class="nav-control">
<button type="submit"  class="btn-primary " ><span class="fa fa-floppy-o"></span>Simpan</button>
<button type="reset"  class="btn-info " ><span class="fa fa-share-square-o"></span>Reset</button>
<button type="button"  class="btn-danger"   onClick="closed()"><span class="fa fa-times"></span>Batal</button>
</div>
</form>