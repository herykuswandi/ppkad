<script>
$(document).ready(function(){
	$('#former input[type="text"], #former select').tooltipster({
        trigger: 'custom',
        onlyOne: false,
        position: 'top'
    });
	
	$('#former').validate({
		  rules: {
			nama: {
	        required: true
	      },
			kode: {
	        required: true
	      }
	    },	
		    errorPlacement: function (error, element) {
            $(element).tooltipster('update', $(error).text());
			($(error).text()!='') ? $(element).tooltipster('show') : $(element).tooltipster('hide');
        },
			success: function(label,element) {
			$(element).tooltipster('hide');
		 },
		    submitHandler: function (form) { 
			saveData('former','?act=rek_lra&do=simpan_akun','tabel-akun','',function(data){
				if(data=="failed"){
					alert("Maaf data gagal di simpan. Kode akun yang anda masukan sudah ada");
				}
			});
        }
	  }); 
   $("#kode_akun").mask("9");
});
</script>

<form method="post" id="former" style="width:100%">
<input type="hidden" value="<?php echo $id;?>" name="id">

<crow><label><tag>Kode :</tag>
<input type="text" name="kode" id="kode_akun" size="4" value="<?php echo $detail['kode'];?>"  />
</label></crow>

<crow><label><tag>Nama  :</tag>
<input type="text" name="nama" value="<?php echo $detail['nama'];?>" size="40" />
</label></crow>

<crow><label><tag>Saldo Normal  :</tag>
<select name="saldo_normal">
	<option value="debet" <?php echo ($detail['saldo_normal']=='debet') ? 'selected="selected"' : ''; ?>>DEBET</option>
	<option value="kredit" <?php echo ($detail['saldo_normal']=='kredit') ? 'selected="selected"' : ''; ?>>KREDIT</option>
</select>
</label></crow>

<div class="nav-control">
<button type="submit"  class="btn-primary " ><span class="fa fa-floppy-o"></span>Simpan</button>
<button type="reset"  class="btn-info " ><span class="fa fa-share-square-o"></span>Reset</button>
<button type="button"  class="btn-danger"   onClick="closed()"><span class="fa fa-times"></span>Batal</button>
</div>
</form>