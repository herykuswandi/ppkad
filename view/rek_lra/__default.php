<script>
$(document).ready(function(){
		$( "#tab-rek-lra" ).tabs();
});
</script>

<div id="tab-rek-lra" style="position:inherit;">
	<ul>
		<li><a href="#akun">Akun</a></li>
		<li><a href="#kelompok">Kelompok</a></li>
		<li><a href="#jenis">Jenis</a></li>
		<li><a href="#obyek">Obyek</a></li>
		<li><a href="#referensi">Referensi</a></li>
	</ul>
	<div id="akun">
		<table class="tabel-akun" style="display: none; "></table>
		<div class="nav-control">
		   <a href="?act=rek_lra&do=form_akun" label="Tambah Akun"  title="Tambah Akun" class="linkbox">
		   		<button type="button"  class="btn-success" ><span class="fa fa-plus-circle"></span>Tambah</button>              
		   </a>
		</div>
	</div>
	<div id="kelompok">
		<table class="tabel-kelompok" style="display: none; "></table>
		<div class="nav-control">
		   <a href="?act=rek_lra&do=form_kelompok" label="Tambah Kelompok"  title="Tambah Kelompok" class="linkbox">
		   		<button type="button"  class="btn-success" ><span class="fa fa-plus-circle"></span>Tambah</button>              
		   </a>
		</div>
	</div>
	<div id="jenis">
		<table class="tabel-jenis" style="display: none; "></table>
		<div class="nav-control">
		   <a href="?act=rek_lra&do=form_jenis" label="Tambah Jenis"  title="Tambah Jenis" class="linkbox">
		   		<button type="button"  class="btn-success" ><span class="fa fa-plus-circle"></span>Tambah</button>              
		   </a>
		</div>
	</div>
	<div id="obyek">
		<table class="tabel-obyek" style="display: none; "></table>
		<div class="nav-control">
		   <a href="?act=rek_lra&do=form_obyek" label="Tambah Obyek"  title="Tambah Obyek" class="linkbox">
		   		<button type="button"  class="btn-success" ><span class="fa fa-plus-circle"></span>Tambah</button>              
		   </a>
		</div>
	</div>
	<div id="referensi">
		<table class="tabel-referensi" style="display: none; "></table>
		<div class="nav-control">
		   <a href="?act=rek_lra&do=form_referensi" label="Tambah Referensi"  title="Tambah Referensi" class="linkbox">
		   		<button type="button"  class="btn-success" ><span class="fa fa-plus-circle"></span>Tambah</button>              
		   </a>
		</div>
	</div>
</div>


