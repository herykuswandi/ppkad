<script>
$(document).ready(function(){
	$("#look_akun").keyup(function(){
		if($(this).val()=="")
		{
			$('#id_lra_kelompok').val('');
			$('#kode_kelompok').val('');
			$('#nama_kelompok').html('');

			$("#look_kelompok").val('');
			
			$("#look_kelompok").keyup();

			$("#look_kelompok").attr('readonly','readonly');
		}
	});


	function lukup_kelompok(){
		$("#look_kelompok").removeAttr('readonly');
		$("#look_kelompok").hlookup({
			url:'?act=lookup&do=get_lra_kelompok&id_lra_akun='+$('#id_lra_akun').val(),
			url_detail:'?act=lookup&do=get_lra_kelompok_detail&id_lra_akun='+$('#id_lra_akun').val(),
			title: 'Data Kelompok '+$('#nama_akun').html(),
			onComplete:function(data){
						$('#id_lra_kelompok').val(data.id);
						$('#kode_kelompok').val(data.kode);
						$('#nama_kelompok').html(data.nama);
			}
		});
	}
	<?php if($id>0){?>
		lukup_kelompok();
	<?php }?>
	$("#look_akun").hlookup({
		url:'?act=lookup&do=get_lra_akun',
		url_detail:'?act=lookup&do=get_lra_akun_detail',
		title: 'Data Akun',
		onComplete:function(data){
					$('#id_lra_akun').val(data.id);
					// $('#kode_akun').val(data.kode);
					$('#nama_akun').html(data.nama);

					$('#id_lra_kelompok').val('');
					$('#kode_kelompok').val('');
					$('#nama_kelompok').html('');

					$("#look_kelompok").val('');
					lukup_kelompok();
		}
	});

	

	$('#former input[type="text"], #former select').tooltipster({
        trigger: 'custom',
        onlyOne: false,
        position: 'top'
    });
	
	$('#former').validate({
		  rules: {
			nama: {
	        required: true
	      },
			kode_jenis: {
	        required: true
	      },
			kode_kelompok: {
	        required: true
	      },
			look_akun: {
	        required: true
	      },
			look_jenis: {
	        required: true
	      }
	    },	
		    errorPlacement: function (error, element) {
            $(element).tooltipster('update', $(error).text());
			($(error).text()!='') ? $(element).tooltipster('show') : $(element).tooltipster('hide');
        },
			success: function(label,element) {
			$(element).tooltipster('hide');
		 },
		    submitHandler: function (form) { 
			saveData('former','?act=rek_lra&do=simpan_jenis','tabel-jenis','',function(data){
				if(data=="failed"){
					alert("Maaf data gagal di simpan. Kode jenis yang anda masukan sudah ada");
				}
			});
        }
	  }); 
	
    $("#kode_jenis").mask("99",{
		placeholder: '_',
    });

});
</script>

<form method="post" id="former" style="width:100%">
<input type="hidden" value="<?php echo $id;?>" name="id">

<crow class="no-border"><label><tag>Akun :</tag>
<input type="text" id="look_akun" placeholder="Kode / Nama Akun" name="look_akun" size="20" value="<?php echo $detail['kode_akun']?>"  />
<input type="hidden" id="id_lra_akun" name="id_lra_akun" value="<?php echo $detail['id_lra_akun']?>"  />
</label></crow>

<crow class="row-ket"><label><tag>&nbsp;</tag>
<span id="nama_akun"><?php echo $detail['nama_akun']?></span>
</label></crow>


<crow  class="no-border"><label><tag>Kelompok :</tag>
<input type="text" id="look_kelompok" placeholder="Kode / Nama Kelompok" name="look_kelompok" size="20" value="<?php echo $detail['kode_kelompok']?>"  readonly="readonly" />
<input type="hidden" id="id_lra_kelompok" name="id_lra_kelompok" value="<?php echo $detail['id_lra_kelompok']?>"  />
</label></crow>

<crow class="row-ket"><label><tag>&nbsp;</tag>
<span id="nama_kelompok"><?php echo $detail['nama_kelompok']?></span>
</label></crow>



<crow><label><tag>Kode :</tag>
<div class="group-text">
	<!-- <input type="text" size="5" name="kode_akun" id="kode_akun" value="<?php echo $detail['kode_akun']; ?>" readonly="readonly"> -->
	<input type="text" size="5" name="kode_kelompok" id="kode_kelompok" value="<?php echo $detail['kode_kelompok']; ?>" readonly="readonly">
	<input type="text" size="5" name="kode_jenis" id="kode_jenis" value="<?php echo substr($detail['kode'],5); ?>" >
</div>

<!-- <input type="text" name="kode" id="kode_kelompok" value="<?php echo $detail['kode'];?>"  /> -->
</label></crow>

<crow><label><tag>Nama  :</tag>
<input type="text" name="nama" value="<?php echo $detail['nama'];?>" size="40" />
</label></crow>


<crow><label><tag>Saldo Normal  :</tag>
<select name="saldo_normal">
	<option value="debet" <?php echo ($detail['saldo_normal']=='debet') ? 'selected="selected"' : ''; ?>>DEBET</option>
	<option value="kredit" <?php echo ($detail['saldo_normal']=='kredit') ? 'selected="selected"' : ''; ?>>KREDIT</option>
</select>
</label></crow>
<div class="nav-control">
<button type="submit"  class="btn-primary " ><span class="fa fa-floppy-o"></span>Simpan</button>
<button type="reset"  class="btn-info " ><span class="fa fa-share-square-o"></span>Reset</button>
<button type="button"  class="btn-danger"   onClick="closed()"><span class="fa fa-times"></span>Batal</button>
</div>
</form>	