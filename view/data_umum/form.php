<script>
$(document).ready(function(){
	$('#former input[type="text"], #former select').tooltipster({
        trigger: 'custom',
        onlyOne: false,
        position: 'top'
    });
	
	$('#former').validate({
		  rules: {
			nama_pemda: {
	        required: true
	      },
			ibukota_pemda: {
	        required: true
	      },
			alamat_pemda: {
	        required: true
	      }
	    },	
		    errorPlacement: function (error, element) {
            $(element).tooltipster('update', $(error).text());
			($(error).text()!='') ? $(element).tooltipster('show') : $(element).tooltipster('hide');
        },
			success: function(label,element) {
			$(element).tooltipster('hide');
		 },
		    submitHandler: function (form) { 
			saveData('former','?act=data_umum&do=simpan','','',function(data){
				if(data=="failed"){
					alert("Maaf data gagal di simpan. data umum pada tahun anggaran tersebut sudah ada");
				}else{
					alert("Data Berhasil Disimpan.");
					ajaxload('?act=data_umum')
				}
			});
        }
	  }); 

	var uploaders = new qq.FileUploader({
		element: document.getElementById('filefoto'),
		action: 'model/file_uploader.php',			
		debug: true,
		onComplete: function(id, fileName, data){
			if(data['success']){
			var n='files/'+data['nama'];
			$('#foto').val(n);
			$('#srcfoto').hide();
			$('#srcfoto').fadeIn(1000);
			$('#srcfoto').attr('src',n)
			}
			hideLoad();
			$("#filefoto").next('span').html(fileName);
		},
		onProgress: function(id, fileName, loaded, total){
			console.log(total);
			console.log(loaded);
			$("#filefoto").next('span').html(Math.round(loaded*100/total)+" %");
		},
		onSubmit: function(id, fileName){
			showLoad("mmb","Silahkan Tunggu, Sedang Meng-Upload File.");
			$("#filefoto").next('span').html("Starting...");
		}
	});

});
</script>

<form method="post" id="former" style="width:100%">
<input type="hidden" value="<?php echo $id;?>" name="id">
<crow><label><tag>Tahun Anggaran :</tag>
<select name="id_ta">
<?php foreach($tang as $tan){?>
<option value="<?php echo $tan['id'];?>" <?php echo ($tan['id']==$detail['id_ta']) ? 'selected="selected"' : '';?>><?php echo $tan['tahun'];?></option>
<?php }?>
</select>	
</label></crow>
<crow><label><tag>Nama Pemda :</tag>
<input type="text" name="nama_pemda" value="<?php echo $detail['nama_pemda']?>" size="40" />
</label></crow>
<crow><label><tag>Ibukota Pemda :</tag>
<input type="text" name="ibukota_pemda" value="<?php echo $detail['ibukota_pemda']?>" size="40" />
</label></crow>

<crow><label><tag>Alamat Pemda:</tag>
<input type="text" name="alamat_pemda" value="<?php echo $detail['alamat_pemda']?>" size="80" />
</label></crow>


<fieldset class="form-rows fheight-3 fbreak" id="instrans">
<legend>Kepala Daerah</legend>
<crow><label><tag>Nama :</tag>
<input type="text" name="nama_kepda" value="<?php echo $detail['nama_kepda']?>" size="40" />
</label></crow>
<crow><label><tag>NIP :</tag>
<input type="text" name="nip_kepda" value="<?php echo $detail['nip_kepda']?>" size="40" />
</label></crow>
<crow><label><tag>Jabatan :</tag>
<input type="text" name="jabatan_kepda" value="<?php echo $detail['jabatan_kepda']?>" size="40" />
</label></crow>
<div class="clear"></div>
</fieldset>

<fieldset class="form-rows fheight-3 fbreak" id="instrans">
<legend>Sekretaris Daerah</legend>
<crow><label><tag>Nama :</tag>
<input type="text" name="nama_sekda" value="<?php echo $detail['nama_sekda']?>" size="40" />
</label></crow>
<crow><label><tag>NIP :</tag>
<input type="text" name="nip_sekda" value="<?php echo $detail['nip_sekda']?>" size="40" />
</label></crow>
<crow><label><tag>Jabatan :</tag>
<input type="text" name="jabatan_sekda" value="<?php echo $detail['jabatan_sekda']?>" size="40" />
</label></crow>
<div class="clear"></div>
</fieldset>

<fieldset class="form-rows fheight-3 fbreak" id="instrans">
<legend>Kepala Badan/Bagian Keuangan</legend>
<crow><label><tag>Nama :</tag>
<input type="text" name="nama_kepkeu" value="<?php echo $detail['nama_kepkeu']?>" size="40" />
</label></crow>
<crow><label><tag>NIP :</tag>
<input type="text" name="nip_kepkeu" value="<?php echo $detail['nip_kepkeu']?>" size="40" />
</label></crow>
<crow><label><tag>Jabatan :</tag>
<input type="text" name="jabatan_kepkeu" value="<?php echo $detail['jabatan_kepkeu']?>" size="40" />
</label></crow>
<div class="clear"></div>
</fieldset>

<fieldset class="form-rows fheight-3 fbreak" id="instrans">
<legend>Kepala Bagian/Sub Bagian Anggaran</legend>
<crow><label><tag>Nama :</tag>
<input type="text" name="nama_kepang" value="<?php echo $detail['nama_kepang']?>" size="40" />
</label></crow>
<crow><label><tag>NIP :</tag>
<input type="text" name="nip_kepang" value="<?php echo $detail['nip_kepang']?>" size="40" />
</label></crow>
<crow><label><tag>Jabatan :</tag>
<input type="text" name="jabatan_kepang" value="<?php echo $detail['jabatan_kepang']?>" size="40" />
</label></crow>
<div class="clear"></div>
</fieldset>

<fieldset class="form-rows fheight-3 fbreak" id="instrans">
<legend>Kepala Bagian/Sub Bagian Verifikasi</legend>
<crow><label><tag>Nama :</tag>
<input type="text" name="nama_kepver" value="<?php echo $detail['nama_kepver']?>" size="40" />
</label></crow>
<crow><label><tag>NIP :</tag>
<input type="text" name="nip_kepver" value="<?php echo $detail['nip_kepver']?>" size="40" />
</label></crow>
<crow><label><tag>Jabatan :</tag>
<input type="text" name="jabatan_kepver" value="<?php echo $detail['jabatan_kepver']?>" size="40" />
</label></crow>
<div class="clear"></div>
</fieldset>

<fieldset class="form-rows fheight-3 fbreak" id="instrans">
<legend>Kepala Bagian/Sub Bagian Perbendaharaan</legend>
<crow><label><tag>Nama :</tag>
<input type="text" name="nama_kepben" value="<?php echo $detail['nama_kepben']?>" size="40" />
</label></crow>
<crow><label><tag>NIP :</tag>
<input type="text" name="nip_kepben" value="<?php echo $detail['nip_kepben']?>" size="40" />
</label></crow>
<crow><label><tag>Jabatan :</tag>
<input type="text" name="jabatan_kepben" value="<?php echo $detail['jabatan_kepben']?>" size="40" />
</label></crow>
<div class="clear"></div>
</fieldset>

<fieldset class="form-rows fheight-3 fbreak" id="instrans">
<legend>Kepala Bagian/Sub Bagian Pembukuan</legend>
<crow><label><tag>Nama :</tag>
<input type="text" name="nama_kepbuk" value="<?php echo $detail['nama_kepbuk']?>" size="40" />
</label></crow>
<crow><label><tag>NIP :</tag>
<input type="text" name="nip_kepbuk" value="<?php echo $detail['nip_kepbuk']?>" size="40" />
</label></crow>
<crow><label><tag>Jabatan :</tag>
<input type="text" name="jabatan_kepbuk" value="<?php echo $detail['jabatan_kepbuk']?>" size="40" />
</label></crow>
<div class="clear"></div>
</fieldset>

<fieldset class="form-rows fheight-3 fbreak" id="instrans">
<legend>Kuasa BUD</legend>
<crow><label><tag>Nama :</tag>
<input type="text" name="nama_kepbud" value="<?php echo $detail['nama_kepbud']?>" size="40" />
</label></crow>
<crow><label><tag>NIP :</tag>
<input type="text" name="nip_kepbud" value="<?php echo $detail['nip_kepbud']?>" size="40" />
</label></crow>
<crow><label><tag>Jabatan :</tag>
<input type="text" name="jabatan_kepbud" value="<?php echo $detail['jabatan_kepbud']?>" size="40" />
</label></crow>
<div class="clear"></div>
</fieldset>

<fieldset class="form-rows fheight-3 fbreak" id="instrans">
<legend>Atasan Langsung Kuasa BUD</legend>
<crow><label><tag>Nama :</tag>
<input type="text" name="nama_atbud" value="<?php echo $detail['nama_atbud']?>" size="40" />
</label></crow>
<crow><label><tag>NIP :</tag>
<input type="text" name="nip_atbud" value="<?php echo $detail['nip_atbud']?>" size="40" />
</label></crow>
<crow><label><tag>Jabatan :</tag>
<input type="text" name="jabatan_atbud" value="<?php echo $detail['jabatan_atbud']?>" size="40" />
</label></crow>
<div class="clear"></div>
</fieldset>
<crow><label><tag>Logo :</tag>
<div id="filefoto" class="btn-primary btn-files "></div>
<span  style="font-size:12px; font-weight:bold; padding-left:10px;float:left;"></span>
<div class="mmb">
</div>
<input type="hidden" class=""  id="foto" name="logo" value="<?php echo (is_file($detail['logo'])) ? $detail['logo'] : 'assets/images/avt.jpg' ?>" size="">
</label></crow>
<crow><label><tag>&nbsp;</tag>
<img src="<?php echo (is_file($detail['logo'])) ? $detail['logo'] : 'assets/images/avt.jpg' ?>" id="srcfoto" style="width:150px; margin-top:5px; border:solid 1px #09F; ">
</label></crow>

<br />


<div class="nav-control">
<button type="submit"  class="btn-primary " ><span class="fa fa-floppy-o"></span>Simpan</button>
<button type="reset"  class="btn-info " ><span class="fa fa-share-square-o"></span>Reset</button>
<button type="button"  class="btn-danger"   onClick="ajaxload('?act=data_umum')"><span class="fa fa-times"></span>Batal</button>
</div>
</form>