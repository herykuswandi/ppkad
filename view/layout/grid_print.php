<?php
if($_REQUEST['n']=='excel'){
header_excel($title.'-'.date('Ymd-gis'));
$noprint=1;
}
?>
<style>
.table-print,.table-header{
font-size:12px;
width:1000px;
border:solid 1px #ddf;
border-collapse:inherit;
border-spacing:0px;
}
.table-header{
border:none;
text-align:center;
}
.table-header td{
padding:3px 0px;
text-align:center;
vertical-align: top;
}
.table-print th{
text-align:left;vertical-align: top;
font-weight:bold;
padding:5px 3px;

border-bottom:solid 1px #ddf;
}
.table-print td{
border-top:solid 1px #ddf;
padding:3px;vertical-align: top;
}

</style>
<?php
// prepare for counter
$pcounter=$settings['counter'];
$vcounter=array();
if(is_array($pcounter))
foreach($pcounter as $keycounter=>$veycounter){
	$vcounter[$keycounter]=$veycounter[3];
}
?>
<table class="table-header">
<tr><?php
if($settings['number']==true){

?>
<td width="3%" style="padding:0px">
</td>
<?php
}
$outlet=$_SESSION['sinad']['outlet'];
$colspan=0;
  foreach($grid['fields'] as $cpfield){
  if($cpfield['printable']==true){
  ?>
  <td style=" width:<?php echo(100*$cpfield['width']/1000)?>%; padding:0px;">
  </td>
  <?php
  $colspan++;
  }
  }
  if($settings['number']==true){
  $colspan++;
  }
?>
</tr>
<tr>
<td colspan="<?php echo$colspan?>" style="font-size:14px; font-weight:bold;">
<?php echo$outlet['nama'];?>
</td>
</tr>
<tr>
<td colspan="<?php echo$colspan?>" style="font-style:italic;">
<?php echo$outlet['slogan'];?>
</td>
</tr>
<tr>
<td colspan="<?php echo$colspan?>">
<?php echo$outlet['alamat'].' - '.$outlet['kota'];?>
</td>
</tr>
<tr>
<td colspan="<?php echo$colspan?>" style="border-bottom:double 4px #000; padding-bottom:5px;">
Telp <?php echo$outlet['telp'];?><?php echo($outlet['kontak']!="") ? ', '.$outlet['kontak'] : '';?>
</td>
</tr>
<tr>
<td colspan="<?php echo$colspan?>" style="font-weight:bold; text-decoration:underline;">
<?php echo$title?>
</td>
</tr>
<tr>
<?php
foreach($filter as $lfilter=>$pfilter){?>

<tr>
<td colspan="2" style="text-align:left;font-weight:bold;"><?php echo $lfilter;?>
</td>
<td colspan="<?php echo($colspan-2)?>" style="text-align:left;"><?php	echo $pfilter;?>
</td>
</tr>
<?php }?>
<tr>
<td colspan="<?php echo$colspan?>">&nbsp;

</td>
</tr>
</table>
<!-- eof header
start content tabel
-->
<table class="table-print">
  <tr>
  
  <?php
  if($settings['number']==true){
  ?>
  <th width="1%">
  No.
  </th>
  <?php
  }
  
  foreach($grid['fields'] as $pfield){
  if($pfield['printable']==true){
  ?>
    <th style=" width:<?php echo(100*$pfield['width']/1000)?>%;"><?php echo$pfield['display']?></th>
<?php }
}?>
   
  </tr>
  <?php
  $nomor=0;
  if(is_array($data['rows']))
  foreach($data['rows'] as $prow){
  $nomor++;
  ?>
  <tr>
  
  
  <?php
  if($settings['number']==true){
  ?>
  <td>
  <?php echo$nomor?>
  </td>
  <?php
  }
  
  $nrow=0;
  	foreach($grid['fields'] as $pfield){
	  if($pfield['printable']==true){
		  if(isset($pcounter[$pfield['name']])){
		  	if($pcounter[$pfield['name']][1]=='rupiah'){
				$vcounter[$pfield['name']]+=derupiah($prow['cell'][$nrow]);
			}else{
				$vcounter[$pfield['name']]+=$prow['cell'][$nrow];
			}
		  }
	  ?>
		<td style="text-align:<?php echo$pfield['align']?>;"><?php echo$prow['cell'][$nrow]?></th>
	<?php 
	}
	$nrow++;
	}?>
  </tr>
  <?php }else{?>
  
<tr>
<td colspan="<?php echo$colspan?>" style=" text-align:center;font-style:italic;">
Data Kosong.
</td>
</tr>
<tr>
  <?php }?>
</table>
<!-- eof content tabel-->

<table class="table-header">
<tr >

<?php
if($settings['number']==true){

?>
<td width="3%" style="padding:0px">
</td>
<?php
}
$colspan=0;
  foreach($grid['fields'] as $cpfield){
  if($cpfield['printable']==true){
  ?>
  
  <td style=" width:<?php echo(100*$cpfield['width']/1000)?>%;padding:0px;">
  </td>
  <?php
  $colspan++;
  }
  }
  if($settings['number']==true){
  $colspan++;
  }
?>
</tr>
<tr>
<td colspan="<?php echo$colspan?>">&nbsp;

</td>
</tr>

<?php
// prepare for number
if($settings['number']==true){

?>
<tr>
<td colspan="2" style="text-align:left;font-weight:bold;">Jumlah
</td>
<td colspan="<?php echo($colspan-2)?>" style="text-align:left;"><?php	
echo $nomor; 
?> data
</td>
</tr>
<?php }?>

<?php
// prepare for counter
if(is_array($pcounter))
foreach($pcounter as $namecounter=>$opscounter){
	

?>
<tr>
<td colspan="2" style="text-align:left;font-weight:bold;"><?php echo $opscounter[0];?></td>
<td colspan="<?php echo($colspan-2)?>" style="text-align:left;">
<?php	
$hecho=$vcounter[$namecounter];
if($opscounter[1]=="rupiah"){
	$hecho=rupiah($hecho);
}
if($opscounter[2]!=""){
	$hecho=str_replace('{'.$namecounter.'}',$hecho,$opscounter[2]);
	echo $hecho;
}else{
	echo $hecho;
}
?>
</td>
</tr>
<?php }?>
</table>

<?php
if(!$noprint){
header_print();
}
?>