<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
     <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
  <link rel="stylesheet" type="text/css" href="assets/css/flexigrid.css" />
        <link rel="stylesheet" type="text/css" href="assets/css/jquery-ui-1.10.3.custom.css" />
        <link rel="stylesheet" type="text/css" href="assets/css/slicknav.css" />
        <link rel="stylesheet" type="text/css" href="assets/css/validasi.css" />
        <link rel="stylesheet" type="text/css" href="assets/css/font-awesome.css" />
        <link rel="stylesheet" type="text/css" href="assets/css/animate.css" />
        <link rel="stylesheet" type="text/css" href="assets/css/select2.css" />
        <!-- timepicker -->
		<link rel="stylesheet" href="assets/css/jquery.ui.timepicker.css" type="text/css" />
        <link rel="stylesheet" type="text/css" href="assets/css/style.css" />
		
</head>

<body id="bodi" class="allblur" >


<div id="firtsload">
<span class="firstloadbel"><img src="<?php echo get_ses('outlet','logo');?>"  ><br />
Hai, Selamat Datang <?php echo get_ses('profil','nama');?><br />
<span class="fa fa-spinner fa-spin fa-speed-2x"></span><i>Memuat Data ...</i></span></div>
  <div class="modals  gocontent" style="display:none; ">
          <div class="grid-title " >
            <div class="pull-left title-modals" id="title-modal"  >Judul Modal</div>
          </div>
          
          <div class="modal_wrapper grid-content conteks" style="margin-top:40px;">
          
          </div>
</div>

<div class="left-menu" >
<div class="nadnav" >
<div class="icon-nav">
<a href="javascript:setSideOn()"><span class="fa fa-lg fa-bars"></span>
</a>
</div>
<div class="switch-nav">
<a label="Periode Tahun Anggaran" class="switch-month" href="javascript:void(0)" ><?php
$arrbulans=array('','Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember');
echo $arrbulans[$_SESSION['bulan']].' '.$_SESSION['tahun'];
?>
<select name="master_tahun_anggaran" id="master_tahun">
<?php foreach($mastang as $tan){?>
<option value="<?php echo $tan['id'];?>" <?php echo ($tan['id']==$_SESSION['sinad']['id_ta']) ? 'selected="selected"' : '';?>><?php echo $tan['tahun'];?></option>
<?php }?>
</select> 

</a>

<!-- <div class="switch-box">
<div class="switch-wrap">

<div class="switch-left" onclick="setSideOn()">
</div>
<div class="switcher" >
</div>

<div class="switch-right"  onclick="setSideOn()">
</div>

</div>
</div> -->
</div>
<!-- <a label="Periode Bulan & Tahun"  class="edit-month linkbox" href="?act=bulan"><span class=" fa fa-pencil-square-o fa-lg"></span></a>
 -->
</div>

    <ul id="firstmenu">
  
          <li class="toogle-first " onclick="ajaxload('?act=dashboard');"  >
         <div class="accordion-heading "> <a href="javascript:void()" class="accordion-toggle"> 
<span class="icon-menu fa fa-dashboard fa-lg"></span>
<span class="label-menu">Dashboard</span>
          </a> </div>
        </li> 
    <li class="toogle-first ">
         <div class="accordion-heading"> <a href="javascript:void(0);" class="accordion-toggle"> 
<span class="icon-menu fa fa-magic fa-lg"></span>
<span class="label-menu"> Setting</span>
<span class="panah-menu"></span>
          </a> </div>
              <ul class="sub-ul-menu" > 
                    <li > <a  class="ajaxurl"  href="?act=tahun_anggaran"> Tahun Anggaran </a> </li>
                    <li > <a  class="ajaxurl"  href="?act=akses" >Group User </a> </li>
                    <li > <a  class="ajaxurl"  href="?act=user"> User </a> </li>
                    <li > <a  class="ajaxurl"  href="?act=data_umum"> Data Umum Pemda </a> </li>
                    <li > <a  class="ajaxurl"  href="?act=jenis_tim_anggaran"> Jenis Tim Anggaran </a> </li>
                    <li > <a  class="ajaxurl"  href="?act=tim_anggaran"> Tim Anggaran </a> </li>
                    <li > <a  class="ajaxurl"  href="?act=ttd_dokumen"> Penandatangan Dokumen </a> </li>
                    <li > <a  class="ajaxurl"  href="?act=perda"> Peraturan Daerah </a> </li>
              </ul>
        </li>
    
        <li class="toogle-first">
         <div class="accordion-heading"> <a href="javascript:void(0);" class="accordion-toggle"> 
<span class="icon-menu fa fa-gear fa-lg"></span>
<span class="label-menu">Master Data</span>
<span class="panah-menu"></span>
          </a> </div>
                    <ul class="sub-ul-menu" > 
                    <li> <a  class="ajaxurl"  href="?act=satuan">Satuan Barang</a> </li>
                    <li> <a  class="ajaxurl"  href="?act=potongan">Potongan</a> </li>
                    <li> <a  class="ajaxurl"  href="?act=organisasi">Unit Organisasi</a> </li>
                    <!-- <li> <a  class="ajaxurl"  href="?act=instansi">Tabel Instansi</a> </li> -->
                    <li> <a  class="ajaxurl"  href="?act=program">Program Kegiatan</a> </li>
                    <li> <a  class="ajaxurl"  href="?act=rek_apbd">Rekening APBD </a> </li>
                    <li> <a  class="ajaxurl"  href="?act=korolari">Korolari</a> </li>
                    <li> <a  class="ajaxurl"  href="?act=rek_lra">Rekening LRA</a> </li>
                    <li> <a  class="ajaxurl"  href="?act=rek_spm">Rekening Potongan SPM</a> </li>

                    <li> <a  class="ajaxurl"  href="?act=sumber_dana">Sumber Dana</a> </li>
                    <li> <a  class="ajaxurl"  href="?act=bank">Bank</a> </li>
                    <li> <a  class="ajaxurl"  href="?act=rek_std">Standar Harga</a> </li>
              </ul>
        </li>
       
        <li class="toogle-first ">
         <div class="accordion-heading"> <a href="javascript:void(0);" class="accordion-toggle"> 
<span class="icon-menu fa fa-tags fa-lg"></span>
<span class="label-menu">Input</span>
<span class="panah-menu"></span>
          </a> </div>
          <ul class="sub-ul-menu" > 
              <li class="sub-menu-tiga"> <a    href="javascript:void(0)"> SKPKD
                      <span class="panah-menu"></span>
                   </a> 
                  <ul class="sub-ul-menu-tiga" > 
                      <li class="sub-menu-empat">
                           <a  href="javascript:void(0)"> Anggaran
                              <span class="panah-menu"></span>
                           </a> 
                          <ul class="sub-ul-menu-empat" >
                              <li class="sub-menu-lima"> <a  href="javascript:void(0)"> Export / Import Data
                                    <span class="panah-menu"></span>
                                 </a> 
                                  <ul class="sub-ul-menu-lima" > 
                                    <li> <a  class="ajaxurl"  href="?act=renstra"> Renstra</a> </li>
                                    <li> <a  class="ajaxurl"  href="?act=layanan"> Anggaran </a> </li>
                                    <li> <a  class="ajaxurl"  href="?act=layanan"> DPA & PPA </a> </li>
                                  </ul>
                              </li>
                              <li> <a  class="ajaxurl"  href="?act=layanan"> Posting Data Anggaran</a> </li>
                              <li> <a  class="ajaxurl"  href="?act=layanan"> DPA SKPD</a> </li>
                          </ul> 
                      </li>    
                      <li class="sub-menu-empat">
                           <a  href="javascript:void(0)"> BUD
                              <span class="panah-menu"></span>
                           </a> 
                          <ul class="sub-ul-menu-empat" >
                              <li class="sub-menu-lima"> <a  href="javascript:void(0)"> Export Data
                                    <span class="panah-menu"></span>
                                 </a> 
                                  <ul class="sub-ul-menu-lima" > 
                                    <li> <a  class="ajaxurl"  href="?act=renstra"> SPD</a> </li>
                                    <li> <a  class="ajaxurl"  href="?act=layanan"> SP2D </a> </li>
                                    <li> <a  class="ajaxurl"  href="?act=layanan"> STS </a> </li>
                                  </ul>
                              </li>
                              <li class="sub-menu-lima"> <a  href="javascript:void(0)"> Import Data
                                    <span class="panah-menu"></span>
                                 </a> 
                                  <ul class="sub-ul-menu-lima" > 
                                    <li> <a  class="ajaxurl"  href="?act=renstra"> SPM</a> </li>
                                    <li> <a  class="ajaxurl"  href="?act=layanan"> Panjar </a> </li>
                                    <li> <a  class="ajaxurl"  href="?act=layanan"> SPJ Panjar </a> </li>
                                    <li> <a  class="ajaxurl"  href="?act=layanan"> Pajak </a> </li>
                                    <li> <a  class="ajaxurl"  href="?act=layanan"> Bukti Penerimaan </a> </li>
                                    <li> <a  class="ajaxurl"  href="?act=layanan"> STS </a> </li>
                                  </ul>
                              </li>
                              <li> <a  class="ajaxurl"  href="?act=layanan"> Anggaran Kas</a> </li>
                              <li> <a  class="ajaxurl"  href="?act=layanan"> Pembuatan SPD</a> </li>
                              <li> <a  class="ajaxurl"  href="?act=layanan"> SPP Non Anggaran</a> </li>
                              <li> <a  class="ajaxurl"  href="?act=layanan"> SPM Non Anggaran</a> </li>
                              <li> <a  class="ajaxurl"  href="?act=layanan"> Pembuatan SP2D</a> </li>
                              <li> <a  class="ajaxurl"  href="?act=layanan"> Daftar Penguji SP2D</a> </li>
                              <li> <a  class="ajaxurl"  href="?act=layanan"> Realisasi Pencairan SP2D</a> </li>
                              <li> <a  class="ajaxurl"  href="?act=layanan"> Penerimaan Pendapatan</a> </li>
                              <li> <a  class="ajaxurl"  href="?act=layanan"> Penerimaan Pembayaran</a> </li>
                              <li> <a  class="ajaxurl"  href="?act=layanan"> Setoran Sisa UP</a> </li>
                              <li> <a  class="ajaxurl"  href="?act=layanan"> Transfer Antar Bank</a> </li>
                          </ul> 
                      </li>    
                      <li class="sub-menu-empat">
                           <a  href="javascript:void(0)"> Pembukuan
                              <span class="panah-menu"></span>
                           </a> 
                          <ul class="sub-ul-menu-empat" >
                              <li> <a  class="ajaxurl"  href="?act=layanan"> Jurnal</a> </li>
                              <li> <a  class="ajaxurl"  href="?act=layanan"> Penyesuaian Pendapatan</a> </li>
                              <li> <a  class="ajaxurl"  href="?act=layanan"> Saldo Awal</a> </li>
                              <li> <a  class="ajaxurl"  href="?act=layanan"> Posting Data</a> </li>
                              <li class="sub-menu-lima"> <a  href="javascript:void(0)"> Import / Export
                                    <span class="panah-menu"></span>
                                 </a> 
                                  <ul class="sub-ul-menu-lima" > 
                                    <li> <a  class="ajaxurl"  href="?act=renstra"> Saldo Awal</a> </li>
                                    <li> <a  class="ajaxurl"  href="?act=layanan"> Posting Jurnal </a> </li>
                                  </ul>
                              </li>
                          </ul> 
                      </li>    
                  </ul>
              </li> 
              <li class="sub-menu-tiga"> <a    href="javascript:void(0)"> SKPD
                      <span class="panah-menu"></span>
                   </a> 
                  <ul class="sub-ul-menu-tiga" > 
                      <li class="sub-menu-empat">
                           <a  href="javascript:void(0)"> Anggaran
                              <span class="panah-menu"></span>
                           </a> 
                          <ul class="sub-ul-menu-empat" >
                              <li> <a  class="ajaxurl"  href="?act=layanan"> Renstra SKPD</a> </li>
                              <li class="sub-menu-lima"> <a  href="javascript:void(0)"> RKA SKPD
                                    <span class="panah-menu"></span>
                                 </a> 
                                  <ul class="sub-ul-menu-lima" > 
                                        <li class="sub-menu-enam"> <a  href="javascript:void(0)"> Export / Import Data
                                            <span class="panah-menu"></span>
                                         </a> 
                                          <ul class="sub-ul-menu-enam" > 
                                            <li> <a  class="ajaxurl"  href="?act=renstra"> Parameter Program Kegiatan</a> </li>
                                            <li> <a  class="ajaxurl"  href="?act=layanan"> Renstra </a> </li>
                                            <li> <a  class="ajaxurl"  href="?act=layanan"> Indikator </a> </li>
                                            <li> <a  class="ajaxurl"  href="?act=layanan"> Anggaran </a> </li>
                                            <li> <a  class="ajaxurl"  href="?act=layanan"> Anggaran Kas </a> </li>
                                            <li> <a  class="ajaxurl"  href="?act=layanan"> DPA&DPPA </a> </li>
                                          </ul>
                                      </li>
                                  </ul>
                              </li>
                              <li> <a  class="ajaxurl"  href="?act=layanan"> Anggaran Kas</a> </li>

                          </ul> 
                      </li>   
                       <li class="sub-menu-empat">
                           <a  href="javascript:void(0)"> Tata Usaha
                              <span class="panah-menu"></span>
                           </a> 
                          <ul class="sub-ul-menu-empat" >
                              <li class="sub-menu-lima"> <a  href="javascript:void(0)"> Export/Import Data
                                    <span class="panah-menu"></span>
                                 </a> 
                                  <ul class="sub-ul-menu-lima" > 
                                        <li> <a  class="ajaxurl"  href="?act=renstra"> SPM</a> </li>
                                        <li> <a  class="ajaxurl"  href="?act=layanan"> SPD </a> </li>
                                        <li> <a  class="ajaxurl"  href="?act=layanan"> SP2D </a> </li>
                                  </ul>
                              </li>
                              
                              <li> <a  class="ajaxurl"  href="?act=layanan">Verifikasi SPP</a> </li>
                              <li> <a  class="ajaxurl"  href="?act=layanan">Pembuatan SPM</a> </li>
                              <li> <a  class="ajaxurl"  href="?act=layanan">Pengesahan SPJ</a> </li>
                          </ul> 
                      </li>    
                      <li class="sub-menu-empat">
                           <a  href="javascript:void(0)"> Bendahara
                              <span class="panah-menu"></span>
                           </a> 
                          <ul class="sub-ul-menu-empat" >
                              <li class="sub-menu-lima"> <a  href="javascript:void(0)"> Pengeluaran
                                    <span class="panah-menu"></span>
                                 </a> 
                                  <ul class="sub-ul-menu-lima" > 
                                    <li> <a  class="ajaxurl"  href="?act=renstra"> Pembuatan SPP  </a> </li>
                                    <li> <a  class="ajaxurl"  href="?act=layanan"> Panjar & SPJ Panjar </a> </li>
                                    <li> <a  class="ajaxurl"  href="?act=layanan"> SPJ </a> </li>
                                    <li> <a  class="ajaxurl"  href="?act=layanan"> Pajak </a> </li>
                                    <li> <a  class="ajaxurl"  href="?act=layanan"> Export Data Panjar/SPJ Panjar/ Pajak </a> </li>
                                  </ul>
                              </li>
                              <li class="sub-menu-lima"> <a  href="javascript:void(0)"> Penerimaan
                                    <span class="panah-menu"></span>
                                 </a> 
                                  <ul class="sub-ul-menu-lima" > 
                                    <li> <a  class="ajaxurl"  href="?act=renstra"> Bukti Penerimaan  </a> </li>
                                    <li> <a  class="ajaxurl"  href="?act=layanan"> STS </a> </li>
                                    <li> <a  class="ajaxurl"  href="?act=layanan"> Export / Import Data Bukti Penerimaan / STS </a> </li>
                                  </ul>
                              </li>
                          </ul> 
                      </li>    
                      <li class="sub-menu-empat">
                           <a  href="javascript:void(0)"> Pembukuan
                              <span class="panah-menu"></span>
                           </a> 
                          <ul class="sub-ul-menu-empat" >
                              <li> <a  class="ajaxurl"  href="?act=layanan"> Jurnal</a> </li>
                              <li> <a  class="ajaxurl"  href="?act=layanan"> Penyesuaian Belanja</a> </li>
                              <li> <a  class="ajaxurl"  href="?act=layanan"> Penyesuaian Pendapatan</a> </li>
                              <li> <a  class="ajaxurl"  href="?act=layanan"> Saldo Awal</a> </li>
                              <li> <a  class="ajaxurl"  href="?act=layanan"> Posting Data</a> </li>
                              <li class="sub-menu-lima"> <a  href="javascript:void(0)"> Import / Export
                                    <span class="panah-menu"></span>
                                 </a> 
                                  <ul class="sub-ul-menu-lima" > 
                                    <li> <a  class="ajaxurl"  href="?act=renstra"> Saldo Awal</a> </li>
                                    <li> <a  class="ajaxurl"  href="?act=layanan"> Posting Jurnal </a> </li>
                                  </ul>
                              </li>
                          </ul> 
                      </li>    
                  </ul>
              </li> 
          </ul>
        </li>        

     
       
        <li class="toogle-first ">
         <div class="accordion-heading"> <a href="javascript:void(0);" class="accordion-toggle"> 
<span class="icon-menu fa fa-clipboard fa-lg"></span>
<span class="label-menu">Laporan</span>
<span class="panah-menu"></span>
          </a> </div>
          <ul class="sub-ul-menu" > 
              <li class="sub-menu-tiga"> <a    href="javascript:void(0)"> SKPKD
                      <span class="panah-menu"></span>
                   </a> 
                  <ul class="sub-ul-menu-tiga" > 
                      <li class="sub-menu-empat">
                           <a  href="javascript:void(0)"> Anggaran
                              <span class="panah-menu"></span>
                           </a> 
                          <ul class="sub-ul-menu-empat" >
                              <li> <a  class="ajaxurl"  href="?act=layanan"> Unit Organisasi</a> </li>
                              <li> <a  class="ajaxurl"  href="?act=layanan"> RAPERDA APBD</a> </li>
                              <li> <a  class="ajaxurl"  href="?act=layanan"> PERDA APBD</a> </li>
                              <li> <a  class="ajaxurl"  href="?act=layanan"> Penjabaran APBD</a> </li>
                              <li> <a  class="ajaxurl"  href="?act=layanan"> DPA SKPD</a> </li>
                          </ul> 
                      </li>    
                      <li class="sub-menu-empat">
                           <a  href="javascript:void(0)"> BUD
                              <span class="panah-menu"></span>
                           </a> 
                          <ul class="sub-ul-menu-empat" >
                              <li> <a  class="ajaxurl"  href="?act=layanan"> SPD</a> </li>
                              <li> <a  class="ajaxurl"  href="?act=layanan"> BUD</a> </li>
                          </ul> 
                      </li>    
                      <li class="sub-menu-empat">
                           <a  href="javascript:void(0)"> Pembukuan
                              <span class="panah-menu"></span>
                           </a> 
                          <ul class="sub-ul-menu-empat" >
                              <li> <a  class="ajaxurl"  href="?act=layanan"> PERDA</a> </li>
                              <li> <a  class="ajaxurl"  href="?act=layanan"> Penjabaran</a> </li>
                          </ul> 
                      </li>    
                      <li> <a  class="ajaxurl"  href="?act=layanan"> Pertanggungjawaban</a> </li>
                  </ul>
              </li> 
              <li class="sub-menu-tiga"> <a    href="javascript:void(0)"> SKPD
                      <span class="panah-menu"></span>
                   </a> 
                  <ul class="sub-ul-menu-tiga" > 
                      <li class="sub-menu-empat">
                           <a  href="javascript:void(0)"> Anggaran
                              <span class="panah-menu"></span>
                           </a> 
                          <ul class="sub-ul-menu-empat" >
                              <li> <a  class="ajaxurl"  href="?act=layanan"> Renstra</a> </li>
                              <li> <a  class="ajaxurl"  href="?act=layanan"> RKA SKPD</a> </li>

                          </ul> 
                      </li>   
                      <li> <a  class="ajaxurl"  href="?act=layanan">Tata Usaha</a> </li>
                      <li class="sub-menu-empat">
                           <a  href="javascript:void(0)"> Bendahara
                              <span class="panah-menu"></span>
                           </a> 
                          <ul class="sub-ul-menu-empat" >
                              <li> <a  class="ajaxurl"  href="?act=layanan">Penerimaan</a> </li>
                              <li> <a  class="ajaxurl"  href="?act=layanan">Pengeluaran</a> </li>
                          </ul> 
                      </li>    
                      <li> <a  class="ajaxurl"  href="?act=layanan">Pembukuan</a> </li>
                  </ul>
              </li> 
          </ul>
        </li>        

     


        <li class="toogle-first "  >
         <div class="accordion-heading"> <a href="javascript:void(0);" class="accordion-toggle"> 
<span class="icon-menu fa fa-gears fa-lg"></span>
<span class="label-menu">Utilitas</span>
<span class="panah-menu"></span>
          </a> </div>
                    <ul class="sub-ul-menu" > 
                         <li> <a  class="ajaxurl"  href="?act=jabatan">Perubahan Status</a> </li>
                         <li> <a  class="ajaxurl"  href="?act=jabatan">Copy Data Belanja</a> </li>
                         <li> <a  class="ajaxurl"  href="?act=jabatan">Copy Data Standard Harga</a> </li>
                         <li> <a  class="ajaxurl"  href="?act=jabatan">Copy Data Antar Tahun</a> </li>
                         <li> <a  class="ajaxurl"  href="?act=jabatan">Backup & Restore</a> </li>
                    </ul>
        </li>
        
          <li class="toogle-first " onClick="window.location='?act=logout'" >
         <div class="accordion-heading "> <a href="#" class="accordion-toggle"> 
<span class="icon-menu fa fa-sign-out fa-lg"></span>
<span class="label-menu">Logout</span>
          </a> </div>
        </li>
    
    </ul>
</div>

<div class="top-header" >
<div class="logo" style="text-align:left;">
<img src="<?php echo $mdu['logo'];?>" ><span style="
    color: #B99B08;
    font-size: 24px;
    display: inline-block;
    margin-top: 10px;
    float: right;
    margin-left: 10px;
"><?php echo $mdu['nama_pemda']?></span>
</div>

	<div class="akun">
	
	
     <div class="dropdon">
        <a class="dropdivmenu"><span class="labakun"><?php echo get_ses('profil','nama');?></span>
		<div  class="imakun" style="background-image:url(<?php echo get_ses('profil','foto');?>);"> </div>
		 <span class="rowdown"></span></a>
        <div class="divmenu">
            <ul>
                  <li> <a  href="?act=user&do=profil" label="Profil Akun - <?php echo get_ses('profil','nama');?>" class="linkbox" onclick="$(document).mouseup()"  ><span class=" fa fa-user fa-lg"></span> Profil User</a> </li>
                  <li> <a  class="ajaxurl"  onclick="$(document).mouseup()" href="?act=profil"><span class=" fa fa-clipboard fa-lg"></span> Help</a> </li>
                <li><a href="javascript:void(0)"  onClick="window.location='?act=logout'"><span class="fa fa-sign-out fa-lg"></span>Logout</a></li>
            </ul>
        </div>
      </div>
    
    </div>
</div>
<div class="master-content">
<!-- <div class="second-header"> -->
<!-- <div id="mobilemenu"></div> -->
<!-- <div class="clear"></div>
</div> -->
<div class="main-content">

<div class="title-content">
<div id="title-content">Loading...</div>
</div>
<div class="content">
<div id="loadovers" class="loadspiner"><span class="fa fa-spinner fa-spin fa-speed-2x"></span><span class="labelt">
Silahkan Tunggu, Sedang Memproses Data...</span></div>
<div class="content-slide">