<script>

$(document).ready(function(){

	$("#look_urusan").keyup(function(){
		if($(this).val()=="")
		{
			$('#id_bidang').val('');
			$('#kode_bidang').val('');
			$('#nama_bidang').html('');

			$("#look_bidang").val('');
			$("#look_bidang").attr('readonly','readonly');
			$("#ctrllook_bidang").hide();
			$("#look_bidang").keyup();
		}
	});

	$("#look_bidang").keyup(function(){
		if($(this).val()=="")
		{

			$('#id_unit').val('');
			$('#kode_unit').val('');
			$('#nama_unit').html('');
			$("#look_unit").val('');
			$("#look_unit").attr('readonly','readonly');
			$("#ctrllook_unit").hide();
			$("#look_unit").keyup();
		}
	});
	$("#look_unit").keyup(function(){
		if($(this).val()=="")
		{

			$('#id_sub_unit').val('');
			$('#kode_sub_unit').val('');
			$('#nama_sub_unit').html('');

			$("#look_sub_unit").val('');
			$("#look_sub_unit").attr('readonly','readonly');
			$("#ctrllook_sub_unit").hide();

		}
	});

	function lukup_bidang(){
		$("#look_bidang").removeAttr('readonly');
		$("#look_bidang").hlookup({
			url:'?act=lookup&do=get_bidang&id_urusan='+$('#id_urusan').val(),
			url_detail:'?act=lookup&do=get_bidang_detail&id_urusan='+$('#id_urusan').val(),
			title: 'Data Bidang '+$('#nama_urusan').html(),
			onComplete:function(data){
						$('#id_bidang').val(data.id);
						// $('#kode_bidang').val(data.kode);
						$('#nama_bidang').html(data.nama);
			

						$('#id_unit').val('');
						$('#kode_unit').val('');
						$('#nama_unit').html('');

						$("#look_unit").val('');
						lukup_unit();
			}
		});
	}

	function lukup_unit(){
		$("#look_unit").removeAttr('readonly');
		$("#look_unit").hlookup({
			url:'?act=lookup&do=get_unit&id_bidang='+$('#id_bidang').val(),
			url_detail:'?act=lookup&do=get_unit_detail&id_bidang='+$('#id_bidang').val(),
			title: 'Data unit '+$('#nama_bidang').html(),
			onComplete:function(data){
						$('#id_unit').val(data.id);
						// $('#kode_unit').val(data.kode);
						$('#nama_unit').html(data.nama);
			

						$('#id_sub_unit').val('');
						$('#kode_sub_unit').val('');
						$('#nama_sub_unit').html('');

						$("#look_sub_unit").val('');
						lukup_sub_unit();
			}
		});
	}

	function lukup_sub_unit(){
					$("#look_sub_unit").removeAttr('readonly');
					$("#look_sub_unit").hlookup({
						url:'?act=lookup&do=get_sub_unit&id_unit='+$('#id_unit').val(),
						url_detail:'?act=lookup&do=get_sub_unit_detail&id_unit='+$('#id_unit').val(),
						title: 'Data Sub unit '+$('#nama_bidang').html(),
						onComplete:function(data){
									$('#id_sub_unit').val(data.id);
									// $('#kode_unit').val(data.kode);
									$('#nama_sub_unit').html(data.nama);
						}
					});
	}


	<?php if($detail['kode_bidang']!=""){?>
		lukup_bidang();
	<?php }?>
	<?php if($detail['kode_unit']!=""){?>
		lukup_unit();
	<?php }?>
	<?php if($detail['kode_sub_unit']!=""){?>
		lukup_sub_unit();
	<?php }?>


	$("#look_urusan").hlookup({
		url:'?act=lookup&do=get_urusan',
		url_detail:'?act=lookup&do=get_urusan_detail',
		title: 'Data Urusan',
		onComplete:function(data){
					$('#id_urusan').val(data.id);
					// $('#kode_urusan').val(data.kode);
					$('#nama_urusan').html(data.nama);

					$('#id_bidang').val('');
					$('#kode_bidang').val('');
					$('#nama_bidang').html('');

					$("#look_bidang").val('');
					$("#look_bidang").keyup();
					lukup_bidang();
					
		}
	});

	var uploaders = new qq.FileUploader({
		element: document.getElementById('filefoto'),
		action: 'model/file_uploader.php',			
		debug: true,
		onComplete: function(id, fileName, data){
			if(data['success']){
			var n='files/'+data['nama'];
			$('#foto').val(n);
			$('#srcfoto').hide();
			$('#srcfoto').fadeIn(1000);
			$('#srcfoto').attr('src',n)
			}
			hideLoad();
			$("#filefoto").next('span').html(fileName);
		},
		onProgress: function(id, fileName, loaded, total){
			console.log(total);
			console.log(loaded);
			$("#filefoto").next('span').html(Math.round(loaded*100/total)+" %");
		},
		onSubmit: function(id, fileName){
			showLoad("mmb","Silahkan Tunggu, Sedang Meng-Upload File.");
			$("#filefoto").next('span').html("Starting...");
		}
	});
	var uploadersttd = new qq.FileUploader({
		element: document.getElementById('filettd'),
		action: 'model/file_uploader.php',			
		debug: true,
		onComplete: function(id, fileName, data){
			if(data['success']){
			var n='files/'+data['nama'];
			$('#ttd').val(n);
			$('#srcttd').hide();
			$('#srcttd').fadeIn(1000);
			$('#srcttd').attr('src',n)
			}
			hideLoad();
			$("#filettd").next('span').html(fileName);
		},
		onProgress: function(id, fileName, loaded, total){
			console.log(total);
			console.log(loaded);
			$("#filettd").next('span').html(Math.round(loaded*100/total)+" %");
		},
		onSubmit: function(id, fileName){
			showLoad("mmb-ttd","Silahkan Tunggu, Sedang Meng-Upload File.");
			$("#filettd").next('span').html("Starting...");
		}
	});	
	$('#former input[type="text"], #former select,#former input[type="password"]').tooltipster({
        trigger: 'custom',
        onlyOne: false,
        position: 'top'
    });
	
	$('#former').validate({
		  rules: {
			nama: {
	        required: true
	      },username: {
				required: true
			  },email: {
				email: true
			  },id_akses: {
				required: true
			  },
			<?php if($id<=0){
			?>	password: {
				required: true
			  }
			<?php }?>  
	    },	
		    errorPlacement: function (error, element) {
            $(element).tooltipster('update', $(error).text());
			($(error).text()!='') ? $(element).tooltipster('show') : $(element).tooltipster('hide');
        },
			success: function(label,element) {
			$(element).tooltipster('hide');
		 },
		    submitHandler: function (form) { 
			saveData('former','?act=user&do=simpan');
        }
	  }); 
});
</script>

<form method="post" id="former" style="width:100%">
<input type="hidden" value="<?php echo  $id;?>" name="id">

<crow><label><tag>Nama Lengkap:</tag>
<input type="text" name="nama" size="30" value="<?php echo $detail['nama']?>"  />
</label></crow>
<crow><label><tag>Username :</tag>
<input type="text" name="username" size="30" value="<?php echo $detail['username']?>"  />
</label></crow>
<span style="color:#009933;" ><b>Info : </b>
isi username hanya dengan huruf. agar mudah ketika login.

</span>
<crow><label><tag>Password <b style="color:#f00;">*</b> :</tag>
<input type="password" name="password" size="30" value=""  /><?php
if($id>0){
?><br />

<span style="color:#F00;" ><b>Perhatian : </b>

Jika tidak ingin mengubah password, Biarkan isian tetap kosong.

</span>
<?php }?>
</label></crow>
<crow><label><tag>Telepon :</tag>
<input type="text" name="telepon" size="30" value="<?php echo $detail['telepon']?>"  />
</label></crow>
<crow><label><tag>Email :</tag>
<input type="text" name="email" size="30" value="<?php echo $detail['email']?>"  />
</label></crow>
<crow><label><tag>Kota :</tag>
<input type="text" name="kota" size="30" value="<?php echo $detail['kota']?>"  />
</label></crow>
<crow><label><tag>Tgl Lahir :</tag>
<input type="text" name="tgl_lahir" class="tgl" size="30" value="<?php echo $detail['tgl_lahir']?>"  />
</label></crow>
<crow><label><tag>Alamat :</tag>
<textarea name="alamat" style="width:300px;" rows="3">
<?php echo $detail['alamat']?>
</textarea>
</label></crow>

<crow><label><tag>Foto :</tag>
<div id="filefoto" class="btn-primary btn-files "></div>
<span  style="font-size:12px; font-weight:bold; padding-left:10px;float:left;"></span>
<div class="mmb">
</div>
<input type="hidden" class=""  id="foto" name="foto" value="<?php echo (is_file($detail['foto'])) ? $detail['foto'] : 'assets/images/avt.jpg' ?>" size="">
</label></crow>
<crow><label><tag>&nbsp;</tag>
<img src="<?php echo (is_file($detail['foto'])) ? $detail['foto'] : 'assets/images/avt.jpg' ?>" id="srcfoto" style="width:150px; margin-top:5px; border:solid 1px #09F; ">
</label></crow>

<br />

<crow><label><tag>Tanda Tangan :</tag>
<div id="filettd" class="btn-primary btn-files "></div>
<span  style="font-size:12px; font-weight:bold; padding-left:10px;float:left;"></span>
<div class="mmb-ttd">
</div>
<input type="hidden" class=""  id="ttd" name="ttd" value="" size="">
</label></crow>
<crow><label><tag>&nbsp;</tag>
<img src="<?php echo (is_file($detail['ttd'])) ? $detail['ttd'] : '' ?>" id="srcttd" style="width:150px; margin-top:5px; border:solid 1px #09F; ">
</label></crow>

<br />


<crow class="no-border"><label><tag>Urusan :</tag>
<input type="text" id="look_urusan" placeholder="Kode / Nama Urusan" name="look_urusan" size="20" value="<?php echo $detail['kode_urusan']?>"  />
<input type="hidden" id="id_urusan" name="id_urusan" value="<?php echo $detail['id_urusan']?>"  />
</label></crow>

<crow class="row-ket"><label><tag>&nbsp;</tag>
<span id="nama_urusan"><?php echo $detail['nama_urusan']?></span>
</label></crow>


<crow  class="no-border"><label><tag>Bidang :</tag>
<input type="text" id="look_bidang" placeholder="Kode / Nama Bidang" name="look_bidang" size="20" value="<?php echo $detail['kode_bidang']?>"  readonly="readonly" />
<input type="hidden" id="id_bidang" name="id_bidang" value="<?php echo $detail['id_bidang']?>"  />
</label></crow>

<crow class="row-ket"><label><tag>&nbsp;</tag>
<span id="nama_bidang"><?php echo $detail['nama_bidang']?></span>
</label></crow>



<crow  class="no-border"><label><tag>Unit :</tag>
<input type="text" id="look_unit" placeholder="Kode / Nama Unit" name="look_unit" size="20" value="<?php echo $detail['kode_unit']?>"  readonly="readonly" />
<input type="hidden" id="id_unit" name="id_unit" value="<?php echo $detail['id_unit']?>"  />
</label></crow>

<crow class="row-ket"><label><tag>&nbsp;</tag>
<span id="nama_unit"><?php echo $detail['nama_unit']?></span>
</label></crow>



<crow  class="no-border"><label><tag>Sub Unit :</tag>
<input type="text" id="look_sub_unit" placeholder="Kode / Nama Sub Unit" name="look_sub_unit" size="20" value="<?php echo $detail['kode_sub_unit']?>"  readonly="readonly" />
<input type="hidden" id="id_sub_unit" name="id_sub_unit" value="<?php echo $detail['id_sub_unit']?>"  />
</label></crow>

<crow class="row-ket"><label><tag>&nbsp;</tag>
<span id="nama_sub_unit"><?php echo $detail['nama_sub_unit']?></span>
</label></crow>

<div class="nav-control">
<button type="submit"  class="btn-primary " ><span class="fa fa-floppy-o"></span>Ubah Akun</button>
<button type="reset"  class="btn-info " ><span class="fa fa-share-square-o"></span>Reset</button>
<button type="button"  class="btn-danger"   onClick="closed()"><span class="fa fa-times"></span>Batal</button>
</div>
</form>