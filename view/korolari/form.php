<script>
$(document).ready(function(){

		
		$("#look_korolari").hlookup({
			url:'?act=lookup&do=get_apbd_rincian',
			url_detail:'?act=lookup&do=get_apbd_rincian_detail',
			title: 'Data Akun ',
			onComplete:function(data){
						$('#id_rek_kor').val(data.id);
						$('#kode_korolari').val(data.kode);
						$('#nama_korolari').val(data.nama);
			}
		});
	
	$("#look_debet").hlookup({
			url:'?act=lookup&do=get_apbd_rincian',
			url_detail:'?act=lookup&do=get_apbd_rincian_detail',
			title: 'Data Akun ',
			onComplete:function(data){
						$('#id_rek_debet').val(data.id);
						$('#kode_debet').val(data.kode);
						$('#nama_debet').val(data.nama);
			}
		});

	$("#look_kredit").hlookup({
		url:'?act=lookup&do=get_apbd_rincian',
		url_detail:'?act=lookup&do=get_apbd_rincian_detail',
		title: 'Data Akun ',
		onComplete:function(data){
					$('#id_rek_kredit').val(data.id);
					$('#kode_kredit').val(data.kode);
					$('#nama_kredit').val(data.nama);
		}
	});

	$('#former input[type="text"], #former select,#former textarea').tooltipster({
        trigger: 'custom',
        onlyOne: false,
        position: 'top'
    });
	
	$('#former').validate({
		  rules: {
			look_korolari: {
	        required: true
	      },
			nama_korolari: {
	        required: true
	      },
			look_debet: {
	        required: true
	      },
			nama_debet: {
	        required: true
	      },
			look_kredit: {
	        required: true
	      },
			nama_kredit: {
	        required: true
	      }
	    },	
		    errorPlacement: function (error, element) {
            $(element).tooltipster('update', $(error).text());
			($(error).text()!='') ? $(element).tooltipster('show') : $(element).tooltipster('hide');
        },
			success: function(label,element) {
			$(element).tooltipster('hide');
		 },
		    submitHandler: function (form) { 
			saveData('former','?act=korolari&do=simpan','','',function(data){
				if(data=="failed"){
					alert("Maaf data gagal di simpan. data korolari dengan kode rekening yang anda masukan sudah ada");
				}
			});
        }
	  }); 


});
</script>

<form method="post" id="former" style="width:100%">
<input type="hidden" value="<?php echo $id;?>" name="id">


<crow><label><tag>Kode Rek.Korolari :</tag>
		<input type="text" id="look_korolari" placeholder="Masukan Kode / Nama" name="look_korolari" size="20" value="<?php echo $detail['kode_korolari']?>"   />
		<input type="hidden" id="id_rek_kor" name="id_rek_kor" value="<?php echo $detail['id_rek_kor']?>"  />

</label></crow>
<crow><label><tag>Nama Rek.Korolari :</tag>

<input type="text"  name="nama_korolari" id="nama_korolari" value="<?php echo $detail['nama_korolari']?>"  size="50" readonly="readonly" />
</label></crow>


<crow><label><tag>Kode Rek.Debet :</tag>
		<input type="text" id="look_debet" placeholder="Masukan Kode / Nama" name="look_debet" size="20" value="<?php echo $detail['kode_debet']?>"   />
		<input type="hidden" id="id_rek_debet" name="id_rek_debet" value="<?php echo $detail['id_rek_debet']?>"  />

</label></crow>
<crow><label><tag>Nama Rek.Debet :</tag>

<input type="text"  name="nama_debet" id="nama_debet" value="<?php echo $detail['nama_debet']?>"  size="50" readonly="readonly" />
</label></crow>


<crow><label><tag>Kode Rek.Kredit :</tag>
		<input type="text" id="look_kredit" placeholder="Masukan Kode / Nama" name="look_kredit" size="20" value="<?php echo $detail['kode_kredit']?>"   />
		<input type="hidden" id="id_rek_kredit" name="id_rek_kredit" value="<?php echo $detail['id_rek_kredit']?>"  />

</label></crow>
<crow><label><tag>Nama Rek.Kredit :</tag>

<input type="text"  name="nama_kredit" id="nama_kredit" value="<?php echo $detail['nama_kredit']?>"  size="50" readonly="readonly" />
</label></crow>

<div class="nav-control">
<button type="submit"  class="btn-primary " ><span class="fa fa-floppy-o"></span>Simpan</button>
<button type="reset"  class="btn-info " ><span class="fa fa-share-square-o"></span>Reset</button>
<button type="button"  class="btn-danger"   onClick="closed()"><span class="fa fa-times"></span>Batal</button>
</div>
</form>