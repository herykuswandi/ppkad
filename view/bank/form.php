<script>
$(document).ready(function(){

		
		$("#look_apbd_rincian").hlookup({
			url:'?act=lookup&do=get_apbd_rincian',
			url_detail:'?act=lookup&do=get_apbd_rincian_detail',
			title: 'Data Akun ',
			onComplete:function(data){
						$('#id_apbd_rincian').val(data.id);
						$('#kode_apbd_rincian').val(data.kode);
						$('#nama_apbd_rincian').val(data.nama);
			}
		});
	

	$('#former input[type="text"], #former select,#former textarea').tooltipster({
        trigger: 'custom',
        onlyOne: false,
        position: 'top'
    });
	
	$('#former').validate({
		  rules: {
			kode: {
	        required: true
	      },
			nama: {
	        required: true
	      },
			no_rek: {
	        required: true
	      },
			look_apbd_rincian: {
	        required: true
	      },
			nama_apbd_rincian: {
	        required: true
	      }
	    },	
		    errorPlacement: function (error, element) {
            $(element).tooltipster('update', $(error).text());
			($(error).text()!='') ? $(element).tooltipster('show') : $(element).tooltipster('hide');
        },
			success: function(label,element) {
			$(element).tooltipster('hide');
		 },
		    submitHandler: function (form) { 
			saveData('former','?act=bank&do=simpan','','',function(data){
				if(data=="failed"){
					alert("Maaf data gagal di simpan. Kode atau nama bank yang anda masukan sudah ada");
				}
			});
        }
	  }); 


});
</script>

<form method="post" id="former" style="width:100%">
<input type="hidden" value="<?php echo $id;?>" name="id">

 <crow><label><tag>Kode :</tag>
<input type="text"  name="kode" value="<?php echo $detail['kode']?>" size="10" />
</label></crow>

<crow><label><tag>Nama Bank:</tag>
<input type="text"  name="nama" value="<?php echo $detail['nama']?>" size="40" />
</label></crow>

<crow><label><tag>No. Rekening :</tag>
<input type="text"  name="no_rek" value="<?php echo $detail['no_rek']?>"  size="30" />
</label></crow>

<crow><label><tag>Kode Rekening :</tag>
		<input type="text" id="look_apbd_rincian" title="Kode / Nama Rincian" name="look_apbd_rincian" size="20" value="<?php echo $detail['kode_apbd_rincian']?>"   />
		<input type="hidden" id="id_apbd_rincian" name="id_apbd_rincian" value="<?php echo $detail['id_apbd_rincian']?>"  />

</label></crow>
<crow><label><tag>Nama Rekening :</tag>

<input type="text"  name="nama_apbd_rincian" id="nama_apbd_rincian" value="<?php echo $detail['nama_apbd_rincian']?>"  size="50" readonly="readonly" />
</label></crow>

<div class="nav-control">
<button type="submit"  class="btn-primary " ><span class="fa fa-floppy-o"></span>Simpan</button>
<button type="reset"  class="btn-info " ><span class="fa fa-share-square-o"></span>Reset</button>
<button type="button"  class="btn-danger"   onClick="closed()"><span class="fa fa-times"></span>Batal</button>
</div>
</form>