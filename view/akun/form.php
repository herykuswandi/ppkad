<script>
$(document).ready(function(){
	$('#former input[type="text"], #former select').tooltipster({
        trigger: 'custom',
        onlyOne: false,
        position: 'top'
    });
	
	$('#former').validate({
		  rules: {
			nama: {
	        required: true
	      },
			kode: {
	        required: true
	      }
	    },	
		    errorPlacement: function (error, element) {
            $(element).tooltipster('update', $(error).text());
			($(error).text()!='') ? $(element).tooltipster('show') : $(element).tooltipster('hide');
        },
			success: function(label,element) {
			$(element).tooltipster('hide');
		 },
		    submitHandler: function (form) { 
			saveData('former','?act=akun&do=simpan','','',function(data){
			if(data=="failed"){
					alert("Maaf data gagal di simpan. Kode akun yang anda masukan sudah ada");
				}else{
						ajaxload('?act=akun');
				}
			});
        }
	  }); 
   $("#kode_akun").mask("9.9.9.99.99.9999");
});
</script>

<form method="post" id="former" style="width:100%">
<input type="hidden" value="<?php echo $id;?>" name="id">

<crow><label><tag>Kode :</tag>
<input type="text" name="kode" id="kode_akun" value="<?php echo $detail['kode'];?>"  />
</label></crow>

<crow><label><tag>Nama  :</tag>
<input type="text" name="nama" value="<?php echo $detail['nama'];?>" size="40" />
</label></crow>

<div class="nav-control">
<button type="submit"  class="btn-primary " ><span class="fa fa-floppy-o"></span>Simpan</button>
<button type="reset"  class="btn-info " ><span class="fa fa-share-square-o"></span>Reset</button>
<button type="button"  class="btn-danger"   onClick="closed()"><span class="fa fa-times"></span>Batal</button>
</div>
</form>