
<div class="nav-control" style="background:none;border:none">
 	<div style="float:left;">
 		<b>Pencarian :</b> <input type="text" placeholder="Masukan Kode / Nama " class="caritemp" value="<?php echo $cari;?>" style="width:200px">
 		<span class="fa fa-search" style=" margin-left: -25px;    color: #00C7FF;"></span>
	</div>
 <a href="?act=akun&do=form" label="Tambah Akun"  title="Tambah Akun" class="linkbox">
   <button type="button"  class="btn-success" ><span class="fa fa-plus-circle"></span>Tambah</button>              
  </a></div>
 <table class="tabel" style="width:100%" width="100%" cellpadding="0" cellspacing="0">
  <tr>
  	<th width="11%">Opsi</th>	
  	<th width="32%">Kode</th>	
  	<th width="52%">Nama Akun</th>	
  </tr>
<?php
if(is_array($data) && count($data)>0){
 foreach ($data as $row) {
?>
    <?php

    $pos1 = stripos($row['kode'],'0.0.00.00.000');
	$pos2 = stripos($row['kode'],'0.00.00.000');
	$pos3 = stripos($row['kode'],'00.00.000');
	$pos4 = stripos($row['kode'],'00.000');
	$pos5 = stripos($row['kode'],'000');
	?>

<tr   class="<?php 
	if ($pos1 !== false){
		echo 'tree_pos_1';
		$kode_pos=substr($row['kode'],0,1);
	}else{
		if ($pos2 !== false){
			echo 'tree_pos_2';
			$kode_pos=substr($row['kode'],0,3);
		}else{
			if ($pos3 !== false){
				echo 'tree_pos_3';
				$kode_pos=substr($row['kode'],0,5);

			}else{
				if ($pos4	 !== false){
					echo 'tree_pos_4';
					$kode_pos=substr($row['kode'],0,8);
				}else{
					if ($pos5 !== false){
						echo 'tree_pos_5';
						$kode_pos=substr($row['kode'],0,11);
					}else{
						echo 'tree_pos_6';
						$kode_pos=substr($row['kode'],0,15);
						
					}
				}
			}		
		}	
	}
  ?> <?php echo $kode_pos;?>" kode-pos="<?php echo $kode_pos;?>"
>	
		<td>&nbsp;&nbsp;<a href="?act=akun&do=form&id=<?php echo $row['id'];?>" onclick="return modaler(this);" class="ficon-green fa fa-pencil-square-o fa-lg linkbox" label="Ubah Akun" title="Ubah Akun"> </a> 
			<a href="javascript:void(0)" onclick="del_temp('?act=akun&do=hapus&id=<?php echo $row['id'];?>',this,function(){ajaxload('?act=akun');})" class="ficon-red fa fa-trash-o fa-lg " title="Hapus Akun"></a>
			<!-- <a href="javascript:void(0);" onclick="" class="ficon-green fa fa-minus-square fa-lg col-pos " title=""> </a> -->
		</td>	
		<td class="postr"><b><?php echo $row['kode']?></b></td>	
		<td class="postr"><b>&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $row['nama']?></b></td>	
	  </tr>
	
<?php
 }
}else{
	echo "<tr><td colspan=3><center>Maaf, Data Kosong.</center></td></tr>";
}
?>
  </table>
<div class="nav-control">
	<div style="float:left;">
 		<b>Pencarian :</b> <input type="text" placeholder="Masukan Kode / Nama " class="caritemp" value="<?php echo $cari;?>" style="width:200px">
 		<span class="fa fa-search" style=" margin-left: -25px;    color: #00C7FF;"></span>
	</div>
 
  <a href="?act=akun&do=form" label="Tambah Akun"  title="Tambah Akun" class="linkbox">
   <button type="button"  class="btn-success" ><span class="fa fa-plus-circle"></span>Tambah</button>              
  </a>
</div>
<script>
//$("[class^='tree_pos_1 1.0.0']")
$(document).ready(function(){
$('.caritemp').keyup(function(e){
	if(e.keyCode=='13'){
			ajaxload('?act=akun&cari='+$(this).val());
	}
});
	$('.tree_pos_1 .col-pos').each(function(){
		$(this).click(function(){
			var elpar=$(this).closest('.tree_pos_1').attr('kode-pos');
			var elchild=$("[class^='tree_pos_2 "+elpar+"'],[class^='tree_pos_3 "+elpar+"'],[class^='tree_pos_4 "+elpar+"'],[class^='tree_pos_5 "+elpar+"'],[class^='tree_pos_6 "+elpar+"']	");
			if(elchild.is(":hidden")){
				elchild.slideDown();
				$(this).removeClass('fa-plus-square');
				$(this).addClass('fa-minus-square');
			}else{
				elchild.slideUp();
				$(this).removeClass('fa-minus-square');
				$(this).addClass('fa-plus-square');
			}
		});
	});

	$('.tree_pos_2 .col-pos').each(function(){
		$(this).click(function(){
			var elpar=$(this).closest('.tree_pos_2').attr('kode-pos');
			var elchild=$("[class^='tree_pos_3 "+elpar+"'],[class^='tree_pos_4 "+elpar+"'],[class^='tree_pos_5 "+elpar+"'],[class^='tree_pos_6 "+elpar+"']	");
			if(elchild.is(":hidden")){
				elchild.slideDown();
				$(this).removeClass('fa-plus-square');
				$(this).addClass('fa-minus-square');
			}else{
				elchild.slideUp();
				$(this).removeClass('fa-minus-square');
				$(this).addClass('fa-plus-square');
			}
		});
	});

	$('.tree_pos_3 .col-pos').each(function(){
		$(this).click(function(){
			var elpar=$(this).closest('.tree_pos_3').attr('kode-pos');
			var elchild=$("[class^='tree_pos_4 "+elpar+"'],[class^='tree_pos_5 "+elpar+"'],[class^='tree_pos_6 "+elpar+"']	");
			if(elchild.is(":hidden")){
				elchild.slideDown();
				$(this).removeClass('fa-plus-square');
				$(this).addClass('fa-minus-square');
			}else{
				elchild.slideUp();
				$(this).removeClass('fa-minus-square');
				$(this).addClass('fa-plus-square');
			}
		});
	});
});
</script>
<style>
.tree_pos_1 td.postr{
	padding-left: 0px;
}
.tree_pos_2 td.postr{
	padding-left: 10px;
}
.tree_pos_3 td.postr{
	padding-left: 20px;
}
.tree_pos_4 td.postr{
	padding-left: 30px;
}
.tree_pos_5 td.postr{
	padding-left: 40px;
}
.tree_pos_6 td.postr{
	padding-left: 50px;
}
</style>