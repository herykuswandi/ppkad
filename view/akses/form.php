<script>

$(document).ready(function(){
	$('.hak-1 input[type="checkbox"]').each(function(){
		$(this).click(function(){
			if($(this).is(':checked')){
				$(this).closest('.hks').find('input[type="checkbox"]').attr('checked','checked');
			}else{
				$(this).closest('.hks').find('input[type="checkbox"]').removeAttr('checked');
			}
		});
	});
	$('.hak-2 input[type="checkbox"]').each(function(){
		$(this).click(function(){
			if($(this).is(':checked')){
				$(this).closest('.hak-sub').find('input[type="checkbox"]').attr('checked','checked');
			}else{
				$(this).closest('.hak-sub').find('input[type="checkbox"]').removeAttr('checked');
			}
		});
	});
	$( "#tabtrans" ).tabs();
	
	$('#former input[type="text"], #former select').tooltipster({
        trigger: 'custom',
        onlyOne: false,
        position: 'top'
    });
	
	$('#former').validate({
	    rules: {
			nama: {
				required: true
	        }
	    },	
		    errorPlacement: function (error, element) {
      			$(element).tooltipster('update', $(error).text());
				($(error).text()!='') ? $(element).tooltipster('show') : $(element).tooltipster('hide');
		},
			success: function(label,element) {
			$(element).tooltipster('hide');
		 },
		    submitHandler: function (form) { 
						saveData('former','?act=akses&do=simpan','','',function(data){
						if(data=="failed"){
									alert("Maaf data gagal di simpan. Nama Group yang anda masukan sudah ada");
						}else{
							ajaxload('?act=akses');
						}
						
						});
		}
	  }); 

});

</script>

<form method="post" id="former" style="width:100%" >
<div class="form-rows  fbreak" id="instrans">
	<input type="hidden" name="id" value="<?php echo $id;?>">

	<crow><label><tag>Nama Group :</tag>
	<input type="text" name="nama" value="<?php echo $detail['nama']; ?>"  />
	</label></crow>
	<crow><label><tag>Keterangan :</tag>
		<textarea name='keterangan'><?php echo $detail['keterangan'];?></textarea>
	</label></crow>

</div>
<div class="clear"></div>
<b>Menu Akses :</b>

<div id="tabtrans" style="position:inherit;">
	<ul>
		<?php
		foreach ($hak as $key=>$row) {
			echo '<li><a href="#hk-'.$key.'">'.$row['label'].'</a></li>';
		}
		?>
	</ul>
	<?php

foreach ($hak as $key=>$row) {
	$nhk1='['.$key.']';
	if(isset($arrdat[$key])){
		$dhk1=$arrdat[$key];
		$ck=1;
	}else{
		$dhk1=array();
		$ck=0;
	}
?>
	<div class="hks" id="hk-<?php echo $key;?>">
		<div class="hak-1">
			<label>
				<input type="checkbox" <?php echo ($ck>0)? 'checked="checked"' : '';?> name="hak<?php echo $nhk1;?>"  value="1"> <span><?php echo $row['label'];?></span>
			</label>
		</div>
		<?php
			if(isset($row['child']))
			foreach ($row['child'] as $key=>$row) {
				$nhk2=$nhk1.'['.$key.']';
				if(isset($dhk1[$key])){
					$dhk2=$dhk1[$key];
					$ck2=1;
				}else{
					$dhk2=array();
					$ck2=0;
				}
			?>
			<div class="hak-sub">
				<div  class="hak-2">
					<label>
						<input type="checkbox"  <?php echo ($ck2>0)? 'checked="checked"' : '';?>  name="hak<?php echo $nhk2;?>"  value="1"> <span><?php echo $row['label'];?></span>
					</label>
				</div>
			<?php
							if(isset($row['child']))
					foreach ($row['child'] as $key=>$row) {
						$nhk3=$nhk2.'['.$key.']';
						if(isset($dhk2[$key])){
							$dhk3=$dhk2[$key];
							$ck3=1;
						}else{		
							$dhk3=array();
							$ck3=0;
						}

						?>
							<div  class="hak-3">
								<label>
									<input <?php echo ($ck3>0)? 'checked="checked"' : '';?>  type="checkbox" name="hak<?php echo $nhk3;?>" value="1"> <span><?php echo $row['label'];?></span>
								</label>
							</div>
						<?php
					}	
			?>
			</div>
			<?php
			}
			?>
			<div class="clear"></div>
		</div>
		<?php	
	}	
?>
</div>

<div class="nav-control">
<button type="submit"  class="btn-primary " ><span class="fa fa-floppy-o"></span>Simpan</button>
<button type="reset"  class="btn-info " ><span class="fa fa-share-square-o"></span>Reset</button>
<button type="button"  class="btn-danger"  onClick="ajaxload('?act=akses')"><span class="fa fa-times"></span>Batal</button>
</div>	


</form>
<style>
.hak-2{
	margin-left: 20px;
}
.hak-3{
	margin-left: 40px;
}
.hak-sub{
	float: left;
	margin: 10px;
	padding: 5px 5px 5px 0px;
	width: 230px;
	border:solid 1px #eaeaea;
}
</style>