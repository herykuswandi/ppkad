<script>
$(document).ready(function(){
	$('#former input[type="text"], #former select,#former input[type="password"]').tooltipster({
        trigger: 'custom',
        onlyOne: false,
        position: 'top'
    });
	
	$('#former').validate({
		  rules: {
			bulan: {
	        required: true
	      },tahun: {
			required: true
		  } 
	    },	
		    errorPlacement: function (error, element) {
            $(element).tooltipster('update', $(error).text());
			($(error).text()!='') ? $(element).tooltipster('show') : $(element).tooltipster('hide');
        },
			success: function(label,element) {
			$(element).tooltipster('hide');
		 },
		    submitHandler: function (form) { 
			saveData('former','?act=bulan&do=simpan','','',function(){
				alert('Bulan & Tahun Periode Berhasil Di Ubah');
				window.location="?";
			});
        }
	  }); 
});
</script>

<form method="post" id="former" style="width:100%">

<crow><label><tag>Bulan :</tag>
<select name="bulan" >
<?php foreach($bulans as $bu=>$lan){?>
<option value="<?php echo $bu;?>" <?php echo ($bu==$bulan) ? 'selected="selected"' : ''?>>
<?php echo $lan;?>
</option>
<?php }?>
</select>
</label></crow>
<crow><label><tag>Tahun :</tag>
<select name="tahun" >
<?php for($i=($tahun-4); $i<$tahun; $i++){?>
<option value="<?php echo $i;?>" <?php echo ($i==$tahun) ? 'selected="selected"' : ''?>>
<?php echo $i;?>
</option>
<?php }?>
<?php for($i=$tahun; $i<=($tahun+4); $i++){?>
<option value="<?php echo $i;?>" <?php echo ($i==$tahun) ? 'selected="selected"' : ''?>>
<?php echo $i;?>
</option>
<?php }?>
</select>
</label></crow>



<div class="nav-control">
<button type="submit"  class="btn-primary " ><span class="fa fa-floppy-o"></span>Ubah Akun</button>
<button type="reset"  class="btn-info " ><span class="fa fa-share-square-o"></span>Reset</button>
<button type="button"  class="btn-danger"   onClick="closed()"><span class="fa fa-times"></span>Batal</button>
</div>
</form>