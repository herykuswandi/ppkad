<script>
$(document).ready(function(){

		
		$("#look_apbd_rincian").hlookup({
			url:'?act=lookup&do=get_apbd_rincian',
			url_detail:'?act=lookup&do=get_apbd_rincian_detail',
			title: 'Data Akun ',
			onComplete:function(data){
						$('#id_apbd_rincian').val(data.id);
						$('#kode_apbd_rincian').val(data.kode);
						$('#nama_apbd_rincian').val(data.nama);
			}
		});
		
		$("#look_potongan").hlookup({
			url:'?act=lookup&do=get_potongan',
			url_detail:'?act=lookup&do=get_potongan_detail',
			title: 'Data Akun ',
			onComplete:function(data){
						$('#id_potongan').val(data.id);
						$('#kode_potongan').val(data.kode);
						$('#nama_potongan').val(data.nama);
			}
		});
	

	$('#former input[type="text"], #former select,#former textarea').tooltipster({
        trigger: 'custom',
        onlyOne: false,
        position: 'top'
    });
	
	$('#former').validate({
		  rules: {
			look_apbd_rincian: {
	        required: true
	      },
			nama_apbd_rincian: {
	        required: true
	      },look_potongan: {
	        required: true
	      },
			nama_potongan: {
	        required: true
	      }
	    },	
		    errorPlacement: function (error, element) {
            $(element).tooltipster('update', $(error).text());
			($(error).text()!='') ? $(element).tooltipster('show') : $(element).tooltipster('hide');
        },
			success: function(label,element) {
			$(element).tooltipster('hide');
		 },
		    submitHandler: function (form) { 
			saveData('former','?act=rek_spm&do=simpan','','',function(data){
				if(data=="failed"){
					alert("Maaf data gagal di simpan.  Rekening Potongan SPM yang anda masukan sudah ada");
				}
			});
        }
	  }); 


});
</script>

<form method="post" id="former" style="width:100%">
<input type="hidden" value="<?php echo $id;?>" name="id">


<crow><label><tag>Kode Rekening :</tag>
		<input type="text" id="look_apbd_rincian" title="Kode / Nama Rincian" name="look_apbd_rincian" size="20" value="<?php echo $detail['kode_apbd_rincian']?>"   />
		<input type="hidden" id="id_apbd_rincian" name="id_apbd_rincian" value="<?php echo $detail['id_apbd_rincian']?>"  />

</label></crow>
<crow><label><tag>Nama Rekening :</tag>

<input type="text"  name="nama_apbd_rincian" id="nama_apbd_rincian" value="<?php echo $detail['nama_apbd_rincian']?>"  size="50" readonly="readonly" />
</label></crow>

<crow><label><tag>Kode Potongan :</tag>
		<input type="text" id="look_potongan" title="Kode / Nama Rincian" name="look_potongan" size="20" value="<?php echo $detail['kode_potongan']?>"   />
		<input type="hidden" id="id_potongan" name="id_potongan" value="<?php echo $detail['id_potongan']?>"  />

</label></crow>
<crow><label><tag>Nama Potongan :</tag>

<input type="text"  name="nama_potongan" id="nama_potongan" value="<?php echo $detail['nama_potongan']?>"  size="50" readonly="readonly" />
</label></crow>

<div class="nav-control">
<button type="submit"  class="btn-primary " ><span class="fa fa-floppy-o"></span>Simpan</button>
<button type="reset"  class="btn-info " ><span class="fa fa-share-square-o"></span>Reset</button>
<button type="button"  class="btn-danger"   onClick="closed()"><span class="fa fa-times"></span>Batal</button>
</div>
</form>