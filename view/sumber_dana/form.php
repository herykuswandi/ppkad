<script>
$(document).ready(function(){
	$('#former input[type="text"], #former select,#former textarea').tooltipster({
        trigger: 'custom',
        onlyOne: false,
        position: 'top'
    });
	
	$('#former').validate({
		  rules: {
			no_urut: {
	        required: true
	      },
			nomor: {
	        required: true
	      },
			tanggal: {
	        required: true
	      	, date:true
	      },
			uraian: {
	        required: true
	      }
	    },	
		    errorPlacement: function (error, element) {
            $(element).tooltipster('update', $(error).text());
			($(error).text()!='') ? $(element).tooltipster('show') : $(element).tooltipster('hide');
        },
			success: function(label,element) {
			$(element).tooltipster('hide');
		 },
		    submitHandler: function (form) { 
			saveData('former','?act=sumber_dana&do=simpan','','',function(data){
				if(data=="failed"){
					alert("Maaf data gagal di simpan. Kode / Nama sumber dana yang anda masukan sudah ada");
				}
			});
        }
	  }); 


});
</script>

<form method="post" id="former" style="width:100%">
<input type="hidden" value="<?php echo $id;?>" name="id">

<crow><label><tag>Tahun Anggaran :</tag>
<input type="hidden"  name="id_ta" value="<?php echo $detail['id_ta']?>" size="20" />
<input type="text"  name="tahun" value="<?php echo $detail['tahun']?>" size="10" readonly="readonly" />
</label></crow>


 <crow><label><tag>Kode :</tag>
<input type="text"  name="kode" value="<?php echo $detail['kode']?>" size="10" />
</label></crow>

<crow><label><tag>Nama :</tag>
<input type="text"  name="nama" value="<?php echo $detail['nama']?>" size="40" />
</label></crow>


<div class="nav-control">
<button type="submit"  class="btn-primary " ><span class="fa fa-floppy-o"></span>Simpan</button>
<button type="reset"  class="btn-info " ><span class="fa fa-share-square-o"></span>Reset</button>
<button type="button"  class="btn-danger"   onClick="closed()"><span class="fa fa-times"></span>Batal</button>
</div>
</form>