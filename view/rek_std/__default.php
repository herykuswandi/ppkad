<script>
$(document).ready(function(){
		$( "#tab-rek-apbd" ).tabs();
});
</script>

<div id="tab-rek-apbd" style="position:inherit;">
	<ul>
		<li><a href="#kelompok">Kelompok</a></li>
		<li><a href="#jenis">Jenis</a></li>
		<li><a href="#rincian">Rincian</a></li>
	</ul>
	<div id="kelompok">
		<table class="tabel-kelompok" style="display: none; "></table>
		<div class="nav-control">
		   <a href="?act=rek_std&do=form_kelompok" label="Tambah Kelompok"  title="Tambah Kelompok" class="linkbox">
		   		<button type="button"  class="btn-success" ><span class="fa fa-plus-circle"></span>Tambah</button>              
		   </a>
		</div>
	</div>
	<div id="jenis">
		<table class="tabel-jenis" style="display: none; "></table>
		<div class="nav-control">
		   <a href="?act=rek_std&do=form_jenis" label="Tambah Jenis"  title="Tambah Jenis" class="linkbox">
		   		<button type="button"  class="btn-success" ><span class="fa fa-plus-circle"></span>Tambah</button>              
		   </a>
		</div>
	</div>
	<div id="rincian">
		<table class="tabel-rincian" style="display: none; "></table>
		<div class="nav-control">
		   <a href="?act=rek_std&do=form_rincian" label="Tambah Rincian"  title="Tambah Rincian" class="linkbox">
		   		<button type="button"  class="btn-success" ><span class="fa fa-plus-circle"></span>Tambah</button>              
		   </a>
		</div>
	</div>
</div>


