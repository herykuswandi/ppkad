<script>
$(document).ready(function(){

	$("#look_kelompok").hlookup({
		url:'?act=lookup&do=get_std_kelompok',
		url_detail:'?act=lookup&do=get_std_kelompok_detail',
		title: 'Data Kelompok',
		onComplete:function(data){
					$('#id_std_kelompok').val(data.id);
					$('#kode_kelompok').val(data.kode);
					$('#nama_kelompok').html(data.nama);
		}
	});

	$('#former input[type="text"], #former select').tooltipster({
        trigger: 'custom',
        onlyOne: false,
        position: 'top'
    });
	
	$('#former').validate({
		  rules: {
			nama: {
	        required: true
	      },
			kode_jenis: {
	        required: true
	      },
			kode_kelompok: {
	        required: true
	      },
			look_kelompok: {
	        required: true
	      }
	    },	
		    errorPlacement: function (error, element) {
            $(element).tooltipster('update', $(error).text());
			($(error).text()!='') ? $(element).tooltipster('show') : $(element).tooltipster('hide');
        },
			success: function(label,element) {
			$(element).tooltipster('hide');
		 },
		    submitHandler: function (form) { 
			saveData('former','?act=rek_std&do=simpan_jenis','tabel-jenis','',function(data){
				if(data=="failed"){
					alert("Maaf data gagal di simpan. Kode jenis yang anda masukan sudah ada");
				}
			});
        }
	  }); 
	
    $("#kode_jenis").mask("9",{
		placeholder: '_',
    });

});
</script>

<form method="post" id="former" style="width:100%">
<input type="hidden" value="<?php echo $id;?>" name="id">

<crow  class="no-border"><label><tag>Kelompok :</tag>
<input type="text" id="look_kelompok" placeholder="Kode / Nama Kelompok" name="look_kelompok" size="20" value="<?php echo $detail['kode_kelompok']?>"  />
<input type="hidden" id="id_std_kelompok" name="id_std_kelompok" value="<?php echo $detail['id_std_kelompok']?>"  />
</label></crow>

<crow class="row-ket"><label><tag>&nbsp;</tag>
<span id="nama_kelompok"><?php echo $detail['nama_kelompok']?></span>
</label></crow>


<crow><label><tag>Kode :</tag>
<div class="group-text">
	<input type="text" size="5" name="kode_kelompok" id="kode_kelompok" value="<?php echo $detail['kode_kelompok']; ?>" readonly="readonly">
	<input type="text" size="5" name="kode_jenis" id="kode_jenis" value="<?php echo substr($detail['kode'],1); ?>" >
</div>

<!-- <input type="text" name="kode" id="kode_jenis" value="<?php echo $detail['kode'];?>"  /> -->
</label></crow>

<crow><label><tag>Nama  :</tag>
<input type="text" name="nama" value="<?php echo $detail['nama'];?>" size="40" />
</label></crow>


<div class="nav-control">
<button type="submit"  class="btn-primary " ><span class="fa fa-floppy-o"></span>Simpan</button>
<button type="reset"  class="btn-info " ><span class="fa fa-share-square-o"></span>Reset</button>
<button type="button"  class="btn-danger"   onClick="closed()"><span class="fa fa-times"></span>Batal</button>
</div>
</form>