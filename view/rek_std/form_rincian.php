<script>
$(document).ready(function(){
	$("#look_kelompok").keyup(function(){
		if($(this).val()=="")
		{
			$('#id_std_jenis').val('');
			$('#kode_jenis').val('');
			$('#nama_jenis').html('');

			$("#look_jenis").val('');
			
			$("#look_jenis").keyup();

			$("#look_jenis").attr('readonly','readonly');
		}
	});


	function lukup_jenis(){
		$("#look_jenis").removeAttr('readonly');
		$("#look_jenis").hlookup({
			url:'?act=lookup&do=get_std_jenis&id_std_kelompok='+$('#id_std_kelompok').val(),
			url_detail:'?act=lookup&do=get_std_jenis_detail&id_std_kelompok='+$('#id_std_kelompok').val(),
			title: 'Data Jenis '+$('#nama_kelompok').html(),
			onComplete:function(data){
						$('#id_std_jenis').val(data.id);
						$('#kode_jenis').val(data.kode);
						$('#nama_jenis').html(data.nama);
			}
		});
	}
	<?php if($id>0){?>
		lukup_jenis();
	<?php }?>
	$("#look_kelompok").hlookup({
		url:'?act=lookup&do=get_std_kelompok',
		url_detail:'?act=lookup&do=get_std_kelompok_detail',
		title: 'Data Kelompok',
		onComplete:function(data){
					$('#id_std_kelompok').val(data.id);
					// $('#kode_kelompok').val(data.kode);
					$('#nama_kelompok').html(data.nama);

					$('#id_std_jenis').val('');
					$('#kode_jenis').val('');
					$('#nama_jenis').html('');

					$("#look_jenis").val('');
					lukup_jenis();
		}
	});

	

	$('#former input[type="text"], #former select').tooltipster({
        trigger: 'custom',
        onlyOne: false,
        position: 'top'
    });
	
	$('#former').validate({
		  rules: {
			nama: {
	        required: true
	      },
			kode_rincian: {
	        required: true
	      },
			kode_jenis: {
	        required: true
	      },
			look_kelompok: {
	        required: true
	      },
			look_rincian: {
	        required: true
	      },
			harga: {
	        required: true
	      },
			satuan: {
	        required: true
	      }
	    },	
		    errorPlacement: function (error, element) {
            $(element).tooltipster('update', $(error).text());
			($(error).text()!='') ? $(element).tooltipster('show') : $(element).tooltipster('hide');
        },
			success: function(label,element) {
			$(element).tooltipster('hide');
		 },
		    submitHandler: function (form) { 
			saveData('former','?act=rek_std&do=simpan_rincian','tabel-rincian','',function(data){
				if(data=="failed"){
					alert("Maaf data gagal di simpan. Kode rincian yang anda masukan sudah ada");
				}
			});
        }
	  }); 
	
    $("#kode_rincian").mask("9",{
		placeholder: '_',
    });

});
</script>

<form method="post" id="former" style="width:100%">
<input type="hidden" value="<?php echo $id;?>" name="id">

<crow class="no-border"><label><tag>Kelompok :</tag>
<input type="text" id="look_kelompok" placeholder="Kode / Nama Kelompok" name="look_kelompok" size="20" value="<?php echo $detail['kode_kelompok']?>"  />
<input type="hidden" id="id_std_kelompok" name="id_std_kelompok" value="<?php echo $detail['id_std_kelompok']?>"  />
</label></crow>

<crow class="row-ket"><label><tag>&nbsp;</tag>
<span id="nama_kelompok"><?php echo $detail['nama_kelompok']?></span>
</label></crow>


<crow  class="no-border"><label><tag>Jenis :</tag>
<input type="text" id="look_jenis" placeholder="Kode / Nama Jenis" name="look_jenis" size="20" value="<?php echo $detail['kode_jenis']?>"  readonly="readonly" />
<input type="hidden" id="id_std_jenis" name="id_std_jenis" value="<?php echo $detail['id_std_jenis']?>"  />
</label></crow>

<crow class="row-ket"><label><tag>&nbsp;</tag>
<span id="nama_jenis"><?php echo $detail['nama_jenis']?></span>
</label></crow>



<crow><label><tag>Kode :</tag>
<div class="group-text">
	<!-- <input type="text" size="5" name="kode_kelompok" id="kode_kelompok" value="<?php echo $detail['kode_kelompok']; ?>" readonly="readonly"> -->
	<input type="text" size="5" name="kode_jenis" id="kode_jenis" value="<?php echo $detail['kode_jenis']; ?>" readonly="readonly">
	<input type="text" size="5" name="kode_rincian" id="kode_rincian" value="<?php echo substr($detail['kode'],5); ?>" >
</div>

<!-- <input type="text" name="kode" id="kode_jenis" value="<?php echo $detail['kode'];?>"  /> -->
</label></crow>

<crow><label><tag>Nama  :</tag>
<input type="text" name="nama" value="<?php echo $detail['nama'];?>" size="40" />
</label></crow>

<crow><label><tag>Harga  :</tag>
<input type="text" name="harga" class="uang" value="<?php echo derupiah($detail['harga']);?>" size="20" />
</label></crow>


<crow><label><tag>Satuan:</tag>
<select name="id_satuan">
<?php foreach($satuan as $jentim){?>
<option value="<?php echo $jentim['id'];?>" <?php echo ($jentim['id']==$detail['id_satuan']) ? 'selected="selected"' : '';?>><?php echo $jentim['nama'];?></option>
<?php }?>
</select>	
</label></crow>

<crow><label><tag>Keterangan  :</tag>
<textarea name="keterangan" cols="30"  ><?php echo $detail['nama'];?></textarea>
</label></crow>



<div class="nav-control">
<button type="submit"  class="btn-primary " ><span class="fa fa-floppy-o"></span>Simpan</button>
<button type="reset"  class="btn-info " ><span class="fa fa-share-square-o"></span>Reset</button>
<button type="button"  class="btn-danger"   onClick="closed()"><span class="fa fa-times"></span>Batal</button>
</div>
</form>	