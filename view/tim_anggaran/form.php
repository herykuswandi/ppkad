<script>
$(document).ready(function(){
	$('#former input[type="text"], #former select').tooltipster({
        trigger: 'custom',
        onlyOne: false,
        position: 'top'
    });
	
	$('#former').validate({
		  rules: {
			nama: {
	        required: true
	      },
			no_urut: {
	        required: true
	      },
			nip: {
	        required: true
	      },
			jabatan: {
	        required: true
	      }
	    },	
		    errorPlacement: function (error, element) {
            $(element).tooltipster('update', $(error).text());
			($(error).text()!='') ? $(element).tooltipster('show') : $(element).tooltipster('hide');
        },
			success: function(label,element) {
			$(element).tooltipster('hide');
		 },
		    submitHandler: function (form) { 
			saveData('former','?act=tim_anggaran&do=simpan','','',function(data){
				if(data=="failed"){
					alert("Maaf data gagal di simpan. No Urut / Nama tim anggaran yang anda masukan sudah ada");
				}
			});
        }
	  }); 


});
</script>

<form method="post" id="former" style="width:100%">
<input type="hidden" value="<?php echo $id;?>" name="id">
<crow><label><tag>Tahun Anggaran :</tag>
<input type="hidden"  name="id_ta" value="<?php echo $detail['id_ta']?>" size="20" />
<input type="text"  name="tahun" value="<?php echo $detail['tahun']?>" size="10" readonly="readonly" />
</label></crow>
<crow><label><tag>No. Urut :</tag>
<input type="text"  name="no_urut" value="<?php echo $detail['no_urut']?>" size="10" />
</label></crow>
<crow><label><tag>Nama :</tag>
<input type="text"  name="nama" value="<?php echo $detail['nama']?>" size="40" />
</label></crow>

<crow><label><tag>NIP :</tag>
<input type="text"  name="nip" value="<?php echo $detail['nip']?>" size="40" />
</label></crow>

<crow><label><tag>Jabatan :</tag>
<input type="text"  name="jabatan" value="<?php echo $detail['jabatan']?>" size="40" />
</label></crow>

<crow><label><tag>Jenis Tim :</tag>
<select name="id_jenis_tim">
<?php foreach($jenis_tim as $jentim){?>
<option value="<?php echo $jentim['id'];?>" <?php echo ($jentim['id']==$detail['id_jenis_tim']) ? 'selected="selected"' : '';?>><?php echo $jentim['nama'];?></option>
<?php }?>
</select>	
</label></crow>

<div class="nav-control">
<button type="submit"  class="btn-primary " ><span class="fa fa-floppy-o"></span>Simpan</button>
<button type="reset"  class="btn-info " ><span class="fa fa-share-square-o"></span>Reset</button>
<button type="button"  class="btn-danger"   onClick="closed()"><span class="fa fa-times"></span>Batal</button>
</div>
</form>