<script>
$(document).ready(function(){
	$('#former input[type="text"], #former select,#former textarea').tooltipster({
        trigger: 'custom',
        onlyOne: false,
        position: 'top'
    });
	
	$('#former').validate({
		  rules: {
			no_urut: {
	        required: true
	      },
			nomor: {
	        required: true
	      },
			tanggal: {
	        required: true
	      	, date:true
	      },
			uraian: {
	        required: true
	      }
	    },	
		    errorPlacement: function (error, element) {
            $(element).tooltipster('update', $(error).text());
			($(error).text()!='') ? $(element).tooltipster('show') : $(element).tooltipster('hide');
        },
			success: function(label,element) {
			$(element).tooltipster('hide');
		 },
		    submitHandler: function (form) { 
			saveData('former','?act=perda&do=simpan','','',function(data){
				if(data=="failed"){
					alert("Maaf data gagal di simpan. No Urut Peraturan Daerah yang anda masukan sudah ada");
				}
			});
        }
	  }); 


});
</script>

<form method="post" id="former" style="width:100%">
<input type="hidden" value="<?php echo $id;?>" name="id">

<crow><label><tag>Tahun Anggaran :</tag>
<input type="hidden"  name="id_ta" value="<?php echo $detail['id_ta']?>" size="20" />
<input type="text"  name="tahun" value="<?php echo $detail['tahun']?>" size="10" readonly="readonly" />
</label></crow>


 <crow><label><tag>No. Urut :</tag>
<input type="text"  name="no_urut" value="<?php echo $detail['no_urut']?>" size="10" />
</label></crow>

<crow><label><tag>Jenis Peraturan :</tag>
<select name="jenis">
<?php foreach($jenis_peraturan as $jentim){?>
<option value="<?php echo $jentim['id'];?>" <?php echo ($jentim['id']==$detail['id_jenis_tim']) ? 'selected="selected"' : '';?>><?php echo $jentim['nama'];?></option>
<?php }?>
</select>	
</label></crow>


<crow><label><tag>Nomor :</tag>
<input type="text"  name="nomor" value="<?php echo $detail['nomor']?>" size="40" />
</label></crow>

<crow><label><tag>Tanggal :</tag>
<input type="text"  name="tanggal" value="<?php echo $detail['tanggal']?>" class="tgl" size="40" />
</label></crow>

<crow><label><tag>Uranian :</tag>
<textarea name="uraian" cols="30"><?php echo $detail['uraian'];?></textarea>	

</label></crow>


<div class="nav-control">
<button type="submit"  class="btn-primary " ><span class="fa fa-floppy-o"></span>Simpan</button>
<button type="reset"  class="btn-info " ><span class="fa fa-share-square-o"></span>Reset</button>
<button type="button"  class="btn-danger"   onClick="closed()"><span class="fa fa-times"></span>Batal</button>
</div>
</form>