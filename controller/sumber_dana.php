<?php
class Sumber_dana extends Yaridhane {

    private $page;
    private $method;
    private $vars = array();

    function __construct($page, $method) {
        $this->page = $page;
        $this->method = $method;
        parent::__construct();
    }
	
	
	// start tim_anggaran
    function __default() {
	
        $this->vars['title'] = 'Sumber Dana';
        $flex_config = array(
            fields => array(
                array(display => 'Opsi', name => 'Option', width => '70', align => 'center'),
                array(display => 'Kode', name => 'kode', align => 'left', width => '80', sortable => true, filter => true), 
                array(display => 'Nama', name => 't.nama', align => 'left', width => '300', sortable => true, filter => true), 
                ),
            url => '?act=sumber_dana&do=grid',
            showpage => '10',
			isprint=>$this->vars['isprint']
        );
        $grid = $this->render_grid($flex_config);
        $this->vars['grid'] = $grid;
        $this->loadView($this->page, $this->method, $this->vars,true);
    }

    function grid() {
        $tbl = 'sumber_dana t'; // nama tabel
        $sel = 't.*'; // default * 
        $id_ta=get_ses('id_ta');
        
        $where = "t.id_ta='$id_ta'"; // jika where kosong isi dengan 1
        $order='t.kode';
        $direction='asc';
        $hasil = $this->crud->get_data_flexi($tbl, $where, $sel, $order,$direction);
		while ($row = mysql_fetch_assoc($hasil['exe'])) {
            $data['page'] = $hasil['page'];
            $data['total'] = $hasil['total'];
            $data['rows'][] = array(
                'id' => $row['id'],
                'cell' => array(
                    '<a href="?act=sumber_dana&do=form&id=' . $row['id'] . '" onClick="return modaler(this);"  class="ficon-green fa fa-pencil-square-o fa-lg linkbox"  label="Ubah Sumber Daerah"  title="Ubah sumber dana"> </a> 
					<a href="javascript:void(0)" onClick="del_data(\'?act=sumber_dana&do=hapus&id=' . $row['id'] . '\',this)" class="ficon-red fa fa-trash-o fa-lg "   title="Hapus Sumber Dana" ></a>
					'
					,
                    $row['kode'],
                    $row['nama']
                    )
            );
        }

    	if($this->vars['isprint']){
			$this->vars['data']=$data;
		}else{
      	  echo json_encode($data);
		  die();
      	}    
        
    }
	 function form() {
       	$id = $_REQUEST['id'];
		if ($id) {
        	$this->vars['detail'] = $this->crud->get_single_data('sumber_dana t,tahun_anggaran a', "t.id='$id' and t.id_ta=a.id","t.*,a.tahun");

		}else{
            $this->vars['detail']['id_ta']=get_ses('id_ta');
            $this->vars['detail']['tahun']=get_ses('ta');

        }
	    $this->vars['id'] = $id;
		$this->loadView($this->page, $this->method, $this->vars, true);
    }
	
	 function simpan() {
 		$id=$_REQUEST['id'];
        if ($id) { // update
            $akun = $this->crud->update($rows, 'sumber_dana', true,"","id");
        } else { // create
            $id_ta=get_ses('id_ta');
         	$cek= $this->crud->get_single_data('sumber_dana', "(kode='$_REQUEST[kode]' or nama='$_REQUEST[nama]') and id_ta='$id_ta'");
         	if(is_array($cek)){
         		echo "failed";
         		die();
         	}
		    $akun = $this->crud->create($rows, 'sumber_dana', true);
        }
    }

    function hapus() {
        $id = $_REQUEST['id'];
		$this->crud->delete_data("sumber_dana","id='$id'");
	}
	
	
}

?>
