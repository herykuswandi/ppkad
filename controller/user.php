<?php
class User extends Yaridhane {

    private $page;
    private $method;
    private $vars = array();

    function __construct($page, $method) {
        $this->page = $page;
        $this->method = $method;
        parent::__construct();
    }
	
	
	// start karyawan
    function __default() {
	
        $this->vars['title'] = 'Data User';
        $flex_config = array(
            fields => array(
                array(display => 'Opsi', name => 'Option', hide => $show, width => '50', align => 'center'),
                array(display => 'Foto', name => 'u.nama', align => 'left', width => '50', sortable => true, filter => true), 
                array(display => 'Nama Lengkap', name => 'u.nama', align => 'left', width => '120', sortable => true, filter => true), 
                array(display => 'Username', name => 'username', align => 'left', width => '150', sortable => true, filter => true), 
                array(display => 'Group', name => 'a.nama', align => 'left', width => '100', sortable => true),  
                array(display => 'Telepon', name => 'telepon', align => 'left', width => '100', sortable => true), 
                array(display => 'Email', name => 'email', align => 'left', width => '100', sortable => true), 
                array(display => 'Kota', name => 'kota', align => 'left', width => '100', sortable => true), 
                array(display => 'Tgl Lahir', name => 'tgl_lahir', align => 'left', width => '100', sortable => true), 
                array(display => 'Alamat', name => 'alamat', align => 'left', width => '150', sortable => true), 
                array(display => 'Urusan', name => 'urusan', align => 'left', width => '150', sortable => true), 
                array(display => 'Bidang', name => 'bidang', align => 'left', width => '150', sortable => true), 
                array(display => 'Unit', name => 'unit', align => 'left', width => '150', sortable => true),
                array(display => 'Sub Unit', name => 'sub_unit', align => 'left', width => '150', sortable => true),  
               ),
            url => '?act=user&do=grid',
            showpage => '10'
        );
        $grid = $this->render_grid($flex_config);
        $this->vars['grid'] = $grid;
        $this->loadView($this->page, $this->method, $this->vars,true);
    }

    function grid() {
        $tbl = 'akses a,user u left join urusan r on r.id=u.id_urusan left join bidang b on b.id=u.id_bidang left join unit t on t.id=u.id_unit left join sub_unit s on s.id=u.id_sub_unit'; // nama tabel
        $sel = 'u.*,a.nama level,r.kode kode_urusan,r.nama nama_urusan,b.kode kode_bidang,b.nama nama_bidang,t.kode kode_unit,t.nama nama_unit,s.kode kode_sub_unit,s.nama nama_sub_unit'; // default * 
        $where = "u.id_akses=a.id"; // jika where kosong isi dengan 1
        $order='u.nama';
        $direction='asc';
        $hasil = $this->crud->get_data_flexi($tbl, $where, $sel, $order,$direction);
	
        while ($row = mysql_fetch_assoc($hasil['exe'])) {
            $data['page'] = $hasil['page'];
            $data['total'] = $hasil['total'];
            $data['rows'][] = array(
                'cell' => array(
                    '<a href="?act=user&do=form&id=' . $row['id'] . '" onClick="return modaler(this);"  class="ficon-green fa fa-pencil-square-o fa-lg linkbox"  label="Ubah Data User"  title="Ubah User"> </a> 
					<a href="javascript:void(0)" onClick="del_data(\'?act=user&do=hapus&id=' . $row['id'] . '\',this)" class="ficon-red fa fa-trash-o fa-lg "   title="Hapus User" ></a>'
,
                    '<img src="'.$row['foto'].'" style="max-height:30px;"/>',
                    $row['nama'],
                    $row['username'],
                    $row['level'],
                    $row['telepon'],
                    $row['email'],
                    $row['kota'],
                    $row['tgl_lahir'],
                    $row['alamat'],
                    $row['nama_urusan'],
                    $row['nama_bidang'],
                    $row['nama_unit'],
                    $row['nama_sub_unit']
                    )
            );
        }
        echo json_encode($data);
        die();
    }

     function form() {
        $id = $_REQUEST['id'];
        $kdo=get_ses("kdo");
        if ($id>0) {
            $this->vars['detail'] = $this->crud->get_single_data('user left join urusan u on u.id=user.id_urusan left join bidang b on b.id=user.id_bidang left join unit t on t.id=user.id_unit left join sub_unit s on s.id=user.id_sub_unit', "user.id='$id'","user.*,u.kode kode_urusan,u.nama nama_urusan,b.kode kode_bidang,b.nama nama_bidang,t.kode kode_unit,t.nama nama_unit,s.kode kode_sub_unit,s.nama nama_sub_unit");
        }
   
        $this->vars['akses'] = $this->crud->get_data('akses', "1", "id,nama");
        $this->vars['id'] = $id;
        $this->loadView($this->page, $this->method, $this->vars, true);
    }
	
	 function simpan() {
		$id=$_REQUEST['id'];
		 $rows = array();
        if ($id > 0) {
            $akun = $this->crud->update($rows, 'user', true);
        } else {			
	        $akun = $this->crud->create($rows, 'user', true);
        }
    }

    function hapus() {
        $id = $_REQUEST['id'];
        $this->crud->delete_data("urusan","id='$id'");

    }
	// end data user
    	//prfl

     function profil() {
        $id=$_SESSION['uniqsinad'];
        $this->vars['detail'] = $this->crud->get_single_data('user left join urusan u on u.id=user.id_urusan left join bidang b on b.id=user.id_bidang left join unit t on t.id=user.id_unit left join sub_unit s on s.id=user.id_sub_unit', "user.id='$id'","user.*,u.kode kode_urusan,u.nama nama_urusan,b.kode kode_bidang,b.nama nama_bidang,t.kode kode_unit,t.nama nama_unit,s.kode kode_sub_unit,s.nama nama_sub_unit");
        $this->vars['id'] = $id;
        $this->loadView($this->page, $this->method, $this->vars, true);
    
    }
     function update_profil() {
        $id=$_SESSION['uniqsinad'];
        $rows = array("id"=>$id);
            $akun = $this->crud->update($rows, 'user', true);

    }
 
	
}

?>
