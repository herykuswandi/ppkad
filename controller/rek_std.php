<?php
class Rek_std extends Yaridhane {

    private $page;
    private $method;
    private $vars = array();

    function __construct($page, $method) {
        $this->page = $page;
        $this->method = $method;
        parent::__construct();
    }
	
	
	// start kelompok
    function __default() {
	
        $this->vars['title'] = 'Standar Harga';
		
		// kelompok
        $flex_config = array(
            fields => array(
                array(display => 'Opsi', name => 'Option', width => '70', align => 'center'),
                array(display => 'Kode', name => 'u.kode', align => 'left', width => '60', sortable => true, filter => true), 
                array(display => 'Nama', name => 'u.nama', align => 'left', width => '200', sortable => true, filter => true)
                ),
            url => '?act=rek_std&do=grid_kelompok',
            showpage => '10',
			isprint=>$this->vars['isprint']
        );
        $grid = $this->render_grid($flex_config,'tabel-kelompok');
        
        // jenis
        $flex_config = array(
            fields => array(
                array(display => 'Opsi', name => 'Option', width => '70', align => 'center'),
                array(display => 'Kode', name => 'b.kode', align => 'left', width => '60', sortable => true, filter => true), 
                array(display => 'Nama', name => 'b.nama', align => 'left', width => '200', sortable => true, filter => true),
                array(display => 'Kelompok', name => 'u.nama', align => 'left', width => '200', sortable => true, filter => true), 
                ),
            url => '?act=rek_std&do=grid_jenis',
            showpage => '10',
			isprint=>$this->vars['isprint']
        );
        $grid .= $this->render_grid($flex_config,'tabel-jenis');
        
        // rincian
        $flex_config = array(
            fields => array(
                array(display => 'Opsi', name => 'Option', width => '70', align => 'center'),
                array(display => 'Kode', name => 't.kode', align => 'left', width => '60', sortable => true, filter => true), 
                array(display => 'Nama', name => 't.nama', align => 'left', width => '180', sortable => true, filter => true), 
                array(display => 'Harga', name => 't.nama', align => 'left', width => '120', sortable => true, filter => true), 
                array(display => 'Satuan', name => 's.nama', align => 'left', width => '80', sortable => true, filter => true), 
                array(display => 'Keterangan', name => 't.nama', align => 'left', width => '200', sortable => true, filter => true), 
                array(display => 'Kelompok', name => 'u.nama', align => 'left', width => '200', sortable => true, filter => true), 
                array(display => 'Jenis', name => 'b.nama', align => 'left', width => '200', sortable => true, filter => true) 
                ),
            url => '?act=rek_std&do=grid_rincian',
            showpage => '10',
			isprint=>$this->vars['isprint']
        );
        $grid .= $this->render_grid($flex_config,'tabel-rincian');

       
        
        $this->vars['grid'] = $grid;
        $this->loadView($this->page, $this->method, $this->vars,true);
    }

    function grid_kelompok() {
        $tbl = 'std_kelompok u'; // nama tabel
        $sel = '*'; // default * 
        $where = "1"; // jika where kosong isi dengan 1
        $order='u.kode';
        $direction='asc';
        $hasil = $this->crud->get_data_flexi($tbl, $where, $sel, $order,$direction);
		
        while ($row = mysql_fetch_assoc($hasil['exe'])) {
            $data['page'] = $hasil['page'];
            $data['total'] = $hasil['total'];
            $data['rows'][] = array(
                'id' => $row['id'],
                'cell' => array(
                    '<a href="?act=rek_std&do=form_kelompok&id=' . $row['id'] . '" onClick="return modaler(this);"  class="ficon-green fa fa-pencil-square-o fa-lg linkbox"  label="Ubah Data Kelompok"  title="Ubah Kelompok"> </a> 
					<a href="javascript:void(0)" onClick="del_data(\'?act=rek_std&do=hapus_kelompok&id=' . $row['id'] . '\',this,\'tabel-kelompok\')" class="ficon-red fa fa-trash-o fa-lg temp-delete"   title="Hapus Kelompok" ></a>
					'
					,
                    $row['kode'],
                    $row['nama']
                    )
            );
        }
     	  echo json_encode($data);
		  die();
        
    }

	 function form_kelompok() {
       	$id = $_REQUEST['id'];
		if ($id) {
        	$this->vars['detail'] = $this->crud->get_single_data('std_kelompok', "id='$id'");
		}
	    $this->vars['id'] = $id;
		$this->loadView($this->page, $this->method, $this->vars, true);
    }
	
	 function simpan_kelompok() {
 		$id=$_REQUEST['id'];
        if ($id) { // update
            $kelompok = $this->crud->update($rows, 'std_kelompok', true,"","id");
        } else { // create
         	$cek= $this->crud->get_single_data('std_kelompok', "kode='$_REQUEST[kode]'");
         	if(is_array($cek)){
         		echo "failed";
         		die();
         	}
		    $kelompok = $this->crud->create($rows, 'std_kelompok', true);
        }
    }

    function hapus_kelompok() {
        $id = $_REQUEST['id'];
		$this->crud->delete_data("std_kelompok","id='$id'");
	}
	
// jenis
	    function grid_jenis() {
        $tbl = 'std_jenis b,std_kelompok u'; // nama tabel
        $sel = 'b.*,u.nama nama_kelompok'; // default * 
        $where = "u.id=b.id_std_kelompok"; // jika where kosong isi dengan 1
        $order='b.kode';
        $direction='asc';
        $hasil = $this->crud->get_data_flexi($tbl, $where, $sel, $order,$direction);
		
        while ($row = mysql_fetch_assoc($hasil['exe'])) {
            $data['page'] = $hasil['page'];
            $data['total'] = $hasil['total'];
            $data['rows'][] = array(
                'id' => $row['id'],
                'cell' => array(
                    '<a href="?act=rek_std&do=form_jenis&id=' . $row['id'] . '" onClick="return modaler(this);"  class="ficon-green fa fa-pencil-square-o fa-lg linkbox"  label="Ubah Data Jenis"  title="Ubah Jenis"> </a> 
					<a href="javascript:void(0)" onClick="del_data(\'?act=rek_std&do=hapus_jenis&id=' . $row['id'] . '\',this,\'tabel-jenis\')" class="ficon-red fa fa-trash-o fa-lg temp-delete"   title="Hapus Jenis" ></a>
					'
					,
                    $row['kode'],
                    $row['nama'],
                    $row['nama_kelompok']
                    )
            );
        }
     	  echo json_encode($data);
		  die();
        
    }

	 function form_jenis() {
       	$id = $_REQUEST['id'];
		if ($id) {
        	$this->vars['detail'] = $this->crud->get_single_data('std_jenis b, std_kelompok u', "b.id='$id' and b.id_std_kelompok=u.id","b.*,u.kode kode_kelompok,u.nama nama_kelompok");
		}
	    $this->vars['id'] = $id;
		$this->loadView($this->page, $this->method, $this->vars, true);
    }
	
	 function simpan_jenis() {
 		$id=$_REQUEST['id'];
 		$kode=$_REQUEST['kode_kelompok'].'.'.$_REQUEST['kode_jenis'];
 		$rows=array('kode'=>$kode);
        if ($id) { // update
            $kelompok = $this->crud->update($rows, 'std_jenis', true,"","id");
        } else { // create
         	$cek= $this->crud->get_single_data('std_jenis', "kode='$kode'");
         	if(is_array($cek)){
         		echo "failed";
         		die();
         	}
		    $kelompok = $this->crud->create($rows, 'std_jenis', true);
        }
    }

    function hapus_jenis() {
        $id = $_REQUEST['id'];
		$this->crud->delete_data("std_jenis","id='$id'");
	}
	

// rincian
	    function grid_rincian() {
        $tbl = 'std_rincian t, std_jenis b,std_kelompok u,satuan s'; // nama tabel
        $sel = 't.*,b.nama nama_jenis,u.nama nama_kelompok,s.nama nama_satuan'; // default * 
        $where = "t.id_std_jenis=b.id and u.id=b.id_std_kelompok and s.id=t.id_satuan"; // jika where kosong isi dengan 1
        $order='t.kode';
        $direction='asc';
        $hasil = $this->crud->get_data_flexi($tbl, $where, $sel, $order,$direction);
		
        while ($row = mysql_fetch_assoc($hasil['exe'])) {
            $data['page'] = $hasil['page'];
            $data['total'] = $hasil['total'];
            $data['rows'][] = array(
                'id' => $row['id'],
                'cell' => array(
                    '<a href="?act=rek_std&do=form_rincian&id=' . $row['id'] . '" onClick="return modaler(this);"  class="ficon-green fa fa-pencil-square-o fa-lg linkbox"  label="Ubah Data Rincian"  title="Ubah Rincian"> </a> 
					<a href="javascript:void(0)" onClick="del_data(\'?act=rek_std&do=hapus_rincian&id=' . $row['id'] . '\',this,\'tabel-rincian\')" class="ficon-red fa fa-trash-o fa-lg temp-delete"   title="Hapus Rincian" ></a>
					'
					,
                    $row['kode'],
                    $row['nama'],
                    rupiah($row['harga']),
                    $row['nama_satuan'],
                    $row['keterangan'],
                    $row['nama_kelompok'],
                    $row['nama_jenis']
                    )
            );
        }
     	  echo json_encode($data);
		  die();
        
    }

	 function form_rincian() {
       	$id = $_REQUEST['id'];
		if ($id) {
        	$this->vars['detail'] = $this->crud->get_single_data('std_rincian t, std_jenis b, std_kelompok u', "t.id='$id' and t.id_std_jenis=b.id and b.id_std_kelompok=u.id","t.*,u.kode kode_kelompok,u.nama nama_kelompok,b.kode kode_jenis,b.nama nama_jenis");
		}

        $this->vars['satuan']=$this->crud->get_data('satuan');
	    $this->vars['id'] = $id;
		$this->loadView($this->page, $this->method, $this->vars, true);
    }
	
	 function simpan_rincian() {
 		$id=$_REQUEST['id'];
 		$kode=$_REQUEST['kode_jenis'].'.'.$_REQUEST['kode_rincian'];
 		$rows=array('kode'=>$kode);
        if ($id) { // update
            $kelompok = $this->crud->update($rows, 'std_rincian', true,"","id");
        } else { // create
         	$cek= $this->crud->get_single_data('std_rincian', "kode='$kode'");
         	if(is_array($cek)){
         		echo "failed";
         		die();
         	}
		    $kelompok = $this->crud->create($rows, 'std_rincian', true);
        }
    }
    function hapus_rincian() {
        $id = $_REQUEST['id'];
		$this->crud->delete_data("std_rincian","id='$id'");
	}
	



}

?>
