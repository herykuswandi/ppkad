<?php
class Bulan extends Yaridhane {

    private $page;
    private $method;
    private $vars = array();

    function __construct($page, $method) {
        $this->page = $page;
        $this->method = $method;
        parent::__construct();
    }
	
	
	// start karyawan
  
     function __default() {
        $this->vars['bulan'] = $_SESSION['bulan'];
        $this->vars['tahun'] = $_SESSION['tahun'];
        if($this->vars['tahun']==""){
            $this->vars['tahun']=date('Y');
        }
        if($this->vars['bulan']==""){
            $this->vars['bulan']=date('n');
        }
        $this->vars['bulans']=array('','Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember');
        $this->loadView($this->page, $this->method, $this->vars, true);
    }
	
	 function simpan() {
        $_SESSION['bulan']=$_REQUEST['bulan'];
        $_SESSION['tahun']=$_REQUEST['tahun'];
    }
 
    function ganti_tahun(){
        $id=$_REQUEST['id'];
                $detail_tang=$this->crud->get_single_data('tahun_anggaran',"id='$id'");
                $_SESSION['sinad']['id_ta']=$detail_tang['id'];  
                $_SESSION['sinad']['ta']=$detail_tang['tahun'];  
    }
	
}

?>
