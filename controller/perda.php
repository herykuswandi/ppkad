<?php
class Perda extends Yaridhane {

    private $page;
    private $method;
    private $vars = array();

    function __construct($page, $method) {
        $this->page = $page;
        $this->method = $method;
        parent::__construct();
    }
	
	
	// start tim_anggaran
    function __default() {
	
        $this->vars['title'] = 'Data Peraturan Daerah';
        $flex_config = array(
            fields => array(
                array(display => 'Opsi', name => 'Option', width => '70', align => 'center'),
                array(display => 'No. Urut', name => 't.no_urut', align => 'left', width => '80', sortable => true, filter => true), 
                array(display => 'Jenis Peraturan', name => 't.jenis', align => 'left', width => '150', sortable => true, filter => true), 
                array(display => 'Nomor', name => 't.nomor', align => 'left', width => '150', sortable => true, filter => true), 
                array(display => 'Tanggal', name => 't.tanggal', align => 'left', width => '150', sortable => true, filter => true), 
                array(display => 'Uraian', name => 't.uraian', align => 'left', width => '300', sortable => true, filter => true), 
                ),
            url => '?act=perda&do=grid',
            showpage => '10',
			isprint=>$this->vars['isprint']
        );
        $grid = $this->render_grid($flex_config);
        $this->vars['grid'] = $grid;
        $this->loadView($this->page, $this->method, $this->vars,true);
    }

    function grid() {
        $tbl = 'perda t'; // nama tabel
        $sel = 't.*'; // default * 
        $id_ta=get_ses('id_ta');
        
        $where = "t.id_ta='$id_ta'"; // jika where kosong isi dengan 1
        $order='t.no_urut';
        $direction='asc';
        $hasil = $this->crud->get_data_flexi($tbl, $where, $sel, $order,$direction);
		$jp=array('','Raperda APBD','Raperda Bupati','Penjabaran APBD','DPA SKPD');
    
        while ($row = mysql_fetch_assoc($hasil['exe'])) {
            $data['page'] = $hasil['page'];
            $data['total'] = $hasil['total'];
            $data['rows'][] = array(
                'id' => $row['id'],
                'cell' => array(
                    '<a href="?act=perda&do=form&id=' . $row['id'] . '" onClick="return modaler(this);"  class="ficon-green fa fa-pencil-square-o fa-lg linkbox"  label="Ubah Peraturan Daerah"  title="Ubah peraturan daerah"> </a> 
					<a href="javascript:void(0)" onClick="del_data(\'?act=perda&do=hapus&id=' . $row['id'] . '\',this)" class="ficon-red fa fa-trash-o fa-lg "   title="Hapus Peraturan Daerah" ></a>
					'
					,
                    $row['no_urut'],
                    $jp[$row['jenis']],
                    $row['nomor'],
                    $row['tanggal'],
                    $row['uraian']
                    )
            );
        }

    	if($this->vars['isprint']){
			$this->vars['data']=$data;
		}else{
      	  echo json_encode($data);
		  die();
      	}    
        
    }
	 function form() {
       	$id = $_REQUEST['id'];
		if ($id) {
        	$this->vars['detail'] = $this->crud->get_single_data('perda t,tahun_anggaran a', "t.id='$id' and t.id_ta=a.id","t.*,a.tahun");

		}else{
            $this->vars['detail']['id_ta']=get_ses('id_ta');
            $this->vars['detail']['tahun']=get_ses('ta');

        }
        $this->vars['jenis_peraturan']=array(
                                            array('id'=>1,'nama'=>'Raperda APBD'),
                                            array('id'=>2,'nama'=>'Raperda Bupati'),
                                            array('id'=>3,'nama'=>'Penjabaran APBD'),
                                            array('id'=>4,'nama'=>'DPA SKPD')
                                    );
	    $this->vars['id'] = $id;
		$this->loadView($this->page, $this->method, $this->vars, true);
    }
	
	 function simpan() {
 		$id=$_REQUEST['id'];
        if ($id) { // update
            $akun = $this->crud->update($rows, 'perda', true,"","id");
        } else { // create
            $id_ta=get_ses('id_ta');
         	$cek= $this->crud->get_single_data('perda', "(no_urut='$_REQUEST[no_urut]') and id_ta='$id_ta'");
         	if(is_array($cek)){
         		echo "failed";
         		die();
         	}
		    $akun = $this->crud->create($rows, 'perda', true);
        }
    }

    function hapus() {
        $id = $_REQUEST['id'];
		$this->crud->delete_data("perda","id='$id'");
	}
	
	
}

?>
