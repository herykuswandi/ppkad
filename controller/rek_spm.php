<?php
class Rek_spm extends Yaridhane {

    private $page;
    private $method;
    private $vars = array();

    function __construct($page, $method) {
        $this->page = $page;
        $this->method = $method;
        parent::__construct();
    }
	
	
	// start tim_anggaran
    function __default() {
	
        $this->vars['title'] = 'Data Rekening Potongan SPM';
        $flex_config = array(
            fields => array(
                array(display => 'Opsi', name => 'Option', width => '70', align => 'center'),
                array(display => 'Kode Rekening', name => 'r.kode', align => 'left', width => '120', sortable => true, filter => true), 
                array(display => 'Nama Rekening', name => 'r.nama', align => 'left', width => '200', sortable => true, filter => true), 
                array(display => 'Kode Potongan', name => 'p.kode', align => 'left', width => '120', sortable => true, filter => true), 
                array(display => 'Nama Potongan', name => 'p.nama', align => 'left', width => '200', sortable => true, filter => true), 
                ),
            url => '?act=rek_spm&do=grid',
            showpage => '10',
			isprint=>$this->vars['isprint']
        );
        $grid = $this->render_grid($flex_config);
        $this->vars['grid'] = $grid;
        $this->loadView($this->page, $this->method, $this->vars,true);
    }

    function grid() {
        $tbl = 'rek_spm b,apbd_rincian r,potongan p'; // nama tabel
        $sel = 'b.*,
                r.nama nama_apbd_rincian,r.kode kode_apbd_rincian,
                p.nama nama_potongan,p.kode kode_potongan
                '; // default * 
        $where = "b.id_apbd_rincian=r.id and b.id_potongan=p.id"; // jika where kosong isi dengan 1
        $order='r.kode';
        $direction='asc';
        $hasil = $this->crud->get_data_flexi($tbl, $where, $sel, $order,$direction);
		while ($row = mysql_fetch_assoc($hasil['exe'])) {
            $data['page'] = $hasil['page'];
            $data['total'] = $hasil['total'];
            $data['rows'][] = array(
                'id' => $row['id'],
                'cell' => array(
                    '<a href="?act=rek_spm&do=form&id=' . $row['id'] . '" onClick="return modaler(this);"  class="ficon-green fa fa-pencil-square-o fa-lg linkbox"  label="Ubah Rekening Potongan SPM"  title="Ubah Rekening Potongan SPM"> </a> 
					<a href="javascript:void(0)" onClick="del_data(\'?act=rek_spm&do=hapus&id=' . $row['id'] . '\',this)" class="ficon-red fa fa-trash-o fa-lg "   title="Hapus Rekening Potongan SPM" ></a>
					'
					,
                    $row['kode_apbd_rincian'],
                    $row['nama_apbd_rincian'],
                    $row['kode_potongan'],
                    $row['nama_potongan']
                    )
            );
        }

    	if($this->vars['isprint']){
			$this->vars['data']=$data;
		}else{
      	  echo json_encode($data);
		  die();
      	}    
        
    }
	 function form() {
       	$id = $_REQUEST['id'];
		if ($id) {
            $tbl = 'rek_spm b,apbd_rincian r,potongan p'; // nama tabel
            $sel = 'b.*,
                    r.nama nama_apbd_rincian,r.kode kode_apbd_rincian,
                    p.nama nama_potongan,p.kode kode_potongan
                    '; // default * 
            $where = "b.id_apbd_rincian=r.id and b.id_potongan=p.id and b.id='$id'"; // jika where kosong isi dengan 1
        	$this->vars['detail'] = $this->crud->get_single_data($tbl, $where,$sel);
		}
	    $this->vars['id'] = $id;
		$this->loadView($this->page, $this->method, $this->vars, true);
    }
	
	 function simpan() {
 		$id=$_REQUEST['id'];
       
        if ($id) { // update
            $apbd_akun = $this->crud->update($rows, 'rek_spm', true,"","id");
        } else { // create
         	$cek= $this->crud->get_single_data('rek_spm', "(id_apbd_rincian='$_REQUEST[id_apbd_rincian]' and id_potongan='$_REQUEST[id_potongan]') ");
         	if(is_array($cek)){
         		echo "failed";
         		die();
         	}
		    $apbd_akun = $this->crud->create($rows, 'rek_spm', true);
        }
    }

    function hapus() {
        $id = $_REQUEST['id'];
		$this->crud->delete_data("rek_spm","id='$id'");
	}
	
	
}

?>
