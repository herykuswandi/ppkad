<?php
class Tahun_anggaran extends Yaridhane {

    private $page;
    private $method;
    private $vars = array();

    function __construct($page, $method) {
        $this->page = $page;
        $this->method = $method;
        parent::__construct();
    }
	
	
	// start tahun_anggaran
    function __default() {
	
        $this->vars['title'] = 'Data Tahun Anggaran';
        $flex_config = array(
            fields => array(
                array(display => 'Opsi', name => 'Option', width => '70', align => 'center'),
                array(display => 'Tahun', name => 'tahun', align => 'left', width => '120', sortable => true, filter => true), 
                ),
            url => '?act=tahun_anggaran&do=grid',
            showpage => '10',
			isprint=>$this->vars['isprint']
        );
        $grid = $this->render_grid($flex_config);
        $this->vars['grid'] = $grid;
        $this->loadView($this->page, $this->method, $this->vars,true);
    }

    function grid() {
        $tbl = 'tahun_anggaran'; // nama tabel
        $sel = '*'; // default * 
        $where = "1"; // jika where kosong isi dengan 1
        $order='tahun';
        $direction='asc';
        $hasil = $this->crud->get_data_flexi($tbl, $where, $sel, $order,$direction);
		
        while ($row = mysql_fetch_assoc($hasil['exe'])) {
            $data['page'] = $hasil['page'];
            $data['total'] = $hasil['total'];
            $data['rows'][] = array(
                'id' => $row['id'],
                'cell' => array(
                    '<a href="?act=tahun_anggaran&do=form&id=' . $row['id'] . '" onClick="return modaler(this);"  class="ficon-green fa fa-pencil-square-o fa-lg linkbox"  label="Ubah Data tahun_anggaran"  title="Ubah tahun_anggaran"> </a> 
					<a href="javascript:void(0)" onClick="del_data(\'?act=tahun_anggaran&do=hapus&id=' . $row['id'] . '\',this)" class="ficon-red fa fa-trash-o fa-lg "   title="Hapus tahun_anggaran" ></a>
					'
					,
                    $row['tahun'],
                    )
            );
        }

    	if($this->vars['isprint']){
			$this->vars['data']=$data;
		}else{
      	  echo json_encode($data);
		  die();
      	}    
        
    }
	 function form() {
       	$id = $_REQUEST['id'];
		if ($id) {
        	$this->vars['detail'] = $this->crud->get_single_data('tahun_anggaran', "id='$id'");
		}
	    $this->vars['id'] = $id;
		$this->loadView($this->page, $this->method, $this->vars, true);
    }
	
	 function simpan() {
 		$id=$_REQUEST['id'];
        if ($id) { // update
            $akun = $this->crud->update($rows, 'tahun_anggaran', true,"","id");
        } else { // create
         	$cek= $this->crud->get_single_data('tahun_anggaran', "tahun='$_REQUEST[tahun]'");
         	if(is_array($cek)){
         		echo "failed";
         		die();
         	}
		    $akun = $this->crud->create($rows, 'tahun_anggaran', true);
        }
    }

    function hapus() {
        $id = $_REQUEST['id'];
		$this->crud->delete_data("tahun_anggaran","id='$id'");
	}
	
	
}

?>
