<?php
class Data_umum extends Yaridhane {

    private $page;
    private $method;
    private $vars = array();

    function __construct($page, $method) {
        $this->page = $page;
        $this->method = $method;
        parent::__construct();
    }
	
	
	// start data_umum
    function __default() {
	
        $this->vars['title'] = 'Data Umum Pemda';
        $flex_config = array(
            fields => array(
                array(display => 'Opsi', name => 'Option', width => '70', align => 'center'),
                array(display => 'Thn Anggaran', name => 'tahun', align => 'left', width => '100', sortable => true, filter => true), 
                array(display => 'Nama Pemda', name => 'nama_pemda', align => 'left', width => '200', sortable => true, filter => true), 
                array(display => 'Ibukota Pemda', name => 'ibukota_pemda', align => 'left', width => '120', sortable => true, filter => true), 
                array(display => 'Kepala Daerah', name => 'alamat_pemda', align => 'left', width => '170', sortable => true, filter => true), 
                array(display => 'Alamat Pemda', name => 'alamat_pemda', align => 'left', width => '200', sortable => true, filter => true) 
                ),
            url => '?act=data_umum&do=grid',
            showpage => '10',
			isprint=>$this->vars['isprint']
        );
        $grid = $this->render_grid($flex_config);
        $this->vars['grid'] = $grid;
        $this->loadView($this->page, $this->method, $this->vars,true);
    }

    function grid() {
    	$id_ta= get_ses('id_ta');
        $tbl = 'data_umum,tahun_anggaran'; // nama tabel
        $sel = 'data_umum.*,tahun_anggaran.tahun'; // default * 
        $where = "data_umum.id_ta=tahun_anggaran.id"; // jika where kosong isi dengan 1
        $order='tahun';
        $direction='asc';
        $hasil = $this->crud->get_data_flexi($tbl, $where, $sel, $order,$direction);
		
        while ($row = mysql_fetch_assoc($hasil['exe'])) {
            $data['page'] = $hasil['page'];
            $data['total'] = $hasil['total'];
            $data['rows'][] = array(
                'id' => $row['id'],
                'cell' => array(
                    '<a href="javascript:void(0)" onClick="ajaxload(\'?act=data_umum&do=form&id=' . $row['id'] . '\');"  class="ficon-green fa fa-pencil-square-o fa-lg linkbox"  label="Ubah Data Umum Pemda"  title="Ubah data umum pemda"> </a> 
					<a href="javascript:void(0)" onClick="del_data(\'?act=data_umum&do=hapus&id=' . $row['id'] . '\',this)" class="ficon-red fa fa-trash-o fa-lg "   title="Hapus data umum pemda" ></a>
					'
					,
                    $row['tahun'],
                    $row['nama_pemda'],
                    $row['ibukota_pemda'],
                    $row['nama_kepda'],
                    $row['alamat_pemda'],
                    )
            );
        }

    	if($this->vars['isprint']){
			$this->vars['data']=$data;
		}else{
      	  echo json_encode($data);
		  die();
      	}    
        
    }
	 function form() {
       	$id = $_REQUEST['id'];
		if ($id) {
        	$this->vars['detail'] = $this->crud->get_single_data('data_umum', "id='$id'");
		}

		$this->vars['tang']=$this->crud->get_data('tahun_anggaran');
	    $this->vars['id'] = $id;
		$this->loadView($this->page, $this->method, $this->vars, true);
    }
	
	 function simpan() {
 		$id=$_REQUEST['id'];
        if ($id) { // update
            $akun = $this->crud->update($rows, 'data_umum', true,"","id");
        } else { // create
         	$cek= $this->crud->get_single_data('data_umum', "id_ta='$_REQUEST[id_ta]'");
         	if(is_array($cek)){
         		echo "failed";
         		die();
         	}
         	// $id_ta=get_ses('id_ta');
         	// $rows=array('id_ta'=>$id_ta);
		    $akun = $this->crud->create($rows, 'data_umum', true);
        }
    }

    function hapus() {
        $id = $_REQUEST['id'];
		$this->crud->delete_data("data_umum","id='$id'");
	}
	
	

    function kartu() {
		$this->vars['isprint']=true;
		$this->grid();
		$this->render_print($this->vars,$this->page,$this->method );
		
	}

	// end data data_umum
	
	
	// start Absen data_umum
    function absen() {
	
        $this->vars['title'] = 'Absensi data_umum';
        $flex_config = array(
            fields => array(
               array(display => 'Kode', name => 'k.kode', align => 'left', width => '60', sortable => true, filter => true,printable=>true), 
                array(display => 'Nama', name => 'k.nama', align => 'left', width => '120', sortable => true, filter => true,printable=>true), 
                array(display => 'Status Kehadiran', name => 'status_hadir', align => 'left', width => '200', sortable => true,printable=>true),  	
				array(display => 'Jam', name => 'status_hadir', align => 'left', width => '80', sortable => true,printable=>true), 
                array(display => 'Keterangan', name => 'keterangan', align => 'left', width => '200', sortable => true, filter => true,printable=>true), 
               ),
            url => '?act=data_umum&do=grid_absen',
            showpage => '20',
			isprint=>$this->vars['isprint']
        );
        $grid = $this->render_grid($flex_config);
        $this->vars['grid'] = $grid;
        $this->loadView($this->page, $this->method, $this->vars,true);
    }

    function grid_absen() {
		$kdo=get_ses("kdo");
		$tgl=($_REQUEST['tgl']) ? $_REQUEST['tgl'] : date('Y-m-d');
        // select data absen 
		$where = "k.hapus='0' and a.kdo='$kdo' and k.kdo='$kdo' and a.kode_data_umum=k.kode and a.tanggal='$tgl'";
        $tbl = 'data_umum k,data_umum_absensi a';
        $sel = 'a.*';
		$absdata = $this->crud->get_data($tbl, $where, $sel);
		$absendata=array();
		if(is_array($absdata)){
			foreach($absdata as $absdatas){
				$absendata[$absdatas['kode_data_umum']]=$absdatas;
			}
		}
		// get flexi data
		$where = "k.hapus='0' and k.kdo='$kdo' ";
        $tbl = 'data_umum k';
        $sel = 'k.*';
		$hasil = $this->crud->get_data_flexi($tbl, $where, $sel, 'kode', 'asc');
		
        while ($row = mysql_fetch_assoc($hasil['exe'])) {
            $data['page'] = $hasil['page'];
            $data['total'] = $hasil['total'];
			$ai=$row['kode'];
			$vjam=($absendata[$ai]['jam']!="") ? $absendata[$ai]['jam'] : date('H:i:s');
				$jam='';
				
		if(!$this->vars['isprint']){
				$jam.='<span class="abshide"><input type="text" style="width:50px !important;" class="jam" value="'.$vjam.'" name="jam['.$ai.']" >
				</span>
				<input type="hidden" name="id['.$ai.']" value="'.$absendata[$ai]['id'].'">
				<input type="hidden" name="kode_data_umum['.$ai.']" value='.$ai.'>
				<input type="hidden" name="status_hadir_lama['.$ai.']" value="'.$absendata[$ai]['status_hadir'].'">
				<input type="hidden" name="keterangan_lama['.$ai.']" value="'.$absendata[$ai]['keterangan'].'">
				<input type="hidden" name="jam_lama['.$ai.']" value="'.$absendata[$ai]['jam'].'">';
			}		
				$jam.='<span class="absshow">
				'.$absendata[$ai]['jam'].'</span>';
			
				$sth=$absendata[$ai]['status_hadir'];
				$ceked=array();
				$ceked[$sth]='checked="checked"';
				$cabsen='-';
				switch($sth){
					case 1:
						$cabsen='Hadir';
					break;
					case 2:
						$cabsen='Ijin';
					
					break;
					case 3:
						$cabsen='Sakit';
					
					break;
					case 4:
						$cabsen='Alpha';
					
					break;
					default:
					
					break;
				}
				$radiohdr="";
				
		if(!$this->vars['isprint']){
			$radiohdr.='<span class="abshide">
			<input type="radio" name="radiostatus['.$ai.']"  onchange="setstatushadir(this)" '.$ceked['1'].' value="1" >Hadir
				<input type="radio" name="radiostatus['.$ai.']"  onchange="setstatushadir(this)"  '.$ceked['2'].' value="2" >Ijin
				<input type="radio" name="radiostatus['.$ai.']" onchange="setstatushadir(this)"  '.$ceked['3'].' value="3" >Sakit
				<input type="radio" name="radiostatus['.$ai.']" onchange="setstatushadir(this)" '.$ceked['4'].' value="4" >Alpha
				</span>
				<input type="hidden" name="status_hadir['.$ai.']"  value="'.$absendata[$ai]['status_hadir'].'">
				
			';
			}
			$radiohdr.='<span class="absshow">
				'.$cabsen.'</span>';
				
				$keterangan="";
			
		if(!$this->vars['isprint']){
			$keterangan.='<span class="abshide"><input type="text" value="'.$absendata[$ai]['keterangan'].'" name="keterangan['.$ai.']"></span>';
		}
				$keterangan.='
				<span class="absshow">
				'.$absendata[$ai]['keterangan'].'</span>';
            $data['rows'][] = array(
                'id' => $row['kode'],
                'cell' => array(
                   	$row['kode'],
                    $row['nama'],
                  	$radiohdr,
					$jam,
				  	$keterangan
                    )
            );
        }
		
		if($this->vars['isprint']){
			$this->vars['data']=$data;
		}else{
      	  echo json_encode($data);
		  die();
      	}
    }
	
	// sof print data absen
	 function print_data_absen(){
	 	$this->vars['isprint']=true;
		$this->absen();
		$this->grid_absen();
		
		$filters=array();
		
		if($_REQUEST['tgl']!="" ){
			$filters['Tanggal ']=$_REQUEST['tgl'];
		
		}
		


		// for searching
		if($_REQUEST['q']!=""){
			$pencari=$this->vars['grid']['filter'];
			foreach($pencari as $pcari){
				if($pcari['name']==$_REQUEST['qtype']){
					$filters['Pencarian']=$pcari['display'].' = "'.$_REQUEST['q'].'"';
				}
			}
		}
		$this->vars['title']="Rekap Pembayaran Gaji";
		$this->vars['filter']=$filters;
		$this->vars['settings']=array(
			'number'=>true,
			'counter'=>array(
				//'g.total_gaji' => array('Total Gaji','rupiah',"Rp. {g.total_gaji}",0)
			)
		);
		$this->render_print($this->vars);
	 }
	 
	 function simpan_absen() {
		$kdo=get_ses("kdo");
		$id = $_REQUEST['id'];
		$kode_data_umum = $_REQUEST['kode_data_umum'];
		$tanggal = $_REQUEST['tanggal'];
		$jam = $_REQUEST['jam'];
		$status_hadir = $_REQUEST['status_hadir'];
		$keterangan = $_REQUEST['keterangan'];
		// tmp lama
		$jam_lama = $_REQUEST['jam_lama'];
		$status_hadir_lama = $_REQUEST['status_hadir_lama'];
		$keterangan_lama = $_REQUEST['keterangan_lama'];
		
		 $rowabsen=array();
		 $keyin=0;
		foreach($id as $key=>$idd){
			$isubah=0;
			if($jam_lama[$key]!=$jam[$key]){
				$isubah=1;
			}else{
				if($status_hadir_lama[$key]!=$status_hadir[$key]){
					$isubah=1;
				}else{
					if($keterangan_lama[$key]!=$keterangan[$key]){
						$isubah=1;
					}
				}
			}
			
			if($idd>0 && $isubah==1 ){
			// update
					$upabsen['id']=$idd;
					$upabsen['jam']=$jam[$key];
					$upabsen['status_hadir']=$status_hadir[$key];
					$upabsen['keterangan']=$keterangan[$key];
					$this->crud->update($upabsen, 'data_umum_absensi');
			}else{
			// insert
				if($idd<=0){
					$rowabsen[$keyin]['kdo']=$kdo;
					$rowabsen[$keyin]['kode_data_umum']=$kode_data_umum[$key];
					$rowabsen[$keyin]['tanggal']=$tanggal;
					$rowabsen[$keyin]['jam']=$jam[$key];
					$rowabsen[$keyin]['status_hadir']=$status_hadir[$key];
					$rowabsen[$keyin]['keterangan']=$keterangan[$key];
					$keyin++;
				}
			}
		}
		
	 	if(count($rowabsen)>0 && is_array($rowabsen)){
			$this->crud->multi_create($rowabsen, 'data_umum_absensi');
		 }
    }

     function simpan_absen_barcode() {
		$kdo=get_ses("kdo");
		$kode_data_umum = strtoupper($_REQUEST['kode_barcode']);
		$tanggal = $_REQUEST['tanggal'];
		$detail= $this->crud->get_single_data('data_umum', "kdo='$kdo' and kode='$kode_data_umum'");
		if(isset($detail['kode'])){
			$detail_abs= $this->crud->get_single_data('data_umum_absensi', "kdo='$kdo' and kode_data_umum='$kode_data_umum' and tanggal='$tanggal'");
			
			if(!isset($detail_abs['id'])){
				$rows=array(
				"kdo"=>$kdo,
				"kode_data_umum"=>$kode_data_umum,
				"tanggal"=>$tanggal,
				"jam"=>date('G:i:s'),
				"status_hadir"=>'1'
				);
				$akun = $this->crud->create($rows, 'data_umum_absensi');
				echo "success";
			}else{
				echo 'exist';
			}
		}else{
		echo "failure";
		}
	}
	
	// start rekap_absen
    function rekap_absen() {
		$kdo=get_ses("kdo");
		$atrc="";
		$tgl_awal=($_REQUEST['tgl_awal']) ? $_REQUEST['tgl_awal'] : date('Y-m-d');
		$tgl_akhir=($_REQUEST['tgl_akhir']) ? $_REQUEST['tgl_akhir'] : date('Y-m-d');
		
		$atrc=" and a.tanggal between '$tgl_awal' and '$tgl_akhir'";
		$top_label=" Periode Tanggal $tgl_awal s/d $tgl_akhir";
		$data_full=$this->crud->get_data("data_umum k,data_umum_absensi a","k.hapus='0' and a.kdo='$kdo' and k.kdo=a.kdo and a.kode_data_umum=k.kode $atrc","a.*");
		$data=array();
		foreach($data_full as $datafull){
			$data[$datafull['kode_data_umum']][$datafull['tanggal']]=$datafull;
		}
		
		$data_data_umum=$this->crud->get_data("data_umum k","k.hapus='0' and k.kdo='$kdo' ","k.*");
		
 		$this->vars['data']=$data;
 		$this->vars['data_data_umum']=$data_data_umum;
 		$this->vars['tgl_awal']=$tgl_awal;
 		$this->vars['tgl_akhir']=$tgl_akhir;
		$this->vars['title']="Rekap Absensi data_umum";
		$this->loadView($this->page, $this->method, $this->vars,true);
    }
	
    function print_rekap() {
		$this->vars['isprint']=true;
		$this->rekap_absen();
		$this->render_print($this->vars,$this->page, 'tabel_rekap' );
		
	}

}

?>
