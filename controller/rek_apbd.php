<?php
class Rek_apbd extends Yaridhane {

    private $page;
    private $method;
    private $vars = array();

    function __construct($page, $method) {
        $this->page = $page;
        $this->method = $method;
        parent::__construct();
    }
	
	
	// start akun
    function __default() {
	
        $this->vars['title'] = 'Rekening APBD';
		
		// akun
        $flex_config = array(
            fields => array(
                array(display => 'Opsi', name => 'Option', width => '70', align => 'center'),
                array(display => 'Kode', name => 'u.kode', align => 'left', width => '60', sortable => true, filter => true), 
                array(display => 'Nama', name => 'u.nama', align => 'left', width => '200', sortable => true, filter => true), 
                array(display => 'Saldo Normal', name => 'u.saldo_normal', align => 'left', width => '100', sortable => true, filter => true), 
                ),
            url => '?act=rek_apbd&do=grid_akun',
            showpage => '10',
			isprint=>$this->vars['isprint']
        );
        $grid = $this->render_grid($flex_config,'tabel-akun');
        
        // kelompok
        $flex_config = array(
            fields => array(
                array(display => 'Opsi', name => 'Option', width => '70', align => 'center'),
                array(display => 'Kode', name => 'b.kode', align => 'left', width => '60', sortable => true, filter => true), 
                array(display => 'Nama', name => 'b.nama', align => 'left', width => '200', sortable => true, filter => true), 
                array(display => 'Saldo Normal', name => 'b.saldo_normal', align => 'left', width => '100', sortable => true, filter => true), 
                array(display => 'Akun', name => 'u.nama', align => 'left', width => '200', sortable => true, filter => true), 
                ),
            url => '?act=rek_apbd&do=grid_kelompok',
            showpage => '10',
			isprint=>$this->vars['isprint']
        );
        $grid .= $this->render_grid($flex_config,'tabel-kelompok');
        
        // jenis
        $flex_config = array(
            fields => array(
                array(display => 'Opsi', name => 'Option', width => '70', align => 'center'),
                array(display => 'Kode', name => 't.kode', align => 'left', width => '60', sortable => true, filter => true), 
                array(display => 'Nama', name => 't.nama', align => 'left', width => '200', sortable => true, filter => true), 
                array(display => 'Saldo Normal', name => 't.saldo_normal', align => 'left', width => '100', sortable => true, filter => true), 
                array(display => 'Akun', name => 'u.nama', align => 'left', width => '200', sortable => true, filter => true), 
                array(display => 'Kelompok', name => 'b.nama', align => 'left', width => '200', sortable => true, filter => true) 
                ),
            url => '?act=rek_apbd&do=grid_jenis',
            showpage => '10',
			isprint=>$this->vars['isprint']
        );
        $grid .= $this->render_grid($flex_config,'tabel-jenis');

        // Obyek
        $flex_config = array(
            fields => array(
                array(display => 'Opsi', name => 'Option', width => '70', align => 'center'),
                array(display => 'Kode', name => 's.kode', align => 'left', width => '80', sortable => true, filter => true), 
                array(display => 'Nama', name => 's.nama', align => 'left', width => '200', sortable => true, filter => true), 
                array(display => 'Saldo Normal', name => 's.saldo_normal', align => 'left', width => '100', sortable => true, filter => true), 
                array(display => 'Akun', name => 'u.nama', align => 'left', width => '200', sortable => true, filter => true), 
                array(display => 'Kelompok', name => 'b.nama', align => 'left', width => '200', sortable => true, filter => true), 
                array(display => 'Jenis', name => 't.nama', align => 'left', width => '200', sortable => true, filter => true) 
                ),
            url => '?act=rek_apbd&do=grid_obyek',
            showpage => '10',
            isprint=>$this->vars['isprint']
        );
        $grid .= $this->render_grid($flex_config,'tabel-obyek');
        
        // Rincian
        $flex_config = array(
            fields => array(
                array(display => 'Opsi', name => 'Option', width => '70', align => 'center'),
                array(display => 'Kode', name => 's.kode', align => 'left', width => '100', sortable => true, filter => true), 
                array(display => 'Nama', name => 's.nama', align => 'left', width => '200', sortable => true, filter => true), 
                array(display => 'Saldo Normal', name => 's.saldo_normal', align => 'left', width => '100', sortable => true, filter => true), 
                array(display => 'Akun', name => 'u.nama', align => 'left', width => '180', sortable => true, filter => true), 
                array(display => 'Kelompok', name => 'b.nama', align => 'left', width => '180', sortable => true, filter => true), 
                array(display => 'Jenis', name => 't.nama', align => 'left', width => '180', sortable => true, filter => true), 
                array(display => 'Obyek', name => 'o.nama', align => 'left', width => '200', sortable => true, filter => true) 
                ),
            url => '?act=rek_apbd&do=grid_rincian',
            showpage => '10',
            isprint=>$this->vars['isprint']
        );
        $grid .= $this->render_grid($flex_config,'tabel-rincian');
        
        $this->vars['grid'] = $grid;
        $this->loadView($this->page, $this->method, $this->vars,true);
    }

    function grid_akun() {
        $tbl = 'apbd_akun u'; // nama tabel
        $sel = '*'; // default * 
        $where = "1"; // jika where kosong isi dengan 1
        $order='u.kode';
        $direction='asc';
        $hasil = $this->crud->get_data_flexi($tbl, $where, $sel, $order,$direction);
		
        while ($row = mysql_fetch_assoc($hasil['exe'])) {
            $data['page'] = $hasil['page'];
            $data['total'] = $hasil['total'];
            $data['rows'][] = array(
                'id' => $row['id'],
                'cell' => array(
                    '<a href="?act=rek_apbd&do=form_akun&id=' . $row['id'] . '" onClick="return modaler(this);"  class="ficon-green fa fa-pencil-square-o fa-lg linkbox"  label="Ubah Data Akun"  title="Ubah Akun"> </a> 
					<a href="javascript:void(0)" onClick="del_data(\'?act=rek_apbd&do=hapus_akun&id=' . $row['id'] . '\',this,\'tabel-akun\')" class="ficon-red fa fa-trash-o fa-lg temp-delete"   title="Hapus Akun" ></a>
					'
					,
                    str_replace('.00', '',$row['kode']),
                    $row['nama'],
                    strtoupper($row['saldo_normal'])
                    )
            );
        }
     	  echo json_encode($data);
		  die();
        
    }

	 function form_akun() {
       	$id = $_REQUEST['id'];
		if ($id) {
        	$this->vars['detail'] = $this->crud->get_single_data('apbd_akun', "id='$id'");
		}
	    $this->vars['id'] = $id;
		$this->loadView($this->page, $this->method, $this->vars, true);
    }
	
	 function simpan_akun() {
 		$id=$_REQUEST['id'];
        if ($id) { // update
            $akun = $this->crud->update($rows, 'apbd_akun', true,"","id");
        } else { // create
         	$cek= $this->crud->get_single_data('apbd_akun', "kode='$_REQUEST[kode]'");
         	if(is_array($cek)){
         		echo "failed";
         		die();
         	}
		    $akun = $this->crud->create($rows, 'apbd_akun', true);
        }
    }

    function hapus_akun() {
        $id = $_REQUEST['id'];
		$this->crud->delete_data("apbd_akun","id='$id'");
	}
	
// kelompok
	    function grid_kelompok() {
        $tbl = 'apbd_kelompok b,apbd_akun u'; // nama tabel
        $sel = 'b.*,u.nama nama_akun'; // default * 
        $where = "u.id=b.id_apbd_akun"; // jika where kosong isi dengan 1
        $order='b.kode';
        $direction='asc';
        $hasil = $this->crud->get_data_flexi($tbl, $where, $sel, $order,$direction);
		
        while ($row = mysql_fetch_assoc($hasil['exe'])) {
            $data['page'] = $hasil['page'];
            $data['total'] = $hasil['total'];
            $data['rows'][] = array(
                'id' => $row['id'],
                'cell' => array(
                    '<a href="?act=rek_apbd&do=form_kelompok&id=' . $row['id'] . '" onClick="return modaler(this);"  class="ficon-green fa fa-pencil-square-o fa-lg linkbox"  label="Ubah Data Kelompok"  title="Ubah Kelompok"> </a> 
					<a href="javascript:void(0)" onClick="del_data(\'?act=rek_apbd&do=hapus_kelompok&id=' . $row['id'] . '\',this,\'tabel-kelompok\')" class="ficon-red fa fa-trash-o fa-lg temp-delete"   title="Hapus Kelompok" ></a>
					'
					,
                    $row['kode'],
                    $row['nama'],
                    strtoupper($row['saldo_normal']),
                    $row['nama_akun']
                    )
            );
        }
     	  echo json_encode($data);
		  die();
        
    }

	 function form_kelompok() {
       	$id = $_REQUEST['id'];
		if ($id) {
        	$this->vars['detail'] = $this->crud->get_single_data('apbd_kelompok b, apbd_akun u', "b.id='$id' and b.id_apbd_akun=u.id","b.*,u.kode kode_akun,u.nama nama_akun");
		}
	    $this->vars['id'] = $id;
		$this->loadView($this->page, $this->method, $this->vars, true);
    }
	
	 function simpan_kelompok() {
 		$id=$_REQUEST['id'];
 		$kode=$_REQUEST['kode_akun'].'.'.$_REQUEST['kode_kelompok'];
 		$rows=array('kode'=>$kode);
        if ($id) { // update
            $akun = $this->crud->update($rows, 'apbd_kelompok', true,"","id");
        } else { // create
         	$cek= $this->crud->get_single_data('apbd_kelompok', "kode='$kode'");
         	if(is_array($cek)){
         		echo "failed";
         		die();
         	}
		    $akun = $this->crud->create($rows, 'apbd_kelompok', true);
        }
    }

    function hapus_kelompok() {
        $id = $_REQUEST['id'];
		$this->crud->delete_data("apbd_kelompok","id='$id'");
	}
	

// jenis
	    function grid_jenis() {
        $tbl = 'apbd_jenis t, apbd_kelompok b,apbd_akun u'; // nama tabel
        $sel = 't.*,b.nama nama_kelompok,u.nama nama_akun'; // default * 
        $where = "t.id_apbd_kelompok=b.id and u.id=b.id_apbd_akun"; // jika where kosong isi dengan 1
        $order='t.kode';
        $direction='asc';
        $hasil = $this->crud->get_data_flexi($tbl, $where, $sel, $order,$direction);
		
        while ($row = mysql_fetch_assoc($hasil['exe'])) {
            $data['page'] = $hasil['page'];
            $data['total'] = $hasil['total'];
            $data['rows'][] = array(
                'id' => $row['id'],
                'cell' => array(
                    '<a href="?act=rek_apbd&do=form_jenis&id=' . $row['id'] . '" onClick="return modaler(this);"  class="ficon-green fa fa-pencil-square-o fa-lg linkbox"  label="Ubah Data Jenis"  title="Ubah Jenis"> </a> 
					<a href="javascript:void(0)" onClick="del_data(\'?act=rek_apbd&do=hapus_jenis&id=' . $row['id'] . '\',this,\'tabel-jenis\')" class="ficon-red fa fa-trash-o fa-lg temp-delete"   title="Hapus Jenis" ></a>
					'
					,
                    $row['kode'],
                    $row['nama'],
                    strtoupper($row['saldo_normal']),
                    $row['nama_akun'],
                    $row['nama_kelompok']
                    )
            );
        }
     	  echo json_encode($data);
		  die();
        
    }

	 function form_jenis() {
       	$id = $_REQUEST['id'];
		if ($id) {
        	$this->vars['detail'] = $this->crud->get_single_data('apbd_jenis t, apbd_kelompok b, apbd_akun u', "t.id='$id' and t.id_apbd_kelompok=b.id and b.id_apbd_akun=u.id","t.*,u.kode kode_akun,u.nama nama_akun,b.kode kode_kelompok,b.nama nama_kelompok");
		}
	    $this->vars['id'] = $id;
		$this->loadView($this->page, $this->method, $this->vars, true);
    }
	
	 function simpan_jenis() {
 		$id=$_REQUEST['id'];
 		$kode=$_REQUEST['kode_kelompok'].'.'.$_REQUEST['kode_jenis'];
 		$rows=array('kode'=>$kode);
        if ($id) { // update
            $akun = $this->crud->update($rows, 'apbd_jenis', true,"","id");
        } else { // create
         	$cek= $this->crud->get_single_data('apbd_jenis', "kode='$kode'");
         	if(is_array($cek)){
         		echo "failed";
         		die();
         	}
		    $akun = $this->crud->create($rows, 'apbd_jenis', true);
        }
    }
    function hapus_jenis() {
        $id = $_REQUEST['id'];
		$this->crud->delete_data("apbd_jenis","id='$id'");
	}
	

// Obyek
	    function grid_obyek() {
        $tbl = 'apbd_obyek s, apbd_jenis t, apbd_kelompok b,apbd_akun u'; // nama tabel
        $sel = 's.*,t.nama nama_jenis,b.nama nama_kelompok,u.nama nama_akun'; // default * 
        $where = "s.id_apbd_jenis=t.id and t.id_apbd_kelompok=b.id and u.id=b.id_apbd_akun"; // jika where kosong isi dengan 1
        $order='s.kode';
        $direction='asc';
        $hasil = $this->crud->get_data_flexi($tbl, $where, $sel, $order,$direction);
		
        while ($row = mysql_fetch_assoc($hasil['exe'])) {
            $data['page'] = $hasil['page'];
            $data['total'] = $hasil['total'];
            $data['rows'][] = array(
                'id' => $row['id'],
                'cell' => array(
                    '<a href="?act=rek_apbd&do=form_obyek&id=' . $row['id'] . '" onClick="return modaler(this);"  class="ficon-green fa fa-pencil-square-o fa-lg linkbox"  label="Ubah Data Obyek"  title="Ubah Obyek"> </a> 
					<a href="javascript:void(0)" onClick="del_data(\'?act=rek_apbd&do=hapus_obyek&id=' . $row['id'] . '\',this,\'tabel-obyek\')" class="ficon-red fa fa-trash-o fa-lg temp-delete"   title="Hapus Obyek" ></a>
					'
					,
                    $row['kode'],
                    $row['nama'],
                    strtoupper($row['saldo_normal']),
                    $row['nama_akun'],
                    $row['nama_kelompok'],
                    $row['nama_jenis']
                    )
            );
        }
     	  echo json_encode($data);
		  die();
        
    }

	 function form_obyek() {
       	$id = $_REQUEST['id'];
		if ($id) {
        	$this->vars['detail'] = $this->crud->get_single_data('apbd_obyek s, apbd_jenis t, apbd_kelompok b, apbd_akun u', "s.id='$id' and s.id_apbd_jenis=t.id and t.id_apbd_kelompok=b.id and b.id_apbd_akun=u.id","s.*,u.kode kode_akun,u.nama nama_akun,b.kode kode_kelompok,b.nama nama_kelompok,t.kode kode_jenis,t.nama nama_jenis");
		}
	    $this->vars['id'] = $id;
		$this->loadView($this->page, $this->method, $this->vars, true);
    }
	
	 function simpan_obyek() {
 		$id=$_REQUEST['id'];
 		$kode=$_REQUEST['kode_jenis'].'.'.$_REQUEST['kode_obyek'];
 		$rows=array('kode'=>$kode);
        if ($id) { // update
            $akun = $this->crud->update($rows, 'apbd_obyek', true,"","id");
        } else { // create
         	$cek= $this->crud->get_single_data('apbd_obyek', "kode='$kode'");
         	if(is_array($cek)){
         		echo "failed";
         		die();
         	}
		    $akun = $this->crud->create($rows, 'apbd_obyek', true);
        }
    }

    function hapus_obyek() {
        $id = $_REQUEST['id'];
		$this->crud->delete_data("apbd_obyek","id='$id'");
	}
	
// Rincian
        function grid_rincian() {
        $tbl = 'apbd_rincian s, apbd_obyek o, apbd_jenis t, apbd_kelompok b,apbd_akun u'; // nama tabel
        $sel = 's.*,o.nama nama_obyek,t.nama nama_jenis,b.nama nama_kelompok,u.nama nama_akun'; // default * 
        $where = "s.id_apbd_obyek=o.id and o.id_apbd_jenis=t.id and t.id_apbd_kelompok=b.id and u.id=b.id_apbd_akun"; // jika where kosong isi dengan 1
        $order='s.kode';
        $direction='asc';
        $hasil = $this->crud->get_data_flexi($tbl, $where, $sel, $order,$direction);
        
        while ($row = mysql_fetch_assoc($hasil['exe'])) {
            $data['page'] = $hasil['page'];
            $data['total'] = $hasil['total'];
            $data['rows'][] = array(
                'id' => $row['id'],
                'cell' => array(
                    '<a href="?act=rek_apbd&do=form_rincian&id=' . $row['id'] . '" onClick="return modaler(this);"  class="ficon-green fa fa-pencil-square-o fa-lg linkbox"  label="Ubah Data Rincian"  title="Ubah Rincian"> </a> 
                    <a href="javascript:void(0)" onClick="del_data(\'?act=rek_apbd&do=hapus_rincian&id=' . $row['id'] . '\',this,\'tabel-rincian\')" class="ficon-red fa fa-trash-o fa-lg temp-delete"   title="Hapus Rincian" ></a>
                    '
                    ,
                    $row['kode'],
                    $row['nama'],
                    strtoupper($row['saldo_normal']),
                    $row['nama_akun'],
                    $row['nama_kelompok'],
                    $row['nama_jenis'],
                    $row['nama_obyek']
                    )
            );
        }
          echo json_encode($data);
          die();
        
    }

     function form_rincian() {
        $id = $_REQUEST['id'];
        if ($id) {
            $this->vars['detail'] = $this->crud->get_single_data('apbd_rincian s, apbd_obyek o, apbd_jenis t, apbd_kelompok b, apbd_akun u', "s.id='$id' and s.id_apbd_obyek=o.id and o.id_apbd_jenis=t.id and t.id_apbd_kelompok=b.id and b.id_apbd_akun=u.id","s.*,u.kode kode_akun,u.nama nama_akun,b.kode kode_kelompok,b.nama nama_kelompok,t.kode kode_jenis,t.nama nama_jenis,o.kode kode_obyek,o.nama nama_obyek");
        }
        $this->vars['id'] = $id;
        $this->loadView($this->page, $this->method, $this->vars, true);
    }
    
     function simpan_rincian() {
        $id=$_REQUEST['id'];
        $kode=$_REQUEST['kode_obyek'].'.'.$_REQUEST['kode_rincian'];
        $rows=array('kode'=>$kode);
        if ($id) { // update
            $akun = $this->crud->update($rows, 'apbd_rincian', true,"","id");
        } else { // create
            $cek= $this->crud->get_single_data('apbd_rincian', "kode='$kode'");
            if(is_array($cek)){
                echo "failed";
                die();
            }
            $akun = $this->crud->create($rows, 'apbd_rincian', true);
        }
    }

    function hapus_rincian() {
        $id = $_REQUEST['id'];
        $this->crud->delete_data("apbd_rincian","id='$id'");
    }
    


}

?>
