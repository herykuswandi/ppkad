<?php
class Dashboard extends Yaridhane {

    private $page;
    private $method;
    private $vars = array();

    function __construct($page, $method) {
        $this->page = $page;
        $this->method = $method;
        parent::__construct();
    }
	
	// start Pembelian
    function __default() {
		
        $this->loadView($this->page, $this->method, $this->vars,true);
    }

}

?>
