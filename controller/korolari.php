<?php
class Korolari extends Yaridhane {

    private $page;
    private $method;
    private $vars = array();

    function __construct($page, $method) {
        $this->page = $page;
        $this->method = $method;
        parent::__construct();
    }
	
	
	// start tim_anggaran
    function __default() {
	
        $this->vars['title'] = 'Data Korolari';
        $flex_config = array(
            fields => array(
                array(display => 'Opsi', name => 'Option', width => '70', align => 'center'),
                array(display => 'Kode Rek.Korolari', name => 'r.kode', align => 'left', width => '120', sortable => true, filter => true), 
                array(display => 'Uraian Rek.Korolari', name => 'r.nama', align => 'left', width => '180', sortable => true, filter => true),
                array(display => 'Kode Rek.Debet', name => 'r.kode', align => 'left', width => '120', sortable => true, filter => true), 
                array(display => 'Uraian Rek.Debet', name => 'r.nama', align => 'left', width => '180', sortable => true, filter => true),
                array(display => 'Kode Rek.Kredit', name => 'r.kode', align => 'left', width => '120', sortable => true, filter => true), 
                array(display => 'Uraian Rek.Kredit', name => 'r.nama', align => 'left', width => '180', sortable => true, filter => true)
                ),
            url => '?act=korolari&do=grid',
            showpage => '10',
			isprint=>$this->vars['isprint']
        );
        $grid = $this->render_grid($flex_config);
        $this->vars['grid'] = $grid;
        $this->loadView($this->page, $this->method, $this->vars,true);
    }

    function grid() {
        $tbl = 'apbd_rincian r,korolari k left join apbd_rincian d on d.id=k.id_rek_debet left join apbd_rincian t on t.id=k.id_rek_kredit'; // nama tabel
        $sel = 'k.*,
                r.nama nama_korolari,r.kode kode_korolari,
                d.nama nama_debet,d.kode kode_debet,
                t.nama nama_kredit,t.kode kode_kredit'; // default * 
        $where = "k.id_rek_kor=r.id"; // jika where kosong isi dengan 1
        $order='r.kode';
        $direction='asc';
        $hasil = $this->crud->get_data_flexi($tbl, $where, $sel, $order,$direction);
		while ($row = mysql_fetch_assoc($hasil['exe'])) {
            $data['page'] = $hasil['page'];
            $data['total'] = $hasil['total'];
            $data['rows'][] = array(
                'id' => $row['id'],
                'cell' => array(
                    '<a href="?act=korolari&do=form&id=' . $row['id'] . '" onClick="return modaler(this);"  class="ficon-green fa fa-pencil-square-o fa-lg linkbox"  label="Ubah Korolari"  title="Ubah Korolari"> </a> 
					<a href="javascript:void(0)" onClick="del_data(\'?act=korolari&do=hapus&id=' . $row['id'] . '\',this)" class="ficon-red fa fa-trash-o fa-lg "   title="Hapus Korolari" ></a>
					'
					,
                    $row['kode_korolari'],
                    $row['nama_korolari'],
                    $row['kode_debet'],
                    $row['nama_debet'],
                    $row['kode_kredit'],
                    $row['nama_kredit']
                    )
            );
        }

    	if($this->vars['isprint']){
			$this->vars['data']=$data;
		}else{
      	  echo json_encode($data);
		  die();
      	}    
        
    }
	 function form() {
       	$id = $_REQUEST['id'];
		if ($id) {
            $tbl = 'apbd_rincian r,korolari k left join apbd_rincian d on d.id=k.id_rek_debet left join apbd_rincian t on t.id=k.id_rek_kredit'; // nama tabel
            $sel = 'k.*,
                    r.nama nama_korolari,r.kode kode_korolari,
                    d.nama nama_debet,d.kode kode_debet,
                    t.nama nama_kredit,t.kode kode_kredit'; // default * 
            $where = "k.id_rek_kor=r.id and k.id='$id'"; // jika where kosong isi dengan 1
        	$this->vars['detail'] = $this->crud->get_single_data($tbl, $where,$sel);
		}
	    $this->vars['id'] = $id;
		$this->loadView($this->page, $this->method, $this->vars, true);
    }
	
	 function simpan() {
 		$id=$_REQUEST['id'];
       
        if ($id) { // update
            $apbd_akun = $this->crud->update($rows, 'korolari', true,"","id");
        } else { // create
         	$cek= $this->crud->get_single_data('korolari', "(id_rek_kor='$_REQUEST[id_rek_kor]' and id_rek_debet='$_REQUEST[id_rek_debet]' and id_rek_kredit='$_REQUEST[id_rek_kredit]') ");
         	if(is_array($cek)){
         		echo "failed";
         		die();
         	}
		    $apbd_akun = $this->crud->create($rows, 'korolari', true);
        }
    }

    function hapus() {
        $id = $_REQUEST['id'];
		$this->crud->delete_data("korolari","id='$id'");
	}
	
	
}

?>
