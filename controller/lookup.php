<?php
class lookup extends Yaridhane {

    private $page;
    private $method;
    private $vars = array();

    function __construct($page, $method) {
        $this->page = $page;
        $this->method = $method;
        parent::__construct();
    }

//  Organisasi
	// start Urusan
	function grid_get_urusan() {
		$elid=$_REQUEST[elid];
        $tbl = 'urusan';
        $where = "1";
        $sel = '*';

        $hasil = $this->crud->get_data_flexi($tbl, $where, $sel, 'kode', 'asc');
        while ($row = mysql_fetch_assoc($hasil['exe'])) {
            $data['page'] = $hasil['page'];
            $data['total'] = $hasil['total'];
                $data['rows'][] = array(
                    'id' => $row['kode'],
                    'cell' => array(
                        $row['kode'],
                        '<a href="javascript:void(0)" onClick="$(\'#'.$elid.'\').hselect(\''.$row['kode'].'\');"  class="ficon-blueold fa fa-download fa-lg "  title="Pilih Data Ini"> </a> ',
                        $row['kode'],
                        $row['nama']
                    )
                );
        }
        echo json_encode($data);
        die();
    }

    function get_urusan() {
    	$elid=$_REQUEST[elid];
        $flex_config = array(
            fields => array(
                array(hide => 'true', name => 'kode'), 
				array(display => 'Opsi', name => 'Pilih Data', hide => $show, width => '50', align => 'center'),
                array(display => 'Kode', name => 'kode', width => '150', align => 'left', sortable => true, filter => true),
                array(display => 'Nama', name => 'nama',  width => '200',align => 'left', sortable => true, filter => true)
            ),
            url => '?act=lookup&do=grid_get_urusan',
            showpage => '10',
			elid=>$elid,
			add_data=>false,
			dblclick=>'function(e){
				var nad=$(e).find("td").first().text();	
				$("#'.$elid.'").hselect(nad);			
			}'
        );
        $grid = $this->render_grid_lookup($flex_config);
    }
	
	function get_urusan_detail(){
        $term = $_REQUEST['term'];
      	
        $tbl = 'urusan';
        $where = "(kode like '%$term%' or nama  like '%$term%') ";
        $sel = "kode,nama,id,kode as value, concat(kode,' / ',nama) as label";
        $hasil = $this->crud->get_data($tbl, $where, $sel);
        echo json_encode($hasil);
        die();

	}
	// end Urusan

    // start bidang
    function grid_get_bidang() {
        $elid=$_REQUEST[elid];
        $urusan=$_REQUEST['id_urusan'];
        $tbl = 'bidang';
        $where = ($urusan>0) ? "id_urusan='$urusan'" : "1";
        $sel = '*';

        $hasil = $this->crud->get_data_flexi($tbl, $where, $sel, 'kode', 'asc');
        while ($row = mysql_fetch_assoc($hasil['exe'])) {
            $data['page'] = $hasil['page'];
            $data['total'] = $hasil['total'];
                $data['rows'][] = array(
                    'id' => $row['kode'],
                    'cell' => array(
                        $row['kode'],
                        '<a href="javascript:void(0)" onClick="$(\'#'.$elid.'\').hselect(\''.$row['kode'].'\');"  class="ficon-blueold fa fa-download fa-lg "  title="Pilih Data Ini"> </a> ',
                        $row['kode'],
                        $row['nama']
                    )
                );
        }
        echo json_encode($data);
        die();
    }

    function get_bidang() {
        $elid=$_REQUEST[elid];
        $flex_config = array(
            fields => array(
                array(hide => 'true', name => 'kode'), 
                array(display => 'Opsi', name => 'Pilih Data', hide => $show, width => '50', align => 'center'),
                array(display => 'Kode', name => 'kode', width => '150', align => 'left', sortable => true, filter => true),
                array(display => 'Nama', name => 'nama',  width => '200',align => 'left', sortable => true, filter => true)
            ),
            url => '?act=lookup&do=grid_get_bidang&id_urusan='.$_REQUEST['id_urusan'],
            showpage => '10',
            elid=>$elid,
            add_data=>false,
            dblclick=>'function(e){
                var nad=$(e).find("td").first().text(); 
                $("#'.$elid.'").hselect(nad);           
            }'
        );
        $grid = $this->render_grid_lookup($flex_config);
    }
    
    function get_bidang_detail(){
        $term = $_REQUEST['term'];
        $urusan = $_REQUEST['id_urusan'];
        
        $tbl = 'bidang';
        if($urusan>0){
        $where = "id_urusan='$urusan' and (kode like '%$term%' or nama  like '%$term%') ";
        }else{
        $where = " (kode like '%$term%' or nama  like '%$term%') ";
        }
        $sel = "kode,nama,id,kode as value, concat(kode,' / ',nama) as label";
        $hasil = $this->crud->get_data($tbl, $where, $sel);
        echo json_encode($hasil);
        die();

    }
    
    // end Urusan
    

    // start unit
    function grid_get_unit() {
        $elid=$_REQUEST[elid];
        $bidang=$_REQUEST['id_bidang'];
        $tbl = 'unit';
        $where = ($bidang>0) ? "id_bidang='$bidang'" : "1";
        $sel = '*';

        $hasil = $this->crud->get_data_flexi($tbl, $where, $sel, 'kode', 'asc');
        while ($row = mysql_fetch_assoc($hasil['exe'])) {
            $data['page'] = $hasil['page'];
            $data['total'] = $hasil['total'];
                $data['rows'][] = array(
                    'id' => $row['kode'],
                    'cell' => array(
                        $row['kode'],
                        '<a href="javascript:void(0)" onClick="$(\'#'.$elid.'\').hselect(\''.$row['kode'].'\');"  class="ficon-blueold fa fa-download fa-lg "  title="Pilih Data Ini"> </a> ',
                        $row['kode'],
                        $row['nama']
                    )
                );
        }
        echo json_encode($data);
        die();
    }

    function get_unit() {
        $elid=$_REQUEST[elid];
        $flex_config = array(
            fields => array(
                array(hide => 'true', name => 'kode'), 
                array(display => 'Opsi', name => 'Pilih Data', hide => $show, width => '50', align => 'center'),
                array(display => 'Kode', name => 'kode', width => '150', align => 'left', sortable => true, filter => true),
                array(display => 'Nama', name => 'nama',  width => '200',align => 'left', sortable => true, filter => true)
            ),
            url => '?act=lookup&do=grid_get_unit&id_bidang='.$_REQUEST['id_bidang'],
            showpage => '10',
            elid=>$elid,
            add_data=>false,
            dblclick=>'function(e){
                var nad=$(e).find("td").first().text(); 
                $("#'.$elid.'").hselect(nad);           
            }'
        );
        $grid = $this->render_grid_lookup($flex_config);
    }
    
    function get_unit_detail(){
        $term = $_REQUEST['term'];
        $bidang = $_REQUEST['id_bidang'];
        
        $tbl = 'unit';
        if($bidang>0){
        $where = "id_bidang='$bidang' and (kode like '%$term%' or nama  like '%$term%') ";
        }else{
        $where = " (kode like '%$term%' or nama  like '%$term%') ";
        }
        $sel = "kode,nama,id,kode as value, concat(kode,' / ',nama) as label";
        $hasil = $this->crud->get_data($tbl, $where, $sel);
        echo json_encode($hasil);
        die();

    }
    
    // end Unit
    // start sub unit
    function grid_get_sub_unit() {
        $elid=$_REQUEST[elid];
        $unit=$_REQUEST['id_unit'];
        $tbl = 'sub_unit';
        $where = ($unit>0) ? "id_unit='$unit'" : "1";
        $sel = '*';

        $hasil = $this->crud->get_data_flexi($tbl, $where, $sel, 'kode', 'asc');
        while ($row = mysql_fetch_assoc($hasil['exe'])) {
            $data['page'] = $hasil['page'];
            $data['total'] = $hasil['total'];
                $data['rows'][] = array(
                    'id' => $row['kode'],
                    'cell' => array(
                        $row['kode'],
                        '<a href="javascript:void(0)" onClick="$(\'#'.$elid.'\').hselect(\''.$row['kode'].'\');"  class="ficon-blueold fa fa-download fa-lg "  title="Pilih Data Ini"> </a> ',
                        $row['kode'],
                        $row['nama']
                    )
                );
        }
        echo json_encode($data);
        die();
    }

    function get_sub_unit() {
        $elid=$_REQUEST[elid];
        $flex_config = array(
            fields => array(
                array(hide => 'true', name => 'kode'), 
                array(display => 'Opsi', name => 'Pilih Data', hide => $show, width => '50', align => 'center'),
                array(display => 'Kode', name => 'kode', width => '150', align => 'left', sortable => true, filter => true),
                array(display => 'Nama', name => 'nama',  width => '200',align => 'left', sortable => true, filter => true)
            ),
            url => '?act=lookup&do=grid_get_sub_unit&id_unit='.$_REQUEST['id_unit'],
            showpage => '10',
            elid=>$elid,
            add_data=>false,
            dblclick=>'function(e){
                var nad=$(e).find("td").first().text(); 
                $("#'.$elid.'").hselect(nad);           
            }'
        );
        $grid = $this->render_grid_lookup($flex_config);
    }
    
    function get_sub_unit_detail(){
        $term = $_REQUEST['term'];
        $unit = $_REQUEST['id_unit'];
        
        $tbl = 'sub_unit';
        if($unit>0){
        $where = "id_unit='$unit' and (kode like '%$term%' or nama  like '%$term%') ";
        }else{
        $where = " (kode like '%$term%' or nama  like '%$term%') ";
        }
        $sel = "kode,nama,id,kode as value, concat(kode,' / ',nama) as label";
        $hasil = $this->crud->get_data($tbl, $where, $sel);
        echo json_encode($hasil);
        die();

    }
    
    // end sub Unit
	
//  program kegiatan
    // start program
    function grid_get_program() {
        $elid=$_REQUEST[elid];
        $bidang=$_REQUEST['id_bidang'];
        $tbl = 'program';
        $where = ($bidang>0) ? "id_bidang='$bidang'" : "1";
        $sel = '*';

        $hasil = $this->crud->get_data_flexi($tbl, $where, $sel, 'kode', 'asc');
        while ($row = mysql_fetch_assoc($hasil['exe'])) {
            $data['page'] = $hasil['page'];
            $data['total'] = $hasil['total'];
                $data['rows'][] = array(
                    'id' => $row['kode'],
                    'cell' => array(
                        $row['kode'],
                        '<a href="javascript:void(0)" onClick="$(\'#'.$elid.'\').hselect(\''.$row['kode'].'\');"  class="ficon-blueold fa fa-download fa-lg "  title="Pilih Data Ini"> </a> ',
                        $row['kode'],
                        $row['nama']
                    )
                );
        }
        echo json_encode($data);
        die();
    }

    function get_program() {
        $elid=$_REQUEST[elid];
        $flex_config = array(
            fields => array(
                array(hide => 'true', name => 'kode'), 
                array(display => 'Opsi', name => 'Pilih Data', hide => $show, width => '50', align => 'center'),
                array(display => 'Kode', name => 'kode', width => '150', align => 'left', sortable => true, filter => true),
                array(display => 'Nama', name => 'nama',  width => '200',align => 'left', sortable => true, filter => true)
            ),
            url => '?act=lookup&do=grid_get_program&id_bidang='.$_REQUEST['id_bidang'],
            showpage => '10',
            elid=>$elid,
            add_data=>false,
            dblclick=>'function(e){
                var nad=$(e).find("td").first().text(); 
                $("#'.$elid.'").hselect(nad);           
            }'
        );
        $grid = $this->render_grid_lookup($flex_config);
    }
    
    function get_program_detail(){
        $term = $_REQUEST['term'];
        $bidang = $_REQUEST['id_bidang'];
        
        $tbl = 'program';
        if($bidang>0){
        $where = "id_bidang='$bidang' and (kode like '%$term%' or nama  like '%$term%') ";
        }else{
        $where = " (kode like '%$term%' or nama  like '%$term%') ";
        }
        $sel = "kode,nama,id,kode as value, concat(kode,' / ',nama) as label";
        $hasil = $this->crud->get_data($tbl, $where, $sel);
        echo json_encode($hasil);
        die();

    }
    
    // end program

    //  Rekening APBD
    // start Akun
    function grid_get_apbd_akun() {
        $elid=$_REQUEST[elid];
        $tbl = 'apbd_akun';
        $where = "1";
        $sel = '*';

        $hasil = $this->crud->get_data_flexi($tbl, $where, $sel, 'kode', 'asc');
        while ($row = mysql_fetch_assoc($hasil['exe'])) {
            $data['page'] = $hasil['page'];
            $data['total'] = $hasil['total'];

            $data['rows'][] = array(
                'id' => $row['kode'],
                'cell' => array(
                    $row['kode'],
                    '<a href="javascript:void(0)" onClick="$(\'#'.$elid.'\').hselect(\''.$row['kode'].'\');"  class="ficon-blueold fa fa-download fa-lg "  title="Pilih Data Ini"> </a> ',
                    $row['kode'],
                     $row['nama'],
                )
            );
        }
        echo json_encode($data);
        die();
    }

    function get_apbd_akun() {
        $elid=$_REQUEST[elid];
        $flex_config = array(
            fields => array(
                array(hide => 'true', name => 'kode'), 
                array(display => 'Opsi', name => 'Pilih Data', hide => $show, width => '50', align => 'center'),
                array(display => 'Kode', name => 'kode', width => '150', align => 'left', sortable => true, filter => true),
                array(display => 'Nama', name => 'nama',  width => '200',align => 'left', sortable => true, filter => true)
            ),
            url => '?act=lookup&do=grid_get_apbd_akun',
            showpage => '10',
            elid=>$elid,
            add_data=>false,
            dblclick=>'function(e){
                var nad=$(e).find("td").first().text(); 
                $("#'.$elid.'").hselect(nad);           
            }'
        );
        $grid = $this->render_grid_lookup($flex_config);
    }
    
    function get_apbd_akun_detail(){
        $term = $_REQUEST['term'];
        
        $tbl = 'apbd_akun';
        $where = "(kode like '%$term%' or nama  like '%$term%') ";
        $sel = "kode,nama,id,kode as value, concat(kode,' / ',nama) as label";
        $hasil = $this->crud->get_data($tbl, $where, $sel);
        echo json_encode($hasil);
        die();

    }
    // end Akun

    // start apbd_kelompok
    function grid_get_apbd_kelompok() {
        $elid=$_REQUEST[elid];
        $apbd_akun=$_REQUEST['id_apbd_akun'];
        $tbl = 'apbd_kelompok';
        $where = ($apbd_akun>0) ? "id_apbd_akun='$apbd_akun'" : "1";
        $sel = '*';

        $hasil = $this->crud->get_data_flexi($tbl, $where, $sel, 'kode', 'asc');
        while ($row = mysql_fetch_assoc($hasil['exe'])) {
            $data['page'] = $hasil['page'];
            $data['total'] = $hasil['total'];
                $data['rows'][] = array(
                    'id' => $row['kode'],
                    'cell' => array(
                        $row['kode'],
                        '<a href="javascript:void(0)" onClick="$(\'#'.$elid.'\').hselect(\''.$row['kode'].'\');"  class="ficon-blueold fa fa-download fa-lg "  title="Pilih Data Ini"> </a> ',
                        $row['kode'],
                        $row['nama']
                    )
                );
        }
        echo json_encode($data);
        die();
    }

    function get_apbd_kelompok() {
        $elid=$_REQUEST[elid];
        $flex_config = array(
            fields => array(
                array(hide => 'true', name => 'kode'), 
                array(display => 'Opsi', name => 'Pilih Data', hide => $show, width => '50', align => 'center'),
                array(display => 'Kode', name => 'kode', width => '150', align => 'left', sortable => true, filter => true),
                array(display => 'Nama', name => 'nama',  width => '200',align => 'left', sortable => true, filter => true)
            ),
            url => '?act=lookup&do=grid_get_apbd_kelompok&id_apbd_akun='.$_REQUEST['id_apbd_akun'],
            showpage => '10',
            elid=>$elid,
            add_data=>false,
            dblclick=>'function(e){
                var nad=$(e).find("td").first().text(); 
                $("#'.$elid.'").hselect(nad);           
            }'
        );
        $grid = $this->render_grid_lookup($flex_config);
    }
    
    function get_apbd_kelompok_detail(){
        $term = $_REQUEST['term'];
        $apbd_akun = $_REQUEST['id_apbd_akun'];
        
        $tbl = 'apbd_kelompok';
        if($apbd_akun>0){
        $where = "id_apbd_akun='$apbd_akun' and (kode like '%$term%' or nama  like '%$term%') ";
        }else{
        $where = " (kode like '%$term%' or nama  like '%$term%') ";
        }
        $sel = "kode,nama,id,kode as value, concat(kode,' / ',nama) as label";
        $hasil = $this->crud->get_data($tbl, $where, $sel);
        echo json_encode($hasil);
        die();

    }
    
    // end Akun
    

    // start apbd_jenis
    function grid_get_apbd_jenis() {
        $elid=$_REQUEST[elid];
        $apbd_kelompok=$_REQUEST['id_apbd_kelompok'];
        $tbl = 'apbd_jenis';
        $where = ($apbd_kelompok>0) ? "id_apbd_kelompok='$apbd_kelompok'" : "1";
        $sel = '*';

        $hasil = $this->crud->get_data_flexi($tbl, $where, $sel, 'kode', 'asc');
        while ($row = mysql_fetch_assoc($hasil['exe'])) {
            $data['page'] = $hasil['page'];
            $data['total'] = $hasil['total'];
                $data['rows'][] = array(
                    'id' => $row['kode'],
                    'cell' => array(
                        $row['kode'],
                        '<a href="javascript:void(0)" onClick="$(\'#'.$elid.'\').hselect(\''.$row['kode'].'\');"  class="ficon-blueold fa fa-download fa-lg "  title="Pilih Data Ini"> </a> ',
                        $row['kode'],
                        $row['nama']
                    )
                );
        }
        echo json_encode($data);
        die();
    }

    function get_apbd_jenis() {
        $elid=$_REQUEST[elid];
        $flex_config = array(
            fields => array(
                array(hide => 'true', name => 'kode'), 
                array(display => 'Opsi', name => 'Pilih Data', hide => $show, width => '50', align => 'center'),
                array(display => 'Kode', name => 'kode', width => '150', align => 'left', sortable => true, filter => true),
                array(display => 'Nama', name => 'nama',  width => '200',align => 'left', sortable => true, filter => true)
            ),
            url => '?act=lookup&do=grid_get_apbd_jenis&id_apbd_kelompok='.$_REQUEST['id_apbd_kelompok'],
            showpage => '10',
            elid=>$elid,
            add_data=>false,
            dblclick=>'function(e){
                var nad=$(e).find("td").first().text(); 
                $("#'.$elid.'").hselect(nad);           
            }'
        );
        $grid = $this->render_grid_lookup($flex_config);
    }
    
    function get_apbd_jenis_detail(){
        $term = $_REQUEST['term'];
        $apbd_kelompok = $_REQUEST['id_apbd_kelompok'];
        
        $tbl = 'apbd_jenis';
        if($apbd_kelompok>0){
        $where = "id_apbd_kelompok='$apbd_kelompok' and (kode like '%$term%' or nama  like '%$term%') ";
        }else{
        $where = " (kode like '%$term%' or nama  like '%$term%') ";
        }
        $sel = "kode,nama,id,kode as value, concat(kode,' / ',nama) as label";
        $hasil = $this->crud->get_data($tbl, $where, $sel);
        echo json_encode($hasil);
        die();

    }
    
    // end Jenis
    // start apbd_obyek
    function grid_get_apbd_obyek() {
        $elid=$_REQUEST[elid];
        $apbd_jenis=$_REQUEST['id_apbd_jenis'];
        $tbl = 'apbd_obyek';
        $where = ($apbd_jenis>0) ? "id_apbd_jenis='$apbd_jenis'" : "1";
        $sel = '*';

        $hasil = $this->crud->get_data_flexi($tbl, $where, $sel, 'kode', 'asc');
        while ($row = mysql_fetch_assoc($hasil['exe'])) {
            $data['page'] = $hasil['page'];
            $data['total'] = $hasil['total'];
                $data['rows'][] = array(
                    'id' => $row['kode'],
                    'cell' => array(
                        $row['kode'],
                        '<a href="javascript:void(0)" onClick="$(\'#'.$elid.'\').hselect(\''.$row['kode'].'\');"  class="ficon-blueold fa fa-download fa-lg "  title="Pilih Data Ini"> </a> ',
                        $row['kode'],
                        $row['nama']
                    )
                );
        }
        echo json_encode($data);
        die();
    }

    function get_apbd_obyek() {
        $elid=$_REQUEST[elid];
        $flex_config = array(
            fields => array(
                array(hide => 'true', name => 'kode'), 
                array(display => 'Opsi', name => 'Pilih Data', hide => $show, width => '50', align => 'center'),
                array(display => 'Kode', name => 'kode', width => '150', align => 'left', sortable => true, filter => true),
                array(display => 'Nama', name => 'nama',  width => '200',align => 'left', sortable => true, filter => true)
            ),
            url => '?act=lookup&do=grid_get_apbd_obyek&id_apbd_jenis='.$_REQUEST['id_apbd_jenis'],
            showpage => '10',
            elid=>$elid,
            add_data=>false,
            dblclick=>'function(e){
                var nad=$(e).find("td").first().text(); 
                $("#'.$elid.'").hselect(nad);           
            }'
        );
        $grid = $this->render_grid_lookup($flex_config);
    }
    
    function get_apbd_obyek_detail(){
        $term = $_REQUEST['term'];
        $apbd_jenis = $_REQUEST['id_apbd_jenis'];
        
        $tbl = 'apbd_obyek';
        if($apbd_jenis>0){
        $where = "id_apbd_jenis='$apbd_jenis' and (kode like '%$term%' or nama  like '%$term%') ";
        }else{
        $where = " (kode like '%$term%' or nama  like '%$term%') ";
        }
        $sel = "kode,nama,id,kode as value, concat(kode,' / ',nama) as label";
        $hasil = $this->crud->get_data($tbl, $where, $sel);
        echo json_encode($hasil);
        die();

    }
    
    // end sub Jenis
    // start apbd_rincian
    function grid_get_apbd_rincian() {
        $elid=$_REQUEST[elid];
        $apbd_obyek=$_REQUEST['id_apbd_obyek'];
        $tbl = 'apbd_rincian';
        $where = ($apbd_obyek>0) ? "id_apbd_obyek='$apbd_obyek'" : "1";
        $sel = '*';

        $hasil = $this->crud->get_data_flexi($tbl, $where, $sel, 'kode', 'asc');
        while ($row = mysql_fetch_assoc($hasil['exe'])) {
            $data['page'] = $hasil['page'];
            $data['total'] = $hasil['total'];
                $data['rows'][] = array(
                    'id' => $row['kode'],
                    'cell' => array(
                        $row['kode'],
                        '<a href="javascript:void(0)" onClick="$(\'#'.$elid.'\').hselect(\''.$row['kode'].'\');"  class="ficon-blueold fa fa-download fa-lg "  title="Pilih Data Ini"> </a> ',
                        $row['kode'],
                        $row['nama']
                    )
                );
        }
        echo json_encode($data);
        die();
    }

    function get_apbd_rincian() {
        $elid=$_REQUEST[elid];
        $flex_config = array(
            fields => array(
                array(hide => 'true', name => 'kode'), 
                array(display => 'Opsi', name => 'Pilih Data', hide => $show, width => '50', align => 'center'),
                array(display => 'Kode', name => 'kode', width => '150', align => 'left', sortable => true, filter => true),
                array(display => 'Nama', name => 'nama',  width => '200',align => 'left', sortable => true, filter => true)
            ),
            url => '?act=lookup&do=grid_get_apbd_rincian&id_apbd_obyek='.$_REQUEST['id_apbd_obyek'],
            showpage => '10',
            elid=>$elid,
            add_data=>false,
            dblclick=>'function(e){
                var nad=$(e).find("td").first().text(); 
                $("#'.$elid.'").hselect(nad);           
            }'
        );
        $grid = $this->render_grid_lookup($flex_config);
    }
    
    function get_apbd_rincian_detail(){
        $term = $_REQUEST['term'];
        $apbd_obyek = $_REQUEST['id_apbd_obyek'];
        
        $tbl = 'apbd_rincian';
        if($apbd_obyek>0){
        $where = "id_apbd_obyek='$apbd_obyek' and (kode like '%$term%' or nama  like '%$term%') ";
        }else{
        $where = " (kode like '%$term%' or nama  like '%$term%') ";
        }
        $sel = "kode,nama,id,kode as value, concat(kode,' / ',nama) as label";
        $hasil = $this->crud->get_data($tbl, $where, $sel);
        echo json_encode($hasil);
        die();

    }
    
    // end apbd_rincian

    // start std
        // start Akun
    function grid_get_std_kelompok() {
        $elid=$_REQUEST[elid];
        $tbl = 'std_kelompok';
        $where = "1";
        $sel = '*';

        $hasil = $this->crud->get_data_flexi($tbl, $where, $sel, 'kode', 'asc');
        while ($row = mysql_fetch_assoc($hasil['exe'])) {
            $data['page'] = $hasil['page'];
            $data['total'] = $hasil['total'];

            $data['rows'][] = array(
                'id' => $row['kode'],
                'cell' => array(
                    $row['kode'],
                    '<a href="javascript:void(0)" onClick="$(\'#'.$elid.'\').hselect(\''.$row['kode'].'\');"  class="ficon-blueold fa fa-download fa-lg "  title="Pilih Data Ini"> </a> ',
                    $row['kode'],
                     $row['nama'],
                )
            );
        }
        echo json_encode($data);
        die();
    }

    function get_std_kelompok() {
        $elid=$_REQUEST[elid];
        $flex_config = array(
            fields => array(
                array(hide => 'true', name => 'kode'), 
                array(display => 'Opsi', name => 'Pilih Data', hide => $show, width => '50', align => 'center'),
                array(display => 'Kode', name => 'kode', width => '150', align => 'left', sortable => true, filter => true),
                array(display => 'Nama', name => 'nama',  width => '200',align => 'left', sortable => true, filter => true)
            ),
            url => '?act=lookup&do=grid_get_std_kelompok',
            showpage => '10',
            elid=>$elid,
            add_data=>false,
            dblclick=>'function(e){
                var nad=$(e).find("td").first().text(); 
                $("#'.$elid.'").hselect(nad);           
            }'
        );
        $grid = $this->render_grid_lookup($flex_config);
    }
    
    function get_std_kelompok_detail(){
        $term = $_REQUEST['term'];
        
        $tbl = 'std_kelompok';
        $where = "(kode like '%$term%' or nama  like '%$term%') ";
        $sel = "kode,nama,id,kode as value, concat(kode,' / ',nama) as label";
        $hasil = $this->crud->get_data($tbl, $where, $sel);
        echo json_encode($hasil);
        die();

    }
    // end Akun

    // start std_jenis
    function grid_get_std_jenis() {
        $elid=$_REQUEST[elid];
        $std_kelompok=$_REQUEST['id_std_kelompok'];
        $tbl = 'std_jenis';
        $where = ($std_kelompok>0) ? "id_std_kelompok='$std_kelompok'" : "1";
        $sel = '*';

        $hasil = $this->crud->get_data_flexi($tbl, $where, $sel, 'kode', 'asc');
        while ($row = mysql_fetch_assoc($hasil['exe'])) {
            $data['page'] = $hasil['page'];
            $data['total'] = $hasil['total'];
                $data['rows'][] = array(
                    'id' => $row['kode'],
                    'cell' => array(
                        $row['kode'],
                        '<a href="javascript:void(0)" onClick="$(\'#'.$elid.'\').hselect(\''.$row['kode'].'\');"  class="ficon-blueold fa fa-download fa-lg "  title="Pilih Data Ini"> </a> ',
                        $row['kode'],
                        $row['nama']
                    )
                );
        }
        echo json_encode($data);
        die();
    }

    function get_std_jenis() {
        $elid=$_REQUEST[elid];
        $flex_config = array(
            fields => array(
                array(hide => 'true', name => 'kode'), 
                array(display => 'Opsi', name => 'Pilih Data', hide => $show, width => '50', align => 'center'),
                array(display => 'Kode', name => 'kode', width => '150', align => 'left', sortable => true, filter => true),
                array(display => 'Nama', name => 'nama',  width => '200',align => 'left', sortable => true, filter => true)
            ),
            url => '?act=lookup&do=grid_get_std_jenis&id_std_kelompok='.$_REQUEST['id_std_kelompok'],
            showpage => '10',
            elid=>$elid,
            add_data=>false,
            dblclick=>'function(e){
                var nad=$(e).find("td").first().text(); 
                $("#'.$elid.'").hselect(nad);           
            }'
        );
        $grid = $this->render_grid_lookup($flex_config);
    }
    
    function get_std_jenis_detail(){
        $term = $_REQUEST['term'];
        $std_kelompok = $_REQUEST['id_std_kelompok'];
        
        $tbl = 'std_jenis';
        if($std_kelompok>0){
        $where = "id_std_kelompok='$std_kelompok' and (kode like '%$term%' or nama  like '%$term%') ";
        }else{
        $where = " (kode like '%$term%' or nama  like '%$term%') ";
        }
        $sel = "kode,nama,id,kode as value, concat(kode,' / ',nama) as label";
        $hasil = $this->crud->get_data($tbl, $where, $sel);
        echo json_encode($hasil);
        die();

    }
    
    // end Akun
    

    // start std_rincian
    function grid_get_std_rincian() {
        $elid=$_REQUEST[elid];
        $std_jenis=$_REQUEST['id_std_jenis'];
        $tbl = 'std_rincian';
        $where = ($std_jenis>0) ? "id_std_jenis='$std_jenis'" : "1";
        $sel = '*';

        $hasil = $this->crud->get_data_flexi($tbl, $where, $sel, 'kode', 'asc');
        while ($row = mysql_fetch_assoc($hasil['exe'])) {
            $data['page'] = $hasil['page'];
            $data['total'] = $hasil['total'];
                $data['rows'][] = array(
                    'id' => $row['kode'],
                    'cell' => array(
                        $row['kode'],
                        '<a href="javascript:void(0)" onClick="$(\'#'.$elid.'\').hselect(\''.$row['kode'].'\');"  class="ficon-blueold fa fa-download fa-lg "  title="Pilih Data Ini"> </a> ',
                        $row['kode'],
                        $row['nama']
                    )
                );
        }
        echo json_encode($data);
        die();
    }

    function get_std_rincian() {
        $elid=$_REQUEST[elid];
        $flex_config = array(
            fields => array(
                array(hide => 'true', name => 'kode'), 
                array(display => 'Opsi', name => 'Pilih Data', hide => $show, width => '50', align => 'center'),
                array(display => 'Kode', name => 'kode', width => '150', align => 'left', sortable => true, filter => true),
                array(display => 'Nama', name => 'nama',  width => '200',align => 'left', sortable => true, filter => true)
            ),
            url => '?act=lookup&do=grid_get_std_rincian&id_std_jenis='.$_REQUEST['id_std_jenis'],
            showpage => '10',
            elid=>$elid,
            add_data=>false,
            dblclick=>'function(e){
                var nad=$(e).find("td").first().text(); 
                $("#'.$elid.'").hselect(nad);           
            }'
        );
        $grid = $this->render_grid_lookup($flex_config);
    }
    
    function get_std_rincian_detail(){
        $term = $_REQUEST['term'];
        $std_jenis = $_REQUEST['id_std_jenis'];
        
        $tbl = 'std_rincian';
        if($std_jenis>0){
        $where = "id_std_jenis='$std_jenis' and (kode like '%$term%' or nama  like '%$term%') ";
        }else{
        $where = " (kode like '%$term%' or nama  like '%$term%') ";
        }
        $sel = "kode,nama,id,kode as value, concat(kode,' / ',nama) as label";
        $hasil = $this->crud->get_data($tbl, $where, $sel);
        echo json_encode($hasil);
        die();

    }
    
    // end std

    //  Rekening LRA
    // start Akun
    function grid_get_lra_akun() {
        $elid=$_REQUEST[elid];
        $tbl = 'lra_akun';
        $where = "1";
        $sel = '*';

        $hasil = $this->crud->get_data_flexi($tbl, $where, $sel, 'kode', 'asc');
        while ($row = mysql_fetch_assoc($hasil['exe'])) {
            $data['page'] = $hasil['page'];
            $data['total'] = $hasil['total'];

            $data['rows'][] = array(
                'id' => $row['kode'],
                'cell' => array(
                    $row['kode'],
                    '<a href="javascript:void(0)" onClick="$(\'#'.$elid.'\').hselect(\''.$row['kode'].'\');"  class="ficon-blueold fa fa-download fa-lg "  title="Pilih Data Ini"> </a> ',
                    $row['kode'],
                     $row['nama'],
                )
            );
        }
        echo json_encode($data);
        die();
    }

    function get_lra_akun() {
        $elid=$_REQUEST[elid];
        $flex_config = array(
            fields => array(
                array(hide => 'true', name => 'kode'), 
                array(display => 'Opsi', name => 'Pilih Data', hide => $show, width => '50', align => 'center'),
                array(display => 'Kode', name => 'kode', width => '150', align => 'left', sortable => true, filter => true),
                array(display => 'Nama', name => 'nama',  width => '200',align => 'left', sortable => true, filter => true)
            ),
            url => '?act=lookup&do=grid_get_lra_akun',
            showpage => '10',
            elid=>$elid,
            add_data=>false,
            dblclick=>'function(e){
                var nad=$(e).find("td").first().text(); 
                $("#'.$elid.'").hselect(nad);           
            }'
        );
        $grid = $this->render_grid_lookup($flex_config);
    }
    
    function get_lra_akun_detail(){
        $term = $_REQUEST['term'];
        
        $tbl = 'lra_akun';
        $where = "(kode like '%$term%' or nama  like '%$term%') ";
        $sel = "kode,nama,id,kode as value, concat(kode,' / ',nama) as label";
        $hasil = $this->crud->get_data($tbl, $where, $sel);
        echo json_encode($hasil);
        die();

    }
    // end Akun

    // start lra_kelompok
    function grid_get_lra_kelompok() {
        $elid=$_REQUEST[elid];
        $lra_akun=$_REQUEST['id_lra_akun'];
        $tbl = 'lra_kelompok';
        $where = ($lra_akun>0) ? "id_lra_akun='$lra_akun'" : "1";
        $sel = '*';

        $hasil = $this->crud->get_data_flexi($tbl, $where, $sel, 'kode', 'asc');
        while ($row = mysql_fetch_assoc($hasil['exe'])) {
            $data['page'] = $hasil['page'];
            $data['total'] = $hasil['total'];
                $data['rows'][] = array(
                    'id' => $row['kode'],
                    'cell' => array(
                        $row['kode'],
                        '<a href="javascript:void(0)" onClick="$(\'#'.$elid.'\').hselect(\''.$row['kode'].'\');"  class="ficon-blueold fa fa-download fa-lg "  title="Pilih Data Ini"> </a> ',
                        $row['kode'],
                        $row['nama']
                    )
                );
        }
        echo json_encode($data);
        die();
    }

    function get_lra_kelompok() {
        $elid=$_REQUEST[elid];
        $flex_config = array(
            fields => array(
                array(hide => 'true', name => 'kode'), 
                array(display => 'Opsi', name => 'Pilih Data', hide => $show, width => '50', align => 'center'),
                array(display => 'Kode', name => 'kode', width => '150', align => 'left', sortable => true, filter => true),
                array(display => 'Nama', name => 'nama',  width => '200',align => 'left', sortable => true, filter => true)
            ),
            url => '?act=lookup&do=grid_get_lra_kelompok&id_lra_akun='.$_REQUEST['id_lra_akun'],
            showpage => '10',
            elid=>$elid,
            add_data=>false,
            dblclick=>'function(e){
                var nad=$(e).find("td").first().text(); 
                $("#'.$elid.'").hselect(nad);           
            }'
        );
        $grid = $this->render_grid_lookup($flex_config);
    }
    
    function get_lra_kelompok_detail(){
        $term = $_REQUEST['term'];
        $lra_akun = $_REQUEST['id_lra_akun'];
        
        $tbl = 'lra_kelompok';
        if($lra_akun>0){
        $where = "id_lra_akun='$lra_akun' and (kode like '%$term%' or nama  like '%$term%') ";
        }else{
        $where = " (kode like '%$term%' or nama  like '%$term%') ";
        }
        $sel = "kode,nama,id,kode as value, concat(kode,' / ',nama) as label";
        $hasil = $this->crud->get_data($tbl, $where, $sel);
        echo json_encode($hasil);
        die();

    }
    
    // end Akun
    

    // start lra_jenis
    function grid_get_lra_jenis() {
        $elid=$_REQUEST[elid];
        $lra_kelompok=$_REQUEST['id_lra_kelompok'];
        $tbl = 'lra_jenis';
        $where = ($lra_kelompok>0) ? "id_lra_kelompok='$lra_kelompok'" : "1";
        $sel = '*';

        $hasil = $this->crud->get_data_flexi($tbl, $where, $sel, 'kode', 'asc');
        while ($row = mysql_fetch_assoc($hasil['exe'])) {
            $data['page'] = $hasil['page'];
            $data['total'] = $hasil['total'];
                $data['rows'][] = array(
                    'id' => $row['kode'],
                    'cell' => array(
                        $row['kode'],
                        '<a href="javascript:void(0)" onClick="$(\'#'.$elid.'\').hselect(\''.$row['kode'].'\');"  class="ficon-blueold fa fa-download fa-lg "  title="Pilih Data Ini"> </a> ',
                        $row['kode'],
                        $row['nama']
                    )
                );
        }
        echo json_encode($data);
        die();
    }

    function get_lra_jenis() {
        $elid=$_REQUEST[elid];
        $flex_config = array(
            fields => array(
                array(hide => 'true', name => 'kode'), 
                array(display => 'Opsi', name => 'Pilih Data', hide => $show, width => '50', align => 'center'),
                array(display => 'Kode', name => 'kode', width => '150', align => 'left', sortable => true, filter => true),
                array(display => 'Nama', name => 'nama',  width => '200',align => 'left', sortable => true, filter => true)
            ),
            url => '?act=lookup&do=grid_get_lra_jenis&id_lra_kelompok='.$_REQUEST['id_lra_kelompok'],
            showpage => '10',
            elid=>$elid,
            add_data=>false,
            dblclick=>'function(e){
                var nad=$(e).find("td").first().text(); 
                $("#'.$elid.'").hselect(nad);           
            }'
        );
        $grid = $this->render_grid_lookup($flex_config);
    }
    
    function get_lra_jenis_detail(){
        $term = $_REQUEST['term'];
        $lra_kelompok = $_REQUEST['id_lra_kelompok'];
        
        $tbl = 'lra_jenis';
        if($lra_kelompok>0){
        $where = "id_lra_kelompok='$lra_kelompok' and (kode like '%$term%' or nama  like '%$term%') ";
        }else{
        $where = " (kode like '%$term%' or nama  like '%$term%') ";
        }
        $sel = "kode,nama,id,kode as value, concat(kode,' / ',nama) as label";
        $hasil = $this->crud->get_data($tbl, $where, $sel);
        echo json_encode($hasil);
        die();

    }
    
    // end Jenis
    // start lra_obyek
    function grid_get_lra_obyek() {
        $elid=$_REQUEST[elid];
        $lra_jenis=$_REQUEST['id_lra_jenis'];
        $tbl = 'lra_obyek';
        $where = ($lra_jenis>0) ? "id_lra_jenis='$lra_jenis'" : "1";
        $sel = '*';

        $hasil = $this->crud->get_data_flexi($tbl, $where, $sel, 'kode', 'asc');
        while ($row = mysql_fetch_assoc($hasil['exe'])) {
            $data['page'] = $hasil['page'];
            $data['total'] = $hasil['total'];
                $data['rows'][] = array(
                    'id' => $row['kode'],
                    'cell' => array(
                        $row['kode'],
                        '<a href="javascript:void(0)" onClick="$(\'#'.$elid.'\').hselect(\''.$row['kode'].'\');"  class="ficon-blueold fa fa-download fa-lg "  title="Pilih Data Ini"> </a> ',
                        $row['kode'],
                        $row['nama']
                    )
                );
        }
        echo json_encode($data);
        die();
    }

    function get_lra_obyek() {
        $elid=$_REQUEST[elid];
        $flex_config = array(
            fields => array(
                array(hide => 'true', name => 'kode'), 
                array(display => 'Opsi', name => 'Pilih Data', hide => $show, width => '50', align => 'center'),
                array(display => 'Kode', name => 'kode', width => '150', align => 'left', sortable => true, filter => true),
                array(display => 'Nama', name => 'nama',  width => '200',align => 'left', sortable => true, filter => true)
            ),
            url => '?act=lookup&do=grid_get_lra_obyek&id_lra_jenis='.$_REQUEST['id_lra_jenis'],
            showpage => '10',
            elid=>$elid,
            add_data=>false,
            dblclick=>'function(e){
                var nad=$(e).find("td").first().text(); 
                $("#'.$elid.'").hselect(nad);           
            }'
        );
        $grid = $this->render_grid_lookup($flex_config);
    }
    
    function get_lra_obyek_detail(){
        $term = $_REQUEST['term'];
        $lra_jenis = $_REQUEST['id_lra_jenis'];
        
        $tbl = 'lra_obyek';
        if($lra_jenis>0){
        $where = "id_lra_jenis='$lra_jenis' and (kode like '%$term%' or nama  like '%$term%') ";
        }else{
        $where = " (kode like '%$term%' or nama  like '%$term%') ";
        }
        $sel = "kode,nama,id,kode as value, concat(kode,' / ',nama) as label";
        $hasil = $this->crud->get_data($tbl, $where, $sel);
        echo json_encode($hasil);
        die();

    }
    
    // end sub Jenis
    // start lra_referensi
    function grid_get_lra_referensi() {
        $elid=$_REQUEST[elid];
        $lra_obyek=$_REQUEST['id_lra_obyek'];
        $tbl = 'lra_referensi';
        $where = ($lra_obyek>0) ? "id_lra_obyek='$lra_obyek'" : "1";
        $sel = '*';

        $hasil = $this->crud->get_data_flexi($tbl, $where, $sel, 'kode', 'asc');
        while ($row = mysql_fetch_assoc($hasil['exe'])) {
            $data['page'] = $hasil['page'];
            $data['total'] = $hasil['total'];
                $data['rows'][] = array(
                    'id' => $row['kode'],
                    'cell' => array(
                        $row['kode'],
                        '<a href="javascript:void(0)" onClick="$(\'#'.$elid.'\').hselect(\''.$row['kode'].'\');"  class="ficon-blueold fa fa-download fa-lg "  title="Pilih Data Ini"> </a> ',
                        $row['kode'],
                        $row['nama']
                    )
                );
        }
        echo json_encode($data);
        die();
    }

    function get_lra_referensi() {
        $elid=$_REQUEST[elid];
        $flex_config = array(
            fields => array(
                array(hide => 'true', name => 'kode'), 
                array(display => 'Opsi', name => 'Pilih Data', hide => $show, width => '50', align => 'center'),
                array(display => 'Kode', name => 'kode', width => '150', align => 'left', sortable => true, filter => true),
                array(display => 'Nama', name => 'nama',  width => '200',align => 'left', sortable => true, filter => true)
            ),
            url => '?act=lookup&do=grid_get_lra_referensi&id_lra_obyek='.$_REQUEST['id_lra_obyek'],
            showpage => '10',
            elid=>$elid,
            add_data=>false,
            dblclick=>'function(e){
                var nad=$(e).find("td").first().text(); 
                $("#'.$elid.'").hselect(nad);           
            }'
        );
        $grid = $this->render_grid_lookup($flex_config);
    }
    
    function get_lra_referensi_detail(){
        $term = $_REQUEST['term'];
        $lra_obyek = $_REQUEST['id_lra_obyek'];
        
        $tbl = 'lra_referensi';
        if($lra_obyek>0){
        $where = "id_lra_obyek='$lra_obyek' and (kode like '%$term%' or nama  like '%$term%') ";
        }else{
        $where = " (kode like '%$term%' or nama  like '%$term%') ";
        }
        $sel = "kode,nama,id,kode as value, concat(kode,' / ',nama) as label";
        $hasil = $this->crud->get_data($tbl, $where, $sel);
        echo json_encode($hasil);
        die();

    }
    
    // end lra_referensi


    // start Potongan
    function grid_get_potongan() {
        $elid=$_REQUEST[elid];
        $tbl = 'potongan';
        $where = "1";
        $sel = '*';

        $hasil = $this->crud->get_data_flexi($tbl, $where, $sel, 'kode', 'asc');
        while ($row = mysql_fetch_assoc($hasil['exe'])) {
            $data['page'] = $hasil['page'];
            $data['total'] = $hasil['total'];

            $data['rows'][] = array(
                'id' => $row['kode'],
                'cell' => array(
                    $row['kode'],
                    '<a href="javascript:void(0)" onClick="$(\'#'.$elid.'\').hselect(\''.$row['kode'].'\');"  class="ficon-blueold fa fa-download fa-lg "  title="Pilih Data Ini"> </a> ',
                    $row['kode'],
                     $row['nama'],
                )
            );
        }
        echo json_encode($data);
        die();
    }

    function get_potongan() {
        $elid=$_REQUEST[elid];
        $flex_config = array(
            fields => array(
                array(hide => 'true', name => 'kode'), 
                array(display => 'Opsi', name => 'Pilih Data', hide => $show, width => '50', align => 'center'),
                array(display => 'Kode', name => 'kode', width => '150', align => 'left', sortable => true, filter => true),
                array(display => 'Nama', name => 'nama',  width => '200',align => 'left', sortable => true, filter => true)
            ),
            url => '?act=lookup&do=grid_get_potongan',
            showpage => '10',
            elid=>$elid,
            add_data=>false,
            dblclick=>'function(e){
                var nad=$(e).find("td").first().text(); 
                $("#'.$elid.'").hselect(nad);           
            }'
        );
        $grid = $this->render_grid_lookup($flex_config);
    }
    
    function get_potongan_detail(){
        $term = $_REQUEST['term'];
        
        $tbl = 'potongan';
        $where = "(kode like '%$term%' or nama  like '%$term%') ";
        $sel = "kode,nama,id,kode as value, concat(kode,' / ',nama) as label";
        $hasil = $this->crud->get_data($tbl, $where, $sel);
        echo json_encode($hasil);
        die();

    }
    // end Potongan


}

?>
