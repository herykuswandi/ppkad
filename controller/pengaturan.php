<?php

class Pengaturan extends Yaridhane {

    private $page;
    private $method;
    private $vars = array();

    function __construct($page, $method) {
        $this->page = $page;
        $this->method = $method;
        parent::__construct();
    }
	
	// start item
    function item() {
	
        $this->vars['title'] = 'Pengaturan Item Detail';
        $flex_config = array(
            fields => array(
                array(display => 'Opsi', name => 'Option', hide => $show, width => '100', align => 'center'),
                array(display => 'Nama', name => 'nama', align => 'left', width => '300', sortable => true, filter => true),
               ),
            url => '?act=pengaturan&do=grid_item',
            showpage => '20'
        );
        $grid = $this->render_grid($flex_config);
        $this->vars['grid'] = $grid;
        $this->loadView($this->page, $this->method, $this->vars,true);
    }

    function grid_item() {
		$kdo=$_SESSION['sinad']['kdo'];
        $tbl = 'atur_item';
        $where = "hapus='0' and kdo='$kdo'";
        $sel = '*';

        $hasil = $this->crud->get_data_flexi($tbl, $where, $sel, 'nama', 'asc');
        while ($row = mysql_fetch_assoc($hasil['exe'])) {
            $data['page'] = $hasil['page'];
            $data['total'] = $hasil['total'];
            $data['rows'][] = array(
                'id' => $row['id'],
                'cell' => array(
                    '<a href="?act=pengaturan&do=form_item&id=' . $row['id'] . '" onClick="return modaler(this);"  class="ficon-green fa fa-pencil-square-o fa-lg linkbox"  label="Ubah Item"  title="Ubah Item"> </a> 
					<a href="javascript:void(0)" onClick="del_data(\'?act=pengaturan&do=hapus_item&id=' . $row['id'] . '\',this)" class="ficon-red fa fa-trash-o fa-lg "   title="Hapus Item" ></a>',
                    $row['nama']
                    )
            );
        }
        echo json_encode($data);
        die();
    }

	 function form_item() {
        $id = $_REQUEST['id'];
	    $this->vars['id'] = $id;
		if($id>0){
        	$this->vars['detail'] = $this->crud->get_single_data('atur_item', "id='$id'");
        }
		$this->loadView($this->page, $this->method, $this->vars, true);
    }
	
	 function simpan_item() {
        $id = $_REQUEST['id'];
        $rows = array("kdo"=>$_SESSION['sinad']['kdo']);
		
        if ($id > 0) {
            $akun = $this->crud->update($rows, 'atur_item', true);
        } else {
            $akun = $this->crud->create($rows, 'atur_item', true);
        }
    }

    function hapus_item() {
        $id = $_REQUEST['id'];
		
		$kdo=$_SESSION['sinad']['kdo'];
		$this->crud->query("update atur_item set hapus='1' where id='$id'  and kdo='$kdo'");
	}
	// end item
	
	// start Jenis Tarif
    function tarif() {
	
        $this->vars['title'] = 'Pengaturan Jenis Tarif';
        $flex_config = array(
            fields => array(
                array(display => 'Opsi', name => 'Option', hide => $show, width => '100', align => 'center'),
                array(display => 'Nama', name => 'nama', align => 'left', width => '130', sortable => true, filter => true),
				array(display => 'Kelipatan Harga', name => 'kali', align => 'left', width => '120', sortable => true, filter => true)/*, 
				array(display => 'Lama Proses Laundry', name => 'jam', align => 'left', width => '150', sortable => true, filter => true),*/
               ),
            url => '?act=pengaturan&do=grid_tarif',
            showpage => '20'
        );
        $grid = $this->render_grid($flex_config);
        $this->vars['grid'] = $grid;
        $this->loadView($this->page, $this->method, $this->vars,true);
    }

    function grid_tarif() {
		$kdo=$_SESSION['sinad']['kdo'];
        $tbl = 'atur_jenis_tarif';
        $where = "hapus='0' and kdo='$kdo'";
        $sel = '*';

        $hasil = $this->crud->get_data_flexi($tbl, $where, $sel, 'nama', 'asc');
        while ($row = mysql_fetch_assoc($hasil['exe'])) {
            $data['page'] = $hasil['page'];
            $data['total'] = $hasil['total'];
            $data['rows'][] = array(
                'id' => $row['id'],
                'cell' => array(
                    '<a href="?act=pengaturan&do=form_tarif&id=' . $row['id'] . '" onClick="return modaler(this);"  class="ficon-green fa fa-pencil-square-o fa-lg linkbox"  label="Ubah Jenis Tarif"  title="Ubah Tarif"> </a> 
					<a href="javascript:void(0)" onClick="del_data(\'?act=pengaturan&do=hapus_tarif&id=' . $row['id'] . '\',this)" class="ficon-red fa fa-trash-o fa-lg "   title="Hapus Tarif" ></a>',
                    $row['nama'],
                   rupiah($row['kali']).' Kali'/*,
                    rupiah($row['jam']).' Jam'*/
                    )
            );
        }
        echo json_encode($data);
        die();
    }

	 function form_tarif() {
        $id = $_REQUEST['id'];
	    $this->vars['id'] = $id;
		if($id>0){
        	$this->vars['detail'] = $this->crud->get_single_data('atur_jenis_tarif', "id='$id'");
        }
		$this->loadView($this->page, $this->method, $this->vars, true);
    }
	
	 function simpan_tarif() {
        $id = $_REQUEST['id'];
        $rows = array("kdo"=>$_SESSION['sinad']['kdo']);
		
        if ($id > 0) {
            $akun = $this->crud->update($rows, 'atur_jenis_tarif', true);
        } else {
            $akun = $this->crud->create($rows, 'atur_jenis_tarif', true);
        }
    }

    function hapus_tarif() {
        $id = $_REQUEST['id'];
		$kdo=$_SESSION['sinad']['kdo'];
		$this->crud->query("update atur_jenis_tarif set hapus='1' where id='$id'  and kdo='$kdo'");
	}
	// end tarif
	
	
	// start satuan
    function satuan() {
	
        $this->vars['title'] = 'Pengaturan Satuan / Unit';
        $flex_config = array(
            fields => array(
                array(display => 'Opsi', name => 'Option', hide => $show, width => '100', align => 'center'),
                array(display => 'Nama', name => 'nama', align => 'left', width => '300', sortable => true, filter => true),
               ),
            url => '?act=pengaturan&do=grid_satuan',
            showpage => '20'
        );
        $grid = $this->render_grid($flex_config);
        $this->vars['grid'] = $grid;
        $this->loadView($this->page, $this->method, $this->vars,true);
    }

    function grid_satuan() {
		$kdo=$_SESSION['sinad']['kdo'];
        $where = "hapus='0' and kdo='$kdo'";
        $tbl = 'atur_satuan';
        $sel = '*';

        $hasil = $this->crud->get_data_flexi($tbl, $where, $sel, 'nama', 'asc');
        while ($row = mysql_fetch_assoc($hasil['exe'])) {
            $data['page'] = $hasil['page'];
            $data['total'] = $hasil['total'];
            $data['rows'][] = array(
                'id' => $row['id'],
                'cell' => array(
                   ($row['statis']==0) ?  '<a href="?act=pengaturan&do=form_satuan&id=' . $row['id'] . '" onClick="return modaler(this);"  class="ficon-green fa fa-pencil-square-o fa-lg linkbox"  label="Ubah Satuan / Unit"  title="Ubah satuan"> </a> 
					<a href="javascript:void(0)" onClick="del_data(\'?act=pengaturan&do=hapus_satuan&id=' . $row['id'] . '\',this)" class="ficon-red fa fa-trash-o fa-lg "   title="Hapus Satuan" ></a>':  '<a href="?act=pengaturan&do=form_satuan&id=' . $row['id'] . '" onClick="return modaler(this);"  class="ficon-green fa fa-pencil-square-o fa-lg linkbox"  label="Ubah Satuan / Unit"  title="Ubah satuan"> </a>',
                    $row['nama'] 
                    )
            );
        }
        echo json_encode($data);
        die();
    }

	 function form_satuan() {
        $id = $_REQUEST['id'];
	    $this->vars['id'] = $id;
		if($id>0){
        	$this->vars['detail'] = $this->crud->get_single_data('atur_satuan', "id='$id'");
        }
		$this->loadView($this->page, $this->method, $this->vars, true);
    }
	
	 function simpan_satuan() {
        $id = $_REQUEST['id'];
        $rows = array("kdo"=>$_SESSION['sinad']['kdo']);
		
        if ($id > 0) {
            $akun = $this->crud->update($rows, 'atur_satuan', true);
        } else {
            $akun = $this->crud->create($rows, 'atur_satuan', true);
        }
    }

    function hapus_satuan() {
        $id = $_REQUEST['id'];
		$kdo=$_SESSION['sinad']['kdo'];
		$this->crud->query("update atur_satuan set hapus='1' where id='$id'  and kdo='$kdo'");
	}
	// end satuan
	
	
	
	// start tahapan
    function tahapan() {
	
        $this->vars['title'] = 'Pengaturan Tahapan Pengerjaan';
        $flex_config = array(
            fields => array(
                array(display => 'Opsi', name => 'Option', hide => $show, width => '100', align => 'center'),
                array(display => 'Nama', name => 'nama', align => 'left', width => '200', sortable => true, filter => true), 
                array(display => 'Nomor Urut', name => 'urut', align => 'left', width => '100', sortable => true, filter => true),
				array(display => 'Gaji Karyawan Per Tahapan', name => 'upah', align => 'left', width => '200', sortable => true),
               ),
            url => '?act=pengaturan&do=grid_tahapan',
            showpage => '10'
        );
        $grid = $this->render_grid($flex_config);
        $this->vars['grid'] = $grid;
        $this->loadView($this->page, $this->method, $this->vars,true);
    }

    function grid_tahapan() {
		$kdo=$_SESSION['sinad']['kdo'];
        $where = "hapus='0' and kdo='$kdo'";
        $tbl = 'atur_tahapan';
        $sel = '*';

        $hasil = $this->crud->get_data_flexi($tbl, $where, $sel, 'nama', 'asc');
        while ($row = mysql_fetch_assoc($hasil['exe'])) {
            $data['page'] = $hasil['page'];
            $data['total'] = $hasil['total'];
            $data['rows'][] = array(
                'id' => $row['id'],
                'cell' => array(
                    
					($row['statis']==0) ? '<a href="?act=pengaturan&do=form_tahapan&id=' . $row['id'] . '" onClick="return modaler(this);"  class="ficon-green fa fa-pencil-square-o fa-lg linkbox"  label="Ubah Tahapan Pengerjaan"  title="Ubah Tahapan"> </a> 
					<a href="javascript:void(0)" onClick="del_data(\'?act=pengaturan&do=hapus_tahapan&id=' . $row['id'] . '\',this)" class="ficon-red fa fa-trash-o fa-lg "   title="Hapus Tahapan" ></a>'
					: '<a href="?act=pengaturan&do=form_tahapan&id=' . $row['id'] . '" onClick="return modaler(this);"  class="ficon-green fa fa-pencil-square-o fa-lg linkbox"  label="Ubah Tahapan Pengerjaan"  title="Ubah Tahapan"> </a> ',
                    $row['nama'],
                     ($row['urut']=="first")? "Awal" : (($row['urut']=="last") ? "Akhir" : $row['urut']) ,
                    ($row['upah']==0) ? 'Tidak' : 'Ya'
                    )
            );
        }
        echo json_encode($data);
        die();
    }

	 function form_tahapan() {
        $id = $_REQUEST['id'];
		
		$kdo=$_SESSION['sinad']['kdo'];
	    $this->vars['id'] = $id;
		if($id>0){
        	$this->vars['detail'] = $this->crud->get_single_data('atur_tahapan', "id='$id'");
        }else{
			$urut = $this->crud->get_single_data('atur_tahapan', "kdo='$kdo' and hapus=0 and statis=0 order by urut desc","urut");
			$nor=$urut['urut']+1;
			$this->vars['detail']=array('urut'=>$nor);
		}
		$this->loadView($this->page, $this->method, $this->vars, true);
    }
	
	 function simpan_tahapan() {
        $id = $_REQUEST['id'];
        $rows = array("kdo"=>$_SESSION['sinad']['kdo']);
		
        if ($id > 0) {
            $akun = $this->crud->update($rows, 'atur_tahapan', true);
        } else {
            $akun = $this->crud->create($rows, 'atur_tahapan', true);
        }
    }

    function hapus_tahapan() {
        $id = $_REQUEST['id'];
		$kdo=$_SESSION['sinad']['kdo'];
		$this->crud->query("update atur_tahapan set hapus='1' where id='$id' and kdo='$kdo'");
	}
	
	function cek_tahapan(){
		 $id = $_REQUEST['id'];
		 $urut = $_REQUEST['urut'];
		 
		$kdo=$_SESSION['sinad']['kdo'];
	    $this->vars['id'] = $id;
		if($id>0){
        	$cek = $this->crud->get_single_data('atur_tahapan', "urut='$urut' and id!='$id' and kdo='$kdo' and hapus=0","id");
		}else{
        	$cek = $this->crud->get_single_data('atur_tahapan', "urut='$urut'  and kdo='$kdo' and hapus=0","id");
		}
		echo $cek['id'];
		
	}
	// end tahapan
	
	
	// start status ambil
    function status() {
	
        $this->vars['title'] = 'Pengaturan Status Pengambilan';
        $flex_config = array(
            fields => array(
                array(display => 'Opsi', name => 'Option', hide => $show, width => '100', align => 'center'),
                array(display => 'Nama', name => 'nama', align => 'left', width => '200', sortable => true, filter => true)
               ),
            url => '?act=pengaturan&do=grid_status',
            showpage => '10'
        );
        $grid = $this->render_grid($flex_config);
        $this->vars['grid'] = $grid;
        $this->loadView($this->page, $this->method, $this->vars,true);
    }

    function grid_status() {
	
		$kdo=$_SESSION['sinad']['kdo'];
        $where = "hapus='0' and kdo='$kdo'";
        $tbl = 'atur_status';
        $sel = '*';

        $hasil = $this->crud->get_data_flexi($tbl, $where, $sel, 'nama', 'asc');
        while ($row = mysql_fetch_assoc($hasil['exe'])) {
            $data['page'] = $hasil['page'];
            $data['total'] = $hasil['total'];
            $data['rows'][] = array(
                'id' => $row['id'],
                'cell' => array(
                    
					($row['statis']==0) ? '<a href="?act=pengaturan&do=form_status&id=' . $row['id'] . '" onClick="return modaler(this);"  class="ficon-green fa fa-pencil-square-o fa-lg linkbox"  label="Ubah Status Pengambilan"  title="Ubah Status"> </a> 
					<a href="javascript:void(0)" onClick="del_data(\'?act=pengaturan&do=hapus_status&id=' . $row['id'] . '\',this)" class="ficon-red fa fa-trash-o fa-lg "   title="Hapus Status Pengambilan" ></a>'
					: '<a href="?act=pengaturan&do=form_status&id=' . $row['id'] . '" onClick="return modaler(this);"  class="ficon-green fa fa-pencil-square-o fa-lg linkbox"  label="Ubah Status Pengambilan"  title="Ubah Status"> </a> ',
                    $row['nama']
                    )
            );
        }
        echo json_encode($data);
        die();
    }

	 function form_status() {
        $id = $_REQUEST['id'];
	    $this->vars['id'] = $id;
		if($id>0){
        	$this->vars['detail'] = $this->crud->get_single_data('atur_status', "id='$id'");
        }
		$this->loadView($this->page, $this->method, $this->vars, true);
    }
	
	 function simpan_status() {
        $id = $_REQUEST['id'];
        $rows = array("kdo"=>$_SESSION['sinad']['kdo']);
		
        if ($id > 0) {
            $akun = $this->crud->update($rows, 'atur_status', true);
        } else {
            $akun = $this->crud->create($rows, 'atur_status', true);
        }
    }

    function hapus_status() {
        $id = $_REQUEST['id'];
		$kdo=$_SESSION['sinad']['kdo'];
		$this->crud->query("update atur_status set hapus='1' where id='$id' and kdo='$kdo'");
	}
	// end status ambil
	
	
	
	
	// start ser parfum
    function parfum() {
	
        $this->vars['title'] = 'Pengaturan Parfum';
        $flex_config = array(
            fields => array(
                array(display => 'Opsi', name => 'Option', hide => $show, width => '100', align => 'center'),
                array(display => 'Nama', name => 'nama', align => 'left', width => '200', sortable => true, filter => true), 
				array(display => 'Default Di Transaksi', name => 'aktif', align => 'left', width => '300', sortable => true),
               ),
            url => '?act=pengaturan&do=grid_parfum',
            showpage => '10'
        );
        $grid = $this->render_grid($flex_config);
        $this->vars['grid'] = $grid;
        $this->loadView($this->page, $this->method, $this->vars,true);
    }

    function grid_parfum() {
	
		$kdo=$_SESSION['sinad']['kdo'];
        $where = "hapus='0' and kdo='$kdo'";
        $tbl = 'atur_parfum';
        $sel = '*';

        $hasil = $this->crud->get_data_flexi($tbl, $where, $sel, 'nama', 'asc');
        while ($row = mysql_fetch_assoc($hasil['exe'])) {
            $data['page'] = $hasil['page'];
            $data['total'] = $hasil['total'];
            $data['rows'][] = array(
                'id' => $row['id'],
                'cell' => array(
                    
					'<a href="?act=pengaturan&do=form_parfum&id=' . $row['id'] . '" onClick="return modaler(this);"  class="ficon-green fa fa-pencil-square-o fa-lg linkbox"  label="Ubah Jenis Parfum"  title="Ubah Tahapan"> </a> 
					<a href="javascript:void(0)" onClick="del_data(\'?act=pengaturan&do=hapus_parfum&id=' . $row['id'] . '\',this)" class="ficon-red fa fa-trash-o fa-lg "   title="Hapus Parfum" ></a>'
					,
                    $row['nama'],
                    ($row['aktif']==0) ? 'Tidak' : 'Ya'
                    )
            );
        }
        echo json_encode($data);
        die();
    }

	 function form_parfum() {
        $id = $_REQUEST['id'];
	    $this->vars['id'] = $id;
		if($id>0){
        	$this->vars['detail'] = $this->crud->get_single_data('atur_parfum', "id='$id'");
        }
		$this->loadView($this->page, $this->method, $this->vars, true);
    }
	
	 function simpan_parfum() {
        $id = $_REQUEST['id'];
		
		$kdo=$_SESSION['sinad']['kdo'];
        $rows = array("kdo"=>$kdo);
	
		if($_REQUEST['aktif']==1){
			$this->crud->query("update atur_parfum set aktif='0' where kdo='$kdo'");
		}
        if ($id > 0) {
            $akun = $this->crud->update($rows, 'atur_parfum', true);
        } else {
            $akun = $this->crud->create($rows, 'atur_parfum', true);
        }
    }

    function hapus_parfum() {
        $id = $_REQUEST['id'];
		$kdo=$_SESSION['sinad']['kdo'];
		$this->crud->query("update atur_parfum set hapus='1' where id='$id' and kdo='$kdo'");
	}
	// end parfum
	
	// start rak
    function rak() {
	
        $this->vars['title'] = 'Pengaturan Rak';
        $flex_config = array(
            fields => array(
                array(display => 'Opsi', name => 'Option', hide => $show, width => '100', align => 'center'),
                array(display => 'Nama', name => 'nama', align => 'left', width => '300', sortable => true, filter => true),array(display => 'Status Rak', name => 'status_rak', align => 'left', width => '300', sortable => true),
               ),
            url => '?act=pengaturan&do=grid_rak',
            showpage => '20'
        );
        $grid = $this->render_grid($flex_config);
        $this->vars['grid'] = $grid;
        $this->loadView($this->page, $this->method, $this->vars,true);
    }

    function grid_rak() {
		$kdo=$_SESSION['sinad']['kdo'];
        $where = "hapus='0' and kdo='$kdo'";
        $tbl = 'atur_rak';
        $sel = '*';

        $hasil = $this->crud->get_data_flexi($tbl, $where, $sel, 'nama', 'asc');
        while ($row = mysql_fetch_assoc($hasil['exe'])) {
            $data['page'] = $hasil['page'];
            $data['total'] = $hasil['total'];
            $data['rows'][] = array(
                'id' => $row['id'],
                'cell' => array(
                    '<a href="?act=pengaturan&do=form_rak&id=' . $row['id'] . '" onClick="return modaler(this);"  class="ficon-green fa fa-pencil-square-o fa-lg linkbox"  label="Ubah Rak"  title="Ubah Rak"> </a> 
					<a href="javascript:void(0)" onClick="del_data(\'?act=pengaturan&do=hapus_rak&id=' . $row['id'] . '\',this)" class="ficon-red fa fa-trash-o fa-lg "   title="Hapus Rak" ></a>',
                    $row['nama'],
                    ($row['status_rak']==0) ? 'Kosong' : 'Terisi'
                    )
            );
        }
        echo json_encode($data);
        die();
    }

	 function form_rak() {
        $id = $_REQUEST['id'];
	    $this->vars['id'] = $id;
		if($id>0){
        	$this->vars['detail'] = $this->crud->get_single_data('atur_rak', "id='$id'");
        }
		$this->loadView($this->page, $this->method, $this->vars, true);
    }
	
	 function simpan_rak() {
        $id = $_REQUEST['id'];
        $rows = array("kdo"=>$_SESSION['sinad']['kdo']);
		
        if ($id > 0) {
            $akun = $this->crud->update($rows, 'atur_rak', true);
        } else {
            $akun = $this->crud->create($rows, 'atur_rak', true);
        }
    }

    function hapus_rak() {
        $id = $_REQUEST['id'];
		$kdo=$_SESSION['sinad']['kdo'];
		$this->crud->query("update atur_rak set hapus='1' where id='$id'  and kdo='$kdo'");
	}
	// end rak
	
	
	// start jabatan
    function jabatan() {
	
        $this->vars['title'] = 'Pengaturan Jabatan Karyawan';
        $flex_config = array(
            fields => array(
                array(display => 'Opsi', name => 'Option', hide => $show, width => '100', align => 'center'),
                array(display => 'Nama', name => 'nama', align => 'left', width => '300', sortable => true, filter => true)
               ),
            url => '?act=pengaturan&do=grid_jabatan',
            showpage => '20'
        );
        $grid = $this->render_grid($flex_config);
        $this->vars['grid'] = $grid;
        $this->loadView($this->page, $this->method, $this->vars,true);
    }

    function grid_jabatan() {
		$kdo=$_SESSION['sinad']['kdo'];
        $where = "hapus='0' and kdo='$kdo'";
        $tbl = 'karyawan_jabatan';
        $sel = '*';

        $hasil = $this->crud->get_data_flexi($tbl, $where, $sel, 'nama', 'asc');
        while ($row = mysql_fetch_assoc($hasil['exe'])) {
            $data['page'] = $hasil['page'];
            $data['total'] = $hasil['total'];
            $data['rows'][] = array(
                'id' => $row['id'],
                'cell' => array(
                    '<a href="?act=pengaturan&do=form_jabatan&id=' . $row['id'] . '" onClick="return modaler(this);"  class="ficon-green fa fa-pencil-square-o fa-lg linkbox"  label="Ubah Jabatan"  title="Ubah Jabatan"> </a> 
					<a href="javascript:void(0)" onClick="del_data(\'?act=pengaturan&do=hapus_jabatan&id=' . $row['id'] . '\',this)" class="ficon-red fa fa-trash-o fa-lg "   title="Hapus Jabatan" ></a>',
                    $row['nama']
                    )
            );
        }
        echo json_encode($data);
        die();
    }

	 function form_jabatan() {
        $id = $_REQUEST['id'];
	    $this->vars['id'] = $id;
		if($id>0){
        	$this->vars['detail'] = $this->crud->get_single_data('karyawan_jabatan', "id='$id'");
        }
		$this->loadView($this->page, $this->method, $this->vars, true);
    }
	
	 function simpan_jabatan() {
        $id = $_REQUEST['id'];
        $rows = array("kdo"=>$_SESSION['sinad']['kdo']);
		
        if ($id > 0) {
            $akun = $this->crud->update($rows, 'karyawan_jabatan', true);
        } else {
            $akun = $this->crud->create($rows, 'karyawan_jabatan', true);
        }
    }

    function hapus_jabatan() {
        $id = $_REQUEST['id'];
		$kdo=$_SESSION['sinad']['kdo'];
		$this->crud->query("update karyawan_jabatan set hapus='1' where id='$id'  and kdo='$kdo'");
	}
	// end jabatan
	// profil outlet
	
	 function profil() {
        $kdo = $_SESSION['sinad']['kdo'];
		
	
        	$this->vars['detail'] = $this->crud->get_single_data('outlet', "kode='$kdo'");
        $this->vars['title'] = 'Profil Outlet '.$this->vars['detail']['nama'];
		$this->loadView($this->page, $this->method, $this->vars,true);
    }
	
	 function simpan_outlet() {
        $rows = array("kode"=>$_SESSION['sinad']['kdo']);
        $akun = $this->crud->update($rows, 'outlet', true,"","kode");
     
    }
	
	// end profil
	// aplikasi
	
	 function aplikasi() {
        $kdo = $_SESSION['sinad']['kdo'];
		$this->vars['detail'] = $this->crud->get_single_data('atur_aplikasi', "kdo='$kdo'");
        $this->vars['title'] = 'Pengaturan Aplikasi ';
		$this->loadView($this->page, $this->method, $this->vars,true);
    }
	
	 function simpan_aplikasi() {
        $rows = array("kdo"=>$_SESSION['sinad']['kdo']);
		$detail = $this->crud->get_single_data('atur_aplikasi', "kdo='$kdo'");
		$rows['jenis_aplikasi']=$_REQUEST['jenis_aplikasi'];
		$rows['tipe_aplikasi']=$_REQUEST['tipe_aplikasi'];
		$rows['bayar_transaksi']=$_REQUEST['bayar_transaksi'];
		$rows['fitur']=(isset($_REQUEST['fitur'])) ? implode(',',$_REQUEST['fitur']) : '' ;
		$rows['frontpage']=(isset($_REQUEST['fitur'])) ? $_REQUEST['frontpage'] : '' ;
		if(is_array($detail) && count($detail)>0){
			$rows['id']=$detail['id'];
			$akun = $this->crud->update($rows, 'atur_aplikasi');
		}else{
	        $akun = $this->crud->create($rows, 'atur_aplikasi');
		}
     
    }
	
	// end aplikasi
	
	
}

?>
