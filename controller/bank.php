<?php
class Bank extends Yaridhane {

    private $page;
    private $method;
    private $vars = array();

    function __construct($page, $method) {
        $this->page = $page;
        $this->method = $method;
        parent::__construct();
    }
	
	
	// start tim_anggaran
    function __default() {
	
        $this->vars['title'] = 'Data Bank';
        $flex_config = array(
            fields => array(
                array(display => 'Opsi', name => 'Option', width => '70', align => 'center'),
                array(display => 'Kode', name => 'b.kode', align => 'left', width => '80', sortable => true, filter => true), 
                array(display => 'Nama', name => 'b.nama', align => 'left', width => '150', sortable => true, filter => true), 
                array(display => 'No. Rekening', name => 'b.no_rek', align => 'left', width => '150', sortable => true, filter => true), 
                array(display => 'Kode Rekening', name => 't.uraian', align => 'left', width => '150'), 
                array(display => 'Nama Rekening', name => 't.uraian', align => 'left', width => '300'), 
                ),
            url => '?act=bank&do=grid',
            showpage => '10',
			isprint=>$this->vars['isprint']
        );
        $grid = $this->render_grid($flex_config);
        $this->vars['grid'] = $grid;
        $this->loadView($this->page, $this->method, $this->vars,true);
    }

    function grid() {
        $tbl = 'bank b,apbd_rincian r'; // nama tabel
        $sel = 'b.*,
                r.nama nama_apbd_rincian,r.kode kode_apbd_rincian
                '; // default * 
        $where = "b.id_apbd_rincian=r.id"; // jika where kosong isi dengan 1
        $order='b.kode';
        $direction='asc';
        $hasil = $this->crud->get_data_flexi($tbl, $where, $sel, $order,$direction);
		while ($row = mysql_fetch_assoc($hasil['exe'])) {
            $data['page'] = $hasil['page'];
            $data['total'] = $hasil['total'];
            $data['rows'][] = array(
                'id' => $row['id'],
                'cell' => array(
                    '<a href="?act=bank&do=form&id=' . $row['id'] . '" onClick="return modaler(this);"  class="ficon-green fa fa-pencil-square-o fa-lg linkbox"  label="Ubah Bank"  title="Ubah Bank"> </a> 
					<a href="javascript:void(0)" onClick="del_data(\'?act=bank&do=hapus&id=' . $row['id'] . '\',this)" class="ficon-red fa fa-trash-o fa-lg "   title="Hapus Bank" ></a>
					'
					,
                    $row['kode'],
                    $row['nama'],
                    $row['no_rek'],
                    $row['kode_apbd_rincian'],
                    $row['nama_apbd_rincian']
                    )
            );
        }

    	if($this->vars['isprint']){
			$this->vars['data']=$data;
		}else{
      	  echo json_encode($data);
		  die();
      	}    
        
    }
	 function form() {
       	$id = $_REQUEST['id'];
		if ($id) {
           $tbl = 'bank b,apbd_rincian r'; // nama tabel
            $sel = 'b.*,
                    r.nama nama_apbd_rincian,r.kode kode_apbd_rincian
                    '; // default * 
            $where = "b.id_apbd_rincian=r.id and b.id='$id'"; // jika where kosong isi dengan 1
        	$this->vars['detail'] = $this->crud->get_single_data($tbl, $where,$sel);
		}
	    $this->vars['id'] = $id;
		$this->loadView($this->page, $this->method, $this->vars, true);
    }
	
	 function simpan() {
 		$id=$_REQUEST['id'];
       
        if ($id) { // update
            $apbd_akun = $this->crud->update($rows, 'bank', true,"","id");
        } else { // create
         	$cek= $this->crud->get_single_data('bank', "(kode='$_REQUEST[kode]' or nama='$_REQUEST[nama]') ");
         	if(is_array($cek)){
         		echo "failed";
         		die();
         	}
		    $apbd_akun = $this->crud->create($rows, 'bank', true);
        }
    }

    function hapus() {
        $id = $_REQUEST['id'];
		$this->crud->delete_data("bank","id='$id'");
	}
	
	
}

?>
