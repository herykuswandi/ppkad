<?php
class Program extends Yaridhane {

    private $page;
    private $method;
    private $vars = array();

    function __construct($page, $method) {
        $this->page = $page;
        $this->method = $method;
        parent::__construct();
    }
	
	
	// start keselarasan
    function __default() {
	
        $this->vars['title'] = 'Program Kegiatan';
		/*  // urusan
        $flex_config = array(
            fields => array(
                array(display => 'Opsi', name => 'Option', width => '70', align => 'center'),
                array(display => 'Kode', name => 'u.kode', align => 'left', width => '60', sortable => true, filter => true), 
                array(display => 'Nama', name => 'u.nama', align => 'left', width => '200', sortable => true, filter => true), 
                ),
            url => '?act=organisasi&do=grid_urusan',
            showpage => '10',
            isprint=>$this->vars['isprint']
        );
        $grid = $this->render_grid($flex_config,'tabel-urusan');
        
        // bidang
        $flex_config = array(
            fields => array(
                array(display => 'Opsi', name => 'Option', width => '70', align => 'center'),
                array(display => 'Kode', name => 'b.kode', align => 'left', width => '60', sortable => true, filter => true), 
                array(display => 'Nama', name => 'b.nama', align => 'left', width => '200', sortable => true, filter => true), 
                array(display => 'Urusan', name => 'u.nama', align => 'left', width => '200', sortable => true, filter => true), 
                ),
            url => '?act=organisasi&do=grid_bidang',
            showpage => '10',
            isprint=>$this->vars['isprint']
        );
        $grid .= $this->render_grid($flex_config,'tabel-bidang');
        */
        //  program
        $flex_config = array(
            fields => array(
                array(display => 'Opsi', name => 'Option', width => '70', align => 'center'),
                array(display => 'Kode', name => 't.kode', align => 'left', width => '60', sortable => true, filter => true), 
                array(display => 'Nama', name => 't.nama', align => 'left', width => '200', sortable => true, filter => true), 
                array(display => 'Urusan', name => 'u.nama', align => 'left', width => '200', sortable => true, filter => true), 
                array(display => 'Bidang', name => 'b.nama', align => 'left', width => '200', sortable => true, filter => true) 
                ),
            url => '?act=program&do=grid_program',
            showpage => '10',
			isprint=>$this->vars['isprint']
        );
        $grid .= $this->render_grid($flex_config,'tabel-program');

        // Kegiatan
        $flex_config = array(
            fields => array(
                array(display => 'Opsi', name => 'Option', width => '70', align => 'center'),
                array(display => 'Kode', name => 's.kode', align => 'left', width => '70', sortable => true, filter => true), 
                array(display => 'Nama', name => 's.nama', align => 'left', width => '200', sortable => true, filter => true), 
                array(display => 'Urusan', name => 'u.nama', align => 'left', width => '200', sortable => true, filter => true), 
                array(display => 'Bidang', name => 'b.nama', align => 'left', width => '200', sortable => true, filter => true), 
                array(display => 'Program', name => 't.nama', align => 'left', width => '200', sortable => true, filter => true) 
                ),
            url => '?act=program&do=grid_kegiatan',
            showpage => '10',
			isprint=>$this->vars['isprint']
        );
        $grid .= $this->render_grid($flex_config,'tabel-kegiatan');
        
        $this->vars['grid'] = $grid;
        $this->loadView($this->page, $this->method, $this->vars,true);
    }


// Program
	    function grid_program() {
        $tbl = 'program t, bidang b,urusan u'; // nama tabel
        $sel = 't.*,b.nama nama_bidang,u.nama nama_urusan'; // default * 
        $where = "t.id_bidang=b.id and u.id=b.id_urusan"; // jika where kosong isi dengan 1
        $order='t.kode';
        $direction='asc';
        $hasil = $this->crud->get_data_flexi($tbl, $where, $sel, $order,$direction);
		
        while ($row = mysql_fetch_assoc($hasil['exe'])) {
            $data['page'] = $hasil['page'];
            $data['total'] = $hasil['total'];
            $data['rows'][] = array(
                'id' => $row['id'],
                'cell' => array(
                    '<a href="?act=program&do=form_program&id=' . $row['id'] . '" onClick="return modaler(this);"  class="ficon-green fa fa-pencil-square-o fa-lg linkbox"  label="Ubah Data Program"  title="Ubah Program"> </a> 
					<a href="javascript:void(0)" onClick="del_data(\'?act=program&do=hapus_program&id=' . $row['id'] . '\',this,\'tabel-program\')" class="ficon-red fa fa-trash-o fa-lg temp-delete"   title="Hapus Program" ></a>
					'
					,
                    $row['kode'],
                    $row['nama'],
                    $row['nama_urusan'],
                    $row['nama_bidang']
                    )
            );
        }
     	  echo json_encode($data);
		  die();
        
    }

	 function form_program() {
       	$id = $_REQUEST['id'];
		if ($id) {
        	$this->vars['detail'] = $this->crud->get_single_data('program t, bidang b, urusan u', "t.id='$id' and t.id_bidang=b.id and b.id_urusan=u.id","t.*,u.kode kode_urusan,u.nama nama_urusan,b.kode kode_bidang,b.nama nama_bidang");
		}
	    $this->vars['id'] = $id;
		$this->loadView($this->page, $this->method, $this->vars, true);
    }
	
	 function simpan_program() {
 		$id=$_REQUEST['id'];
 		$kode=$_REQUEST['kode_bidang'].'.'.$_REQUEST['kode_program'];
 		$rows=array('kode'=>$kode);
        if ($id) { // update
            $akun = $this->crud->update($rows, 'program', true,"","id");
        } else { // create
         	$cek= $this->crud->get_single_data('program', "kode='$kode'");
         	if(is_array($cek)){
         		echo "failed";
         		die();
         	}
		    $akun = $this->crud->create($rows, 'program', true);
        }
    }
    function hapus_program() {
        $id = $_REQUEST['id'];
		$this->crud->delete_data("program","id='$id'");
	}
	

//  kegiatan
	    function grid_kegiatan() {
        $tbl = 'kegiatan s, program t, bidang b,urusan u'; // nama tabel
        $sel = 's.*,t.nama nama_program,b.nama nama_bidang,u.nama nama_urusan'; // default * 
        $where = "s.id_program=t.id and t.id_bidang=b.id and u.id=b.id_urusan"; // jika where kosong isi dengan 1
        $order='s.kode';
        $direction='asc';
        $hasil = $this->crud->get_data_flexi($tbl, $where, $sel, $order,$direction);
		
        while ($row = mysql_fetch_assoc($hasil['exe'])) {
            $data['page'] = $hasil['page'];
            $data['total'] = $hasil['total'];
            $data['rows'][] = array(
                'id' => $row['id'],
                'cell' => array(
                    '<a href="?act=program&do=form_kegiatan&id=' . $row['id'] . '" onClick="return modaler(this);"  class="ficon-green fa fa-pencil-square-o fa-lg linkbox"  label="Ubah Data Kegiatan"  title="Ubah Kegiatan"> </a> 
					<a href="javascript:void(0)" onClick="del_data(\'?act=program&do=hapus_kegiatan&id=' . $row['id'] . '\',this,\'tabel-kegiatan\')" class="ficon-red fa fa-trash-o fa-lg temp-delete"   title="Hapus Kegiatan" ></a>
					'
					,
                    $row['kode'],
                    $row['nama'],
                    $row['nama_urusan'],
                    $row['nama_bidang'],
                    $row['nama_program']
                    )
            );
        }
     	  echo json_encode($data);
		  die();
        
    }

	 function form_kegiatan() {
       	$id = $_REQUEST['id'];
		if ($id) {
        	$this->vars['detail'] = $this->crud->get_single_data('kegiatan s, program t, bidang b, urusan u', "s.id='$id' and s.id_program=t.id and t.id_bidang=b.id and b.id_urusan=u.id","s.*,u.kode kode_urusan,u.nama nama_urusan,b.kode kode_bidang,b.nama nama_bidang,t.kode kode_program,t.nama nama_program");
		}
	    $this->vars['id'] = $id;
		$this->loadView($this->page, $this->method, $this->vars, true);
    }
	
	 function simpan_kegiatan() {
 		$id=$_REQUEST['id'];
 		$kode=$_REQUEST['kode_program'].'.'.$_REQUEST['kode_kegiatan'];
 		$rows=array('kode'=>$kode);
        if ($id) { // update
            $akun = $this->crud->update($rows, 'kegiatan', true,"","id");
        } else { // create
         	$cek= $this->crud->get_single_data('kegiatan', "kode='$kode'");
         	if(is_array($cek)){
         		echo "failed";
         		die();
         	}
		    $akun = $this->crud->create($rows, 'kegiatan', true);
        }
    }

    function hapus_kegiatan() {
        $id = $_REQUEST['id'];
		$this->crud->delete_data("kegiatan","id='$id'");
	}
	


}

?>
