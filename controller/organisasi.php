<?php
class Organisasi extends Yaridhane {

    private $page;
    private $method;
    private $vars = array();

    function __construct($page, $method) {
        $this->page = $page;
        $this->method = $method;
        parent::__construct();
    }
	
	
	// start keselarasan
    function __default() {
	
        $this->vars['title'] = 'Unit Organisasi';
		
		// urusan
        $flex_config = array(
            fields => array(
                array(display => 'Opsi', name => 'Option', width => '70', align => 'center'),
                array(display => 'Kode', name => 'u.kode', align => 'left', width => '60', sortable => true, filter => true), 
                array(display => 'Nama', name => 'u.nama', align => 'left', width => '200', sortable => true, filter => true), 
                ),
            url => '?act=organisasi&do=grid_urusan',
            showpage => '10',
			isprint=>$this->vars['isprint']
        );
        $grid = $this->render_grid($flex_config,'tabel-urusan');
        
        // bidang
        $flex_config = array(
            fields => array(
                array(display => 'Opsi', name => 'Option', width => '70', align => 'center'),
                array(display => 'Kode', name => 'b.kode', align => 'left', width => '60', sortable => true, filter => true), 
                array(display => 'Nama', name => 'b.nama', align => 'left', width => '200', sortable => true, filter => true), 
                array(display => 'Urusan', name => 'u.nama', align => 'left', width => '200', sortable => true, filter => true), 
                ),
            url => '?act=organisasi&do=grid_bidang',
            showpage => '10',
			isprint=>$this->vars['isprint']
        );
        $grid .= $this->render_grid($flex_config,'tabel-bidang');
        
        // unit
        $flex_config = array(
            fields => array(
                array(display => 'Opsi', name => 'Option', width => '70', align => 'center'),
                array(display => 'Kode', name => 't.kode', align => 'left', width => '60', sortable => true, filter => true), 
                array(display => 'Nama', name => 't.nama', align => 'left', width => '200', sortable => true, filter => true), 
                array(display => 'Urusan', name => 'u.nama', align => 'left', width => '200', sortable => true, filter => true), 
                array(display => 'Bidang', name => 'b.nama', align => 'left', width => '200', sortable => true, filter => true) 
                ),
            url => '?act=organisasi&do=grid_unit',
            showpage => '10',
			isprint=>$this->vars['isprint']
        );
        $grid .= $this->render_grid($flex_config,'tabel-unit');

        // sub unit
        $flex_config = array(
            fields => array(
                array(display => 'Opsi', name => 'Option', width => '70', align => 'center'),
                array(display => 'Kode', name => 's.kode', align => 'left', width => '70', sortable => true, filter => true), 
                array(display => 'Nama', name => 's.nama', align => 'left', width => '200', sortable => true, filter => true), 
                array(display => 'Urusan', name => 'u.nama', align => 'left', width => '200', sortable => true, filter => true), 
                array(display => 'Bidang', name => 'b.nama', align => 'left', width => '200', sortable => true, filter => true), 
                array(display => 'Unit', name => 't.nama', align => 'left', width => '200', sortable => true, filter => true) 
                ),
            url => '?act=organisasi&do=grid_sub_unit',
            showpage => '10',
			isprint=>$this->vars['isprint']
        );
        $grid .= $this->render_grid($flex_config,'tabel-sub-unit');
        
        $this->vars['grid'] = $grid;
        $this->loadView($this->page, $this->method, $this->vars,true);
    }

    function grid_urusan() {
        $tbl = 'urusan u'; // nama tabel
        $sel = '*'; // default * 
        $where = "1"; // jika where kosong isi dengan 1
        $order='u.kode';
        $direction='asc';
        $hasil = $this->crud->get_data_flexi($tbl, $where, $sel, $order,$direction);
		
        while ($row = mysql_fetch_assoc($hasil['exe'])) {
            $data['page'] = $hasil['page'];
            $data['total'] = $hasil['total'];
            $data['rows'][] = array(
                'id' => $row['id'],
                'cell' => array(
                    '<a href="?act=organisasi&do=form_urusan&id=' . $row['id'] . '" onClick="return modaler(this);"  class="ficon-green fa fa-pencil-square-o fa-lg linkbox"  label="Ubah Data Urusan"  title="Ubah Urusan"> </a> 
					<a href="javascript:void(0)" onClick="del_data(\'?act=organisasi&do=hapus_urusan&id=' . $row['id'] . '\',this,\'tabel-urusan\')" class="ficon-red fa fa-trash-o fa-lg temp-delete"   title="Hapus Urusan" ></a>
					'
					,
                    str_replace('.00', '',$row['kode']),
                    $row['nama']
                    )
            );
        }
     	  echo json_encode($data);
		  die();
        
    }

	 function form_urusan() {
       	$id = $_REQUEST['id'];
		if ($id) {
        	$this->vars['detail'] = $this->crud->get_single_data('urusan', "id='$id'");
		}
	    $this->vars['id'] = $id;
		$this->loadView($this->page, $this->method, $this->vars, true);
    }
	
	 function simpan_urusan() {
 		$id=$_REQUEST['id'];
        if ($id) { // update
            $akun = $this->crud->update($rows, 'urusan', true,"","id");
        } else { // create
         	$cek= $this->crud->get_single_data('urusan', "kode='$_REQUEST[kode]'");
         	if(is_array($cek)){
         		echo "failed";
         		die();
         	}
		    $akun = $this->crud->create($rows, 'urusan', true);
        }
    }

    function hapus_urusan() {
        $id = $_REQUEST['id'];
		$this->crud->delete_data("urusan","id='$id'");
	}
	
// bidang
	    function grid_bidang() {
        $tbl = 'bidang b,urusan u'; // nama tabel
        $sel = 'b.*,u.nama nama_urusan'; // default * 
        $where = "u.id=b.id_urusan"; // jika where kosong isi dengan 1
        $order='b.kode';
        $direction='asc';
        $hasil = $this->crud->get_data_flexi($tbl, $where, $sel, $order,$direction);
		
        while ($row = mysql_fetch_assoc($hasil['exe'])) {
            $data['page'] = $hasil['page'];
            $data['total'] = $hasil['total'];
            $data['rows'][] = array(
                'id' => $row['id'],
                'cell' => array(
                    '<a href="?act=organisasi&do=form_bidang&id=' . $row['id'] . '" onClick="return modaler(this);"  class="ficon-green fa fa-pencil-square-o fa-lg linkbox"  label="Ubah Data Bidang"  title="Ubah Bidang"> </a> 
					<a href="javascript:void(0)" onClick="del_data(\'?act=organisasi&do=hapus_bidang&id=' . $row['id'] . '\',this,\'tabel-bidang\')" class="ficon-red fa fa-trash-o fa-lg temp-delete"   title="Hapus Bidang" ></a>
					'
					,
                    $row['kode'],
                    $row['nama'],
                    $row['nama_urusan']
                    )
            );
        }
     	  echo json_encode($data);
		  die();
        
    }

	 function form_bidang() {
       	$id = $_REQUEST['id'];
		if ($id) {
        	$this->vars['detail'] = $this->crud->get_single_data('bidang b, urusan u', "b.id='$id' and b.id_urusan=u.id","b.*,u.kode kode_urusan,u.nama nama_urusan");
		}
	    $this->vars['id'] = $id;
		$this->loadView($this->page, $this->method, $this->vars, true);
    }
	
	 function simpan_bidang() {
 		$id=$_REQUEST['id'];
 		$kode=$_REQUEST['kode_urusan'].'.'.$_REQUEST['kode_bidang'];
 		$rows=array('kode'=>$kode);
        if ($id) { // update
            $akun = $this->crud->update($rows, 'bidang', true,"","id");
        } else { // create
         	$cek= $this->crud->get_single_data('bidang', "kode='$kode'");
         	if(is_array($cek)){
         		echo "failed";
         		die();
         	}
		    $akun = $this->crud->create($rows, 'bidang', true);
        }
    }

    function hapus_bidang() {
        $id = $_REQUEST['id'];
		$this->crud->delete_data("bidang","id='$id'");
	}
	

// unit
	    function grid_unit() {
        $tbl = 'unit t, bidang b,urusan u'; // nama tabel
        $sel = 't.*,b.nama nama_bidang,u.nama nama_urusan'; // default * 
        $where = "t.id_bidang=b.id and u.id=b.id_urusan"; // jika where kosong isi dengan 1
        $order='t.kode';
        $direction='asc';
        $hasil = $this->crud->get_data_flexi($tbl, $where, $sel, $order,$direction);
		
        while ($row = mysql_fetch_assoc($hasil['exe'])) {
            $data['page'] = $hasil['page'];
            $data['total'] = $hasil['total'];
            $data['rows'][] = array(
                'id' => $row['id'],
                'cell' => array(
                    '<a href="?act=organisasi&do=form_unit&id=' . $row['id'] . '" onClick="return modaler(this);"  class="ficon-green fa fa-pencil-square-o fa-lg linkbox"  label="Ubah Data Unit"  title="Ubah Unit"> </a> 
					<a href="javascript:void(0)" onClick="del_data(\'?act=organisasi&do=hapus_unit&id=' . $row['id'] . '\',this,\'tabel-unit\')" class="ficon-red fa fa-trash-o fa-lg temp-delete"   title="Hapus Unit" ></a>
					'
					,
                    $row['kode'],
                    $row['nama'],
                    $row['nama_urusan'],
                    $row['nama_bidang']
                    )
            );
        }
     	  echo json_encode($data);
		  die();
        
    }

	 function form_unit() {
       	$id = $_REQUEST['id'];
		if ($id) {
        	$this->vars['detail'] = $this->crud->get_single_data('unit t, bidang b, urusan u', "t.id='$id' and t.id_bidang=b.id and b.id_urusan=u.id","t.*,u.kode kode_urusan,u.nama nama_urusan,b.kode kode_bidang,b.nama nama_bidang");
		}
	    $this->vars['id'] = $id;
		$this->loadView($this->page, $this->method, $this->vars, true);
    }
	
	 function simpan_unit() {
 		$id=$_REQUEST['id'];
 		$kode=$_REQUEST['kode_bidang'].'.'.$_REQUEST['kode_unit'];
 		$rows=array('kode'=>$kode);
        if ($id) { // update
            $akun = $this->crud->update($rows, 'unit', true,"","id");
        } else { // create
         	$cek= $this->crud->get_single_data('unit', "kode='$kode'");
         	if(is_array($cek)){
         		echo "failed";
         		die();
         	}
		    $akun = $this->crud->create($rows, 'unit', true);
        }
    }
    function hapus_unit() {
        $id = $_REQUEST['id'];
		$this->crud->delete_data("unit","id='$id'");
	}
	

// sub unit
	    function grid_sub_unit() {
        $tbl = 'sub_unit s, unit t, bidang b,urusan u'; // nama tabel
        $sel = 's.*,t.nama nama_unit,b.nama nama_bidang,u.nama nama_urusan'; // default * 
        $where = "s.id_unit=t.id and t.id_bidang=b.id and u.id=b.id_urusan"; // jika where kosong isi dengan 1
        $order='s.kode';
        $direction='asc';
        $hasil = $this->crud->get_data_flexi($tbl, $where, $sel, $order,$direction);
		
        while ($row = mysql_fetch_assoc($hasil['exe'])) {
            $data['page'] = $hasil['page'];
            $data['total'] = $hasil['total'];
            $data['rows'][] = array(
                'id' => $row['id'],
                'cell' => array(
                    '<a href="?act=organisasi&do=form_sub_unit&id=' . $row['id'] . '" onClick="return modaler(this);"  class="ficon-green fa fa-pencil-square-o fa-lg linkbox"  label="Ubah Data Sub Unit"  title="Ubah Sub Unit"> </a> 
					<a href="javascript:void(0)" onClick="del_data(\'?act=organisasi&do=hapus_sub_unit&id=' . $row['id'] . '\',this,\'tabel-sub-unit\')" class="ficon-red fa fa-trash-o fa-lg temp-delete"   title="Hapus Sub Unit" ></a>
					'
					,
                    $row['kode'],
                    $row['nama'],
                    $row['nama_urusan'],
                    $row['nama_bidang'],
                    $row['nama_unit']
                    )
            );
        }
     	  echo json_encode($data);
		  die();
        
    }

	 function form_sub_unit() {
       	$id = $_REQUEST['id'];
		if ($id) {
        	$this->vars['detail'] = $this->crud->get_single_data('sub_unit s, unit t, bidang b, urusan u', "s.id='$id' and s.id_unit=t.id and t.id_bidang=b.id and b.id_urusan=u.id","s.*,u.kode kode_urusan,u.nama nama_urusan,b.kode kode_bidang,b.nama nama_bidang,t.kode kode_unit,t.nama nama_unit");
		}
	    $this->vars['id'] = $id;
		$this->loadView($this->page, $this->method, $this->vars, true);
    }
	
	 function simpan_sub_unit() {
 		$id=$_REQUEST['id'];
 		$kode=$_REQUEST['kode_unit'].'.'.$_REQUEST['kode_sub_unit'];
 		$rows=array('kode'=>$kode);
        if ($id) { // update
            $akun = $this->crud->update($rows, 'sub_unit', true,"","id");
        } else { // create
         	$cek= $this->crud->get_single_data('sub_unit', "kode='$kode'");
         	if(is_array($cek)){
         		echo "failed";
         		die();
         	}
		    $akun = $this->crud->create($rows, 'sub_unit', true);
        }
    }

    function hapus_sub_unit() {
        $id = $_REQUEST['id'];
		$this->crud->delete_data("sub_unit","id='$id'");
	}
	


}

?>
