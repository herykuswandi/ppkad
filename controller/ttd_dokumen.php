<?php
class Ttd_dokumen extends Yaridhane {

    private $page;
    private $method;
    private $vars = array();

    function __construct($page, $method) {
        $this->page = $page;
        $this->method = $method;
        parent::__construct();
    }
	
	
	// start ttd_dokumen
    function __default() {
	
        $this->vars['title'] = 'Data Penanda Tangan Dokumen';
        $flex_config = array(
            fields => array(
                array(display => 'Opsi', name => 'Option', width => '70', align => 'center'),
                array(display => 'No. Urut', name => 'no_urut', align => 'left', width => '80', sortable => true, filter => true), 
                array(display => 'Nama', name => 't.nama', align => 'left', width => '150', sortable => true, filter => true), 
                array(display => 'NIP', name => 't.nip', align => 'left', width => '150', sortable => true, filter => true), 
                array(display => 'Jabatan', name => 't.jabatan', align => 'left', width => '150', sortable => true, filter => true), 
                array(display => 'Dokumen', name => 'j.nama', align => 'left', width => '150', sortable => true, filter => true), 
                ),
            url => '?act=ttd_dokumen&do=grid',
            showpage => '10',
			isprint=>$this->vars['isprint']
        );
        $grid = $this->render_grid($flex_config);
        $this->vars['grid'] = $grid;
        $this->loadView($this->page, $this->method, $this->vars,true);
    }

    function grid() {
        $tbl = 'ttd_dokumen t,jenis_dokumen j'; // nama tabel
        $sel = 't.*,j.nama nama_jenis'; // default * 
        $id_ta=get_ses('id_ta');
        
        $where = "j.id=t.id_jenis_dokumen and t.id_ta='$id_ta'"; // jika where kosong isi dengan 1
        $order='t.no_urut';
        $direction='asc';
        $hasil = $this->crud->get_data_flexi($tbl, $where, $sel, $order,$direction);
		while ($row = mysql_fetch_assoc($hasil['exe'])) {
            $data['page'] = $hasil['page'];
            $data['total'] = $hasil['total'];
            $data['rows'][] = array(
                'id' => $row['id'],
                'cell' => array(
                    '<a href="?act=ttd_dokumen&do=form&id=' . $row['id'] . '" onClick="return modaler(this);"  class="ficon-green fa fa-pencil-square-o fa-lg linkbox"  label="Ubah Penanda Tangan Dokumen"  title="Ubah Penanda Tangan Dokumen"> </a> 
					<a href="javascript:void(0)" onClick="del_data(\'?act=ttd_dokumen&do=hapus&id=' . $row['id'] . '\',this)" class="ficon-red fa fa-trash-o fa-lg "   title="Hapus Penanda Tangan Dokumen" ></a>
					'
					,
                    $row['no_urut'],
                    $row['nama'],
                    $row['nip'],
                    $row['jabatan'],
                    $row['nama_jenis']
                    )
            );
        }

    	if($this->vars['isprint']){
			$this->vars['data']=$data;
		}else{
      	  echo json_encode($data);
		  die();
      	}    
        
    }
	 function form() {
       	$id = $_REQUEST['id'];
		if ($id) {
        	$this->vars['detail'] = $this->crud->get_single_data('ttd_dokumen t,tahun_anggaran a', "t.id='$id' and t.id_ta=a.id","t.*,a.tahun");
		}else{
            $this->vars['detail']['id_ta']=get_ses('id_ta');
            $this->vars['detail']['tahun']=get_ses('ta');

        }
        $this->vars['jenis_dokumen']=$this->crud->get_data('jenis_dokumen');
	    $this->vars['id'] = $id;
		$this->loadView($this->page, $this->method, $this->vars, true);
    }
	
	 function simpan() {
 		$id=$_REQUEST['id'];
        if ($id) { // update
            $akun = $this->crud->update($rows, 'ttd_dokumen', true,"","id");
        } else { // create

            $id_ta=get_ses('id_ta');
         	$cek= $this->crud->get_single_data('ttd_dokumen', "id_ta='$id_ta' and (nama='$_REQUEST[nama]' or no_urut='$_REQUEST[no_urut]')");
         	if(is_array($cek)){
         		echo "failed";
         		die();
         	}
		    $akun = $this->crud->create($rows, 'ttd_dokumen', true);
        }
    }

    function hapus() {
        $id = $_REQUEST['id'];
		$this->crud->delete_data("ttd_dokumen","id='$id'");
	}
	
	
}

?>
