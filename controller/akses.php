<?php
class akses extends Yaridhane {

    private $page;
    private $method;
    private $vars = array();

    function __construct($page, $method) {
        $this->page = $page;
        $this->method = $method;
        parent::__construct();
    }
	
	
	// start akses
    function __default() {
	
        $this->vars['title'] = 'Data akses';
        $flex_config = array(
            fields => array(
                array(display => 'Opsi', name => 'Option', width => '70', align => 'center'),
                array(display => 'Nama', name => 'nama', align => 'left', width => '120', sortable => true, filter => true), 
                array(display => 'Keterangan', name => 'keterangan', align => 'left', width => '120', sortable => true, filter => true), 
                ),
            url => '?act=akses&do=grid',
            showpage => '10',
			isprint=>$this->vars['isprint']
        );
        $grid = $this->render_grid($flex_config);
        $this->vars['grid'] = $grid;
        $this->loadView($this->page, $this->method, $this->vars,true);
    }

    function grid() {
        $tbl = 'akses'; // nama tabel
        $sel = '*'; // default * 
        $where = "1"; // jika where kosong isi dengan 1
        $order='nama'; // order name
        $direction='asc';
        $hasil = $this->crud->get_data_flexi($tbl, $where, $sel, $order,$direction);
		
        while ($row = mysql_fetch_assoc($hasil['exe'])) {
            $data['page'] = $hasil['page'];
            $data['total'] = $hasil['total'];
            $data['rows'][] = array(
                'id' => $row['id'],
                'cell' => array(
                    '<a href="javascript:void(0)" onClick="ajaxload(\'?act=akses&do=form&id=' . $row['id'] . '\')"  class="ficon-green fa fa-pencil-square-o fa-lg linkbox"  label="Ubah Data akses"  title="Ubah akses"> </a> 
					<a href="javascript:void(0)" onClick="del_data(\'?act=akses&do=hapus&id=' . $row['id'] . '\',this)" class="ficon-red fa fa-trash-o fa-lg "   title="Hapus akses" ></a>
					',
                    $row['nama'],
                    $row['keterangan'],
                    )
            );
        }

    	if($this->vars['isprint']){
			$this->vars['data']=$data;
		}else{
      	  echo json_encode($data);
		  die();
      	}    
        
    }
	 function form() {
       	$id = $_REQUEST['id'];
		if ($id) {
        	$this->vars['detail'] = $this->crud->get_single_data('akses', "id='$id'");
		}
	    $this->vars['id'] = $id;
	    $tuh=array(
				't'=>array('label'=>'Tambah'),
				'u'=>array('label'=>'Ubah'),
				'h'=>array('label'=>'Hapus')
			);
	    $this->vars['hak']=array(
			'file'=>array(
				'label'=>"File",
				'child'=>array(
					'group'=>array(
						'label'=>"Data User Group",
						'child'=>$tuh
					),
					'user'=>array(
						'label'=>"Data User",
						'child'=>array(
							't'=>array('label'=>'Tambah'),
							'u'=>array('label'=>'Ubah'),
							'h'=>array('label'=>'Hapus')
						)
					)
				)
			),
			'setup'=>array(
				'label'=>"Setup",
				'child'=>array(
					'properties'=>array(
						'label'=>"Properties",
						'child'=>$tuh
					),
					'urusan'=>array(
						'label'=>"Tabel Urusan",
						'child'=>$tuh
					),
					'instansi'=>array(
						'label'=>"Tabel Instansi",
						'child'=>$tuh
					),
					'program'=>array(
						'label'=>"Tabel Program",
						'child'=>$tuh
					),
					'akun'=>array(
						'label'=>"Tabel Akun",
						'child'=>$tuh
					),
					'keselarasan'=>array(
						'label'=>"Tabel Keselarasan",
						'child'=>$tuh
					),
					'identitas'=>array(
						'label'=>"Identitas Kepala Kantor"
					)
				)
			),
			'input'=>array(
				'label'=>"Input",
				'child'=>array(
					'jp_kas'=>array(
						'label'=>"Input Jurnal Pendapatan Kas",
						'child'=>$tuh
					),
					'jb_kas'=>array(
						'label'=>"Input Jurnal Belanja Kas",
						'child'=>$tuh
					),
					'jp_bank'=>array(
						'label'=>"Input Jurnal Pendapatan Bank",
						'child'=>$tuh
					),
					'jb_bank'=>array(
						'label'=>"Input Jurnal Belanja Bank",
						'child'=>$tuh
					),
					'jurnal_korolari'=>array(
						'label'=>"Input Jurnal Korolari",
						'child'=>$tuh
					),
					'jurnal_umum'=>array(
						'label'=>"Input Jurnal Umum",
						'child'=>$tuh
					),
					'target_pad'=>array(
						'label'=>"Input Target PAD",
						'child'=>$tuh
					),
					'target_belanja'=>array(
						'label'=>"Input Target Belanja",
						'child'=>$tuh
					),
					'jurnal_neraca'=>array(
						'label'=>"Input Jurnal Neraca",
						'child'=>$tuh
					)
				)
			),
			'laporan'=>array(
				'label'=>"Laporan",
				'child'=>array(
					'jurnal'=>array(
						'label'=>"Jurnal",
						'child'=>array(
									'jpk'=>array('label'=>'Jurnal Pendapatan Kas'),
									'jpb'=>array('label'=>'Jurnal Pendapatan Bank'),
									'jbk'=>array('label'=>'Jurnal Belanja Kas'),
									'jbb'=>array('label'=>'Jurnal Belanja Bank'),
									'jkorolari'=>array('label'=>'Jurnal Korolari'),
									'jumum'=>array('label'=>'Jurnal Umum'),
									'jkorolari'=>array('label'=>'Jurnal Korolari/Neraca'),
									'pendapatan'=>array('label'=>'Pendapatan'),
									'belanja'=>array('label'=>'Belanja')
								)
					),
					'pad'=>array(
						'label'=>"Jurnal",
						'child'=>array(
									'perdinas'=>array('label'=>'Per Dinas'),
									'semua'=>array('label'=>'Semua Dinas'),
									'harian'=>array('label'=>'Pendapatan Harian')
								)
					),
					'belanja'=>array(
						'label'=>"Belanja",
						'child'=>array(
									'perdinas'=>array('label'=>'Per Dinas'),
									'semua'=>array('label'=>'Semua Dinas')
								)
					),
					'kas_umum'=>array(
						'label'=>"Buku Kas Umum",
						'child'=>array(
									'kas_umum'=>array('label'=>'Buku Kas Umum'),
									'buku_kas'=>array('label'=>'Buku Kas'),
									'buku_kas_sub'=>array('label'=>'Buku Kas & Bank Per Sub'),
									'buku_kas_dinkes_rsud'=>array('label'=>'Buku Kas Dinkes & RSUD'),
									'buku_kas_dinkes'=>array('label'=>'Buku Kas Dinkes'),
									'buku_kas_rsud'=>array('label'=>'Buku Kas RSUD'),
									'buku_kas_bank'=>array('label'=>'Buku Kas Bank'),
									'buku_kas_bendahara'=>array('label'=>'Buku Kas Di Bendahara Pengeluaran')
								)
					),
					'buku_besar'=>array(
						'label'=>"Buku Besar",
						'child'=>array(
									'perdinas'=>array('label'=>'Per Dinas'),
									'semua'=>array('label'=>'Semua Dinas'),
									'skpd'=>array('label'=>'RK-SKPD')
								)
					),
					'neraca_saldo'=>array(
						'label'=>"Laporan Neraca Saldo Sebelum Penyesuaian",
						'child'=>array(
									'perdinas'=>array('label'=>'Per Dinas'),
									'semua'=>array('label'=>'Semua Dinas')
								)
					),
					'realisasi_anggaran_1'=>array(
						'label'=>"Laporan Realisasi Anggaran",
						'child'=>array(
									'lra'=>array('label'=>'LRA'),
									'lra_periode'=>array('label'=>'LRA Periode'),
									'lra_penjabaran'=>array('label'=>'LRA Penjabaran'),
									'3_digit'=>array('label'=>'Semua Dinas 3 Digit'),
									'5_digit'=>array('label'=>'Semua Dinas 5 Digit')
								)
					),
					'realisasi_anggaran_2'=>array(
						'label'=>"Laporan Realisasi Anggaran",
						'child'=>array(
									'lra_perda'=>array('label'=>'LRA Perda'),
									'lra_penjabaran'=>array('label'=>'LRA Penjabaran')
									
								)
					),
					'neraca_penyesuaian'=>array(
						'label'=>"Laporan Neraca Setelah Penyesuaian",
						'child'=>array(
									'perdinas'=>array('label'=>'Per Dinas'),
									'semua'=>array('label'=>'Semua Dinas'),
									'skpd'=>array('label'=>'Laporan Neraca R/K SKPD')
									
								)
					),
					'neraca'=>array(
						'label'=>"Neraca",
						'child'=>array(
									'dinas'=>array('label'=>'Neraca Dinas'),
									'pemda'=>array('label'=>'Neraca Pemda')
									
								)
					),
					'arus_kas'=>array(
						'label'=>"Laporan Arus Kas"
					),
					'rekap_urusan14'=>array(
						'label'=>"Rekapitulasi LRA Per Urusan (Lampiran 1.4)",
						'child'=>array(
									
									'perdinas'=>array('label'=>'Per Dinas'),
									'semua'=>array('label'=>'Semua Dinas'),
						)
					),
					'rekap_urusan13'=>array(
						'label'=>"Rekapitulasi LRA Per Urusan (Lampiran 1.3)",
						'child'=>array(
									
									'perdinas'=>array('label'=>'Per Dinas'),
									'semua'=>array('label'=>'Semua Dinas'),
						)
					),
					'rekap_urusan11'=>array(
						'label'=>"Rekapitulasi LRA Per Dinas (Lampiran 1.1)"
					),
					'rekap_rekening'=>array(
						'label'=>"Laporan Rekap Kode Rekening",
						'child'=>array(
									'lima'=>array('label'=>'Lima (5) Digit')
						)
					),
					'penjabaran_prognosis'=>array(
						'label'=>"Laporan Penjabaran Prognosis"
					),
					'penjabaran_komandan'=>array(
						'label'=>"Laporan Penjabaran CSV Komandan"
					),
					'penjabaran_komandan12'=>array(
						'label'=>"Laporan CFV Komandan 12"
					),
					'abd3'=>array(
						'label'=>"Tabel Anggaran Belanja Daerah 3 digit akun"
					),
					'abdp'=>array(
						'label'=>"Tabel Anggaran Belanja Daerah Per Dinas"
					),
					'abap'=>array(
						'label'=>"Tabel Anggaran Belanja Akumulasi Per Dinas"
					),
					'seb'=>array(
						'label'=>"Setting Baris"
					)
				)
				
			),'utilitas'=>array(
					'label'=>"Utilitas",
					'child'=>array(
						'perbaikan'=>array(
							'label'=>"perbaikan"
						),
						'buku_besar'=>array(
							'label'=>"Perbaikan Buku Besar Satu Tahun"
						),
						'perbaikan_anggaran'=>array(
							'label'=>"Perbaikan Anggaran"
						),
						'neraca'=>array(
							'label'=>"Proses Neraca"
						),
						'bku_rekening'=>array(
							'label'=>"Proses BKU Rekening Lengkap"
						),
						'realisasi'=>array(
							'label'=>"Proses Realisasi Anggaran"
						),
						'buku_triwulan_3'=>array(
							'label'=>"Proses Buku Besar Triwulan 3 (tiga) digit",
							'child'=>array(
										'triwulan1'=>array('label'=>'Proses Buku Besar Triwulan I'),
										'triwulan2'=>array('label'=>'Proses Buku Besar Triwulan II'),
										'triwulan3'=>array('label'=>'Proses Buku Besar Triwulan III'),
										'triwulan4'=>array('label'=>'Proses Buku Besar Triwulan IV'),
									)
						),
						'buku_triwulan_5'=>array(
							'label'=>"Proses Buku Besar Triwulan 5 (lima) digit",
							'child'=>array(
										'triwulan1'=>array('label'=>'Proses Buku Besar Triwulan I'),
										'triwulan2'=>array('label'=>'Proses Buku Besar Triwulan II'),
										'triwulan4'=>array('label'=>'Proses Buku Besar Triwulan IV'),
									)
						),
						'proses_neraca'=>array(
							'label'=>"Proses Neraca Saldo & Realisasi Anggaran"
						)
					)
				)
		);
		$data=$this->vars['detail']['akses'];
		$this->vars['arrdat']=json_decode($data,true);

		$this->loadView($this->page, $this->method, $this->vars, true);
    }
	
	 function simpan() {
 		$id=$_REQUEST['id'];
 		$rows=array('akses'=>json_encode($_REQUEST['hak']));
        if ($id) { // update
            $akun = $this->crud->update($rows, 'akses', true,"","id");
        } else { // create
         	$cek= $this->crud->get_single_data('akses', "nama='$_REQUEST[nama]'");
         	if(is_array($cek)){
         		echo "failed";
         		die();
         	}
		    $akun = $this->crud->create($rows, 'akses', true);
        }
    }

    function hapus() {
        $id = $_REQUEST['id'];
		$this->crud->delete_data("akses","id='$id'");
	}
	
	

    function kartu() {
		$this->vars['isprint']=true;
		$this->grid();
		$this->render_print($this->vars,$this->page,$this->method );
		
	}

	// end data akses
}

?>
