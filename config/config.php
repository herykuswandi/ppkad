<?php
	@ini_set('magic_quotes_gpc', false);
	@ini_set('magic_quotes_runtime', false);
	@ini_set('magic_quotes_sybase', false);
	@ini_set('display_errors', true);
	error_reporting(~E_NOTICE);
	@date_default_timezone_set('Asia/Jakarta');
	
	$_CONFIG['db'] = array(
		'host' => 'localhost',
		'user' => 'root',
		'pass' => '',
		'db' => 'naratel_ppkad',
	);
?>
