-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Inang: 127.0.0.1
-- Waktu pembuatan: 05 Sep 2014 pada 12.25
-- Versi Server: 5.5.27
-- Versi PHP: 5.4.7

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Basis data: `naratel_ppkad`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `akses`
--

CREATE TABLE IF NOT EXISTS `akses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(50) NOT NULL,
  `keterangan` varchar(100) NOT NULL,
  `akses` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data untuk tabel `akses`
--

INSERT INTO `akses` (`id`, `nama`, `keterangan`, `akses`) VALUES
(1, 'Admin', 'administrator', '{"file":{"group":{"t":"1","u":"1","h":"1"},"user":{"t":"1","u":"1","h":"1"}},"setup":{"properties":{"h":"1"},"urusan":{"t":"1","u":"1","h":"1"},"instansi":{"t":"1","u":"1","h":"1"},"program":{"t":"1","u":"1","h":"1"},"akun":{"t":"1","u":"1","h":"1"},"keselarasan":{"t":"1","u":"1","h":"1"},"identitas":"1"},"input":{"jp_kas":{"t":"1","u":"1","h":"1"},"jb_kas":{"t":"1","u":"1","h":"1"},"jp_bank":{"t":"1","u":"1","h":"1"},"jb_bank":{"t":"1","u":"1","h":"1"},"jurnal_korolari":{"t":"1","u":"1","h":"1"},"jurnal_umum":{"t":"1","u":"1","h":"1"},"target_pad":{"t":"1","u":"1","h":"1"},"target_belanja":{"t":"1","u":"1","h":"1"},"jurnal_neraca":{"t":"1","u":"1","h":"1"}},"laporan":{"jurnal":{"jpk":"1","jpb":"1","jbk":"1","jbb":"1","jkorolari":"1","jumum":"1","pendapatan":"1","belanja":"1"},"pad":{"perdinas":"1","semua":"1","harian":"1"},"belanja":{"perdinas":"1","semua":"1"},"kas_umum":{"kas_umum":"1","buku_kas":"1","buku_kas_sub":"1","buku_kas_dinkes_rsud":"1","buku_kas_dinkes":"1","buku_kas_rsud":"1","buku_kas_bank":"1","buku_kas_bendahara":"1"},"buku_besar":{"perdinas":"1","semua":"1","skpd":"1"},"neraca_saldo":{"perdinas":"1","semua":"1"},"realisasi_anggaran_1":{"lra":"1","lra_periode":"1","lra_penjabaran":"1","3_digit":"1","5_digit":"1"},"realisasi_anggaran_2":{"lra_perda":"1","lra_penjabaran":"1"},"neraca_penyesuaian":{"perdinas":"1","semua":"1","skpd":"1"},"neraca":{"dinas":"1","pemda":"1"},"arus_kas":"1","rekap_urusan14":{"perdinas":"1","semua":"1"},"rekap_urusan13":{"perdinas":"1","semua":"1"},"rekap_urusan11":"1","rekap_rekening":{"lima":"1"},"penjabaran_prognosis":"1","penjabaran_komandan":"1","penjabaran_komandan12":"1","abd3":"1","abdp":"1","abap":"1","seb":"1"},"utilitas":{"perbaikan":"1","buku_besar":"1","perbaikan_anggaran":"1","neraca":"1","bku_rekening":"1","realisasi":"1","buku_triwulan_3":{"triwulan1":"1","triwulan2":"1","triwulan3":"1","triwulan4":"1"},"buku_triwulan_5":{"triwulan1":"1","triwulan2":"1","triwulan4":"1"},"proses_neraca":"1"}}');

-- --------------------------------------------------------

--
-- Struktur dari tabel `apbd_akun`
--

CREATE TABLE IF NOT EXISTS `apbd_akun` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kode` varchar(30) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `saldo_normal` varchar(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data untuk tabel `apbd_akun`
--

INSERT INTO `apbd_akun` (`id`, `kode`, `nama`, `saldo_normal`) VALUES
(1, '1', 'Akun satu debet', 'debet'),
(2, '2', 'akun dua', 'kredit');

-- --------------------------------------------------------

--
-- Struktur dari tabel `apbd_jenis`
--

CREATE TABLE IF NOT EXISTS `apbd_jenis` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kode` varchar(30) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `id_apbd_kelompok` int(11) NOT NULL,
  `saldo_normal` varchar(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data untuk tabel `apbd_jenis`
--

INSERT INTO `apbd_jenis` (`id`, `kode`, `nama`, `id_apbd_kelompok`, `saldo_normal`) VALUES
(1, '1.20.02', 'jenis dari kelompok 2', 2, 'debet'),
(2, '1.01.21', 'jenis 21', 1, 'kredit');

-- --------------------------------------------------------

--
-- Struktur dari tabel `apbd_kelompok`
--

CREATE TABLE IF NOT EXISTS `apbd_kelompok` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kode` varchar(30) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `id_apbd_akun` int(11) NOT NULL,
  `saldo_normal` varchar(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data untuk tabel `apbd_kelompok`
--

INSERT INTO `apbd_kelompok` (`id`, `kode`, `nama`, `id_apbd_akun`, `saldo_normal`) VALUES
(1, '1.01', 'Kelompok 1 debet', 1, 'debet'),
(2, '1.20', 'kelompok dua', 1, 'kredit');

-- --------------------------------------------------------

--
-- Struktur dari tabel `apbd_obyek`
--

CREATE TABLE IF NOT EXISTS `apbd_obyek` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kode` varchar(30) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `id_apbd_jenis` int(11) NOT NULL,
  `saldo_normal` varchar(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data untuk tabel `apbd_obyek`
--

INSERT INTO `apbd_obyek` (`id`, `kode`, `nama`, `id_apbd_jenis`, `saldo_normal`) VALUES
(1, '1.01.21.25', 'obyek  25', 2, 'debet');

-- --------------------------------------------------------

--
-- Struktur dari tabel `apbd_rincian`
--

CREATE TABLE IF NOT EXISTS `apbd_rincian` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kode` varchar(30) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `id_apbd_obyek` int(11) NOT NULL,
  `saldo_normal` varchar(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data untuk tabel `apbd_rincian`
--

INSERT INTO `apbd_rincian` (`id`, `kode`, `nama`, `id_apbd_obyek`, `saldo_normal`) VALUES
(1, '1.01.21.25.30', 'rincian 30', 1, 'debet'),
(2, '1.01.21.25.12', 'Rincian 12', 1, 'kredit');

-- --------------------------------------------------------

--
-- Struktur dari tabel `bank`
--

CREATE TABLE IF NOT EXISTS `bank` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kode` varchar(20) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `no_rek` varchar(50) NOT NULL,
  `id_apbd_rincian` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data untuk tabel `bank`
--

INSERT INTO `bank` (`id`, `kode`, `nama`, `no_rek`, `id_apbd_rincian`) VALUES
(1, '12', 'bank mandiri suhat', '39340002', 1),
(5, '22', 'Bank BCA Wagir', '202992', 2);

-- --------------------------------------------------------

--
-- Struktur dari tabel `bidang`
--

CREATE TABLE IF NOT EXISTS `bidang` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kode` varchar(50) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `id_urusan` int(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data untuk tabel `bidang`
--

INSERT INTO `bidang` (`id`, `kode`, `nama`, `id_urusan`) VALUES
(1, '1.01', 'Bidang Satu', 1),
(2, '2.02', 'Bidang dua', 2);

-- --------------------------------------------------------

--
-- Struktur dari tabel `data_umum`
--

CREATE TABLE IF NOT EXISTS `data_umum` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama_pemda` varchar(50) NOT NULL,
  `ibukota_pemda` varchar(50) NOT NULL,
  `alamat_pemda` varchar(100) NOT NULL,
  `nama_kepda` varchar(50) NOT NULL,
  `nip_kepda` varchar(50) NOT NULL,
  `jabatan_kepda` varchar(50) NOT NULL,
  `nama_sekda` varchar(50) NOT NULL,
  `nip_sekda` varchar(50) NOT NULL,
  `jabatan_sekda` varchar(50) NOT NULL,
  `nama_kepkeu` varchar(50) NOT NULL,
  `nip_kepkeu` varchar(50) NOT NULL,
  `jabatan_kepkeu` varchar(50) NOT NULL,
  `nama_kepang` varchar(50) NOT NULL,
  `nip_kepang` varchar(50) NOT NULL,
  `jabatan_kepang` varchar(50) NOT NULL,
  `nama_kepver` varchar(50) NOT NULL,
  `nip_kepver` varchar(50) NOT NULL,
  `jabatan_kepver` varchar(50) NOT NULL,
  `nama_kepben` varchar(50) NOT NULL,
  `nip_kepben` varchar(50) NOT NULL,
  `jabatan_kepben` varchar(50) NOT NULL,
  `nama_kepbuk` varchar(50) NOT NULL,
  `nip_kepbuk` varchar(50) NOT NULL,
  `jabatan_kepbuk` varchar(50) NOT NULL,
  `nama_kepbud` varchar(50) NOT NULL,
  `nip_kepbud` varchar(50) NOT NULL,
  `jabatan_kepbud` varchar(50) NOT NULL,
  `nama_atbud` varchar(50) NOT NULL,
  `nip_atbud` varchar(50) NOT NULL,
  `jabatan_atbud` varchar(50) NOT NULL,
  `logo` text NOT NULL,
  `id_ta` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data untuk tabel `data_umum`
--

INSERT INTO `data_umum` (`id`, `nama_pemda`, `ibukota_pemda`, `alamat_pemda`, `nama_kepda`, `nip_kepda`, `jabatan_kepda`, `nama_sekda`, `nip_sekda`, `jabatan_sekda`, `nama_kepkeu`, `nip_kepkeu`, `jabatan_kepkeu`, `nama_kepang`, `nip_kepang`, `jabatan_kepang`, `nama_kepver`, `nip_kepver`, `jabatan_kepver`, `nama_kepben`, `nip_kepben`, `jabatan_kepben`, `nama_kepbuk`, `nip_kepbuk`, `jabatan_kepbuk`, `nama_kepbud`, `nip_kepbud`, `jabatan_kepbud`, `nama_atbud`, `nip_atbud`, `jabatan_atbud`, `logo`, `id_ta`) VALUES
(1, 'Pemerintahan Kabupaten Simulasi', 'Simulasi', 'Jl. Simulasi 21. Wagir - Malang', 'Hery Aristo S.kom, MM.', '43294234239423', 'kepala Simulasi', 'Nadia Soraya', '2320002321312', 'sekretaris daerah', 'Lidya Subandrio', '930221111092', 'Keuangan', 'Dinda Wulandari', '982001111110', 'Penganggaran', 'Arien Widyanti', '9340012299991', 'Verifikasi', 'Rochma Nila', '8773300110001', 'Bendahara', 'Nurfitri Indahyani', '8370002199991', 'Pembukuan', 'Vera Mayasari', '8733000228991', 'Kuasa BUD', 'Novi Dwi Pujilestari', '9821000023892', 'Atasan BUD', 'files/4acdd080082a27a47abc8a63c395ff01_61958.png', 2);

-- --------------------------------------------------------

--
-- Struktur dari tabel `jenis_dokumen`
--

CREATE TABLE IF NOT EXISTS `jenis_dokumen` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data untuk tabel `jenis_dokumen`
--

INSERT INTO `jenis_dokumen` (`id`, `nama`) VALUES
(1, 'Dokumen SPM'),
(2, 'Dokumen IMK');

-- --------------------------------------------------------

--
-- Struktur dari tabel `jenis_tim_anggaran`
--

CREATE TABLE IF NOT EXISTS `jenis_tim_anggaran` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(70) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data untuk tabel `jenis_tim_anggaran`
--

INSERT INTO `jenis_tim_anggaran` (`id`, `nama`) VALUES
(1, 'Tim Penyesuaian Berkas Baru'),
(3, 'Tim Anggaran');

-- --------------------------------------------------------

--
-- Struktur dari tabel `kegiatan`
--

CREATE TABLE IF NOT EXISTS `kegiatan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kode` varchar(30) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `id_program` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data untuk tabel `kegiatan`
--

INSERT INTO `kegiatan` (`id`, `kode`, `nama`, `id_program`) VALUES
(1, '1.01.03.10.01', 'Pelaksanaan kegiatan Pendidikan', 5),
(2, '1.01.02.03', 'program ke 2 kegiatan 3', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `korolari`
--

CREATE TABLE IF NOT EXISTS `korolari` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_rek_kor` int(11) NOT NULL,
  `id_rek_debet` int(11) NOT NULL,
  `id_rek_kredit` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data untuk tabel `korolari`
--

INSERT INTO `korolari` (`id`, `id_rek_kor`, `id_rek_debet`, `id_rek_kredit`) VALUES
(1, 0, 0, 0),
(2, 0, 1, 1),
(3, 1, 1, 2),
(4, 2, 1, 2);

-- --------------------------------------------------------

--
-- Struktur dari tabel `lra_akun`
--

CREATE TABLE IF NOT EXISTS `lra_akun` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kode` varchar(30) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `saldo_normal` varchar(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data untuk tabel `lra_akun`
--

INSERT INTO `lra_akun` (`id`, `kode`, `nama`, `saldo_normal`) VALUES
(1, '1', 'akun', 'debet');

-- --------------------------------------------------------

--
-- Struktur dari tabel `lra_jenis`
--

CREATE TABLE IF NOT EXISTS `lra_jenis` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kode` varchar(30) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `id_lra_kelompok` int(11) NOT NULL,
  `saldo_normal` varchar(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data untuk tabel `lra_jenis`
--

INSERT INTO `lra_jenis` (`id`, `kode`, `nama`, `id_lra_kelompok`, `saldo_normal`) VALUES
(1, '1.12.33', 'jenis 123tiga', 1, 'kredit');

-- --------------------------------------------------------

--
-- Struktur dari tabel `lra_kelompok`
--

CREATE TABLE IF NOT EXISTS `lra_kelompok` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kode` varchar(30) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `id_lra_akun` int(11) NOT NULL,
  `saldo_normal` varchar(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data untuk tabel `lra_kelompok`
--

INSERT INTO `lra_kelompok` (`id`, `kode`, `nama`, `id_lra_akun`, `saldo_normal`) VALUES
(1, '1.12', 'kelompok 12', 1, 'debet');

-- --------------------------------------------------------

--
-- Struktur dari tabel `lra_obyek`
--

CREATE TABLE IF NOT EXISTS `lra_obyek` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kode` varchar(30) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `id_lra_jenis` int(11) NOT NULL,
  `saldo_normal` varchar(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data untuk tabel `lra_obyek`
--

INSERT INTO `lra_obyek` (`id`, `kode`, `nama`, `id_lra_jenis`, `saldo_normal`) VALUES
(1, '1.12.33.42', 'obyek 4 2', 1, 'debet');

-- --------------------------------------------------------

--
-- Struktur dari tabel `lra_referensi`
--

CREATE TABLE IF NOT EXISTS `lra_referensi` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kode` varchar(30) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `id_lra_obyek` int(11) NOT NULL,
  `saldo_normal` varchar(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data untuk tabel `lra_referensi`
--

INSERT INTO `lra_referensi` (`id`, `kode`, `nama`, `id_lra_obyek`, `saldo_normal`) VALUES
(1, '1.12.33.42.32', 'referensi 32', 1, 'debet');

-- --------------------------------------------------------

--
-- Struktur dari tabel `perda`
--

CREATE TABLE IF NOT EXISTS `perda` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `no_urut` int(15) NOT NULL,
  `jenis` int(11) NOT NULL COMMENT '1= raperda APBD, Perda Bupati',
  `nomor` varchar(50) NOT NULL,
  `tanggal` date NOT NULL,
  `uraian` text NOT NULL,
  `id_ta` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data untuk tabel `perda`
--

INSERT INTO `perda` (`id`, `no_urut`, `jenis`, `nomor`, `tanggal`, `uraian`, `id_ta`) VALUES
(1, 1, 1, '20 mei 2001', '2014-09-01', 'tentang perda', 2),
(2, 2, 2, '9200', '2014-09-01', 'tentang bupati', 2),
(3, 3, 3, 'Penjabaran APBD', '2014-09-04', 'apbd daerah', 2);

-- --------------------------------------------------------

--
-- Struktur dari tabel `potongan`
--

CREATE TABLE IF NOT EXISTS `potongan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kode` varchar(30) NOT NULL,
  `nama` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data untuk tabel `potongan`
--

INSERT INTO `potongan` (`id`, `kode`, `nama`) VALUES
(1, 'PT001', 'Potongan Pajak'),
(2, 'PT011', 'Potongan Daerah');

-- --------------------------------------------------------

--
-- Struktur dari tabel `program`
--

CREATE TABLE IF NOT EXISTS `program` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kode` varchar(30) NOT NULL,
  `id_bidang` int(30) NOT NULL,
  `nama` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data untuk tabel `program`
--

INSERT INTO `program` (`id`, `kode`, `id_bidang`, `nama`) VALUES
(1, '1.01.02', 1, 'Program 1 ke dua');

-- --------------------------------------------------------

--
-- Struktur dari tabel `rek_spm`
--

CREATE TABLE IF NOT EXISTS `rek_spm` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_apbd_rincian` int(11) NOT NULL,
  `id_potongan` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data untuk tabel `rek_spm`
--

INSERT INTO `rek_spm` (`id`, `id_apbd_rincian`, `id_potongan`) VALUES
(7, 2, 1),
(8, 1, 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `satuan`
--

CREATE TABLE IF NOT EXISTS `satuan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(40) NOT NULL,
  `singkatan` varchar(40) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data untuk tabel `satuan`
--

INSERT INTO `satuan` (`id`, `nama`, `singkatan`) VALUES
(1, 'Kilometer', 'KM'),
(2, 'Dreaming', 'DRM');

-- --------------------------------------------------------

--
-- Struktur dari tabel `std_jenis`
--

CREATE TABLE IF NOT EXISTS `std_jenis` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kode` varchar(11) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `id_std_kelompok` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data untuk tabel `std_jenis`
--

INSERT INTO `std_jenis` (`id`, `kode`, `nama`, `id_std_kelompok`) VALUES
(1, '1.2', 'jenis dua', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `std_kelompok`
--

CREATE TABLE IF NOT EXISTS `std_kelompok` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kode` varchar(11) NOT NULL,
  `nama` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data untuk tabel `std_kelompok`
--

INSERT INTO `std_kelompok` (`id`, `kode`, `nama`) VALUES
(1, '1', 'Kelompok'),
(2, '2', 'Kelompok2');

-- --------------------------------------------------------

--
-- Struktur dari tabel `std_rincian`
--

CREATE TABLE IF NOT EXISTS `std_rincian` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kode` varchar(20) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `harga` double(14,2) NOT NULL,
  `id_satuan` int(11) NOT NULL,
  `keterangan` text NOT NULL,
  `id_std_jenis` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data untuk tabel `std_rincian`
--

INSERT INTO `std_rincian` (`id`, `kode`, `nama`, `harga`, `id_satuan`, `keterangan`, `id_std_jenis`) VALUES
(2, '1.2.3', 'Rincian 123', 2.00, 2, 'tested', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `sub_unit`
--

CREATE TABLE IF NOT EXISTS `sub_unit` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kode` varchar(30) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `id_unit` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data untuk tabel `sub_unit`
--

INSERT INTO `sub_unit` (`id`, `kode`, `nama`, `id_unit`) VALUES
(1, '1.01.02.21', 'sub unit dua ke 21', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `sumber_dana`
--

CREATE TABLE IF NOT EXISTS `sumber_dana` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kode` varchar(15) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `id_ta` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data untuk tabel `sumber_dana`
--

INSERT INTO `sumber_dana` (`id`, `kode`, `nama`, `id_ta`) VALUES
(1, '10.02', 'Dana Hibah', 2),
(2, '10.01', 'Dana BOX', 2);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tahun_anggaran`
--

CREATE TABLE IF NOT EXISTS `tahun_anggaran` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tahun` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data untuk tabel `tahun_anggaran`
--

INSERT INTO `tahun_anggaran` (`id`, `tahun`) VALUES
(2, 2014),
(3, 2015),
(4, 2013);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tim_anggaran`
--

CREATE TABLE IF NOT EXISTS `tim_anggaran` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `no_urut` int(10) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `nip` varchar(50) NOT NULL,
  `jabatan` varchar(50) NOT NULL,
  `id_jenis_tim` int(11) NOT NULL,
  `id_ta` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data untuk tabel `tim_anggaran`
--

INSERT INTO `tim_anggaran` (`id`, `no_urut`, `nama`, `nip`, `jabatan`, `id_jenis_tim`, `id_ta`) VALUES
(1, 1, 'Hery Julio Aristo', '2300001230', 'kepala dinas', 3, 2),
(2, 10, 'shdfdhs', 'sfds', 'sdfd', 1, 2),
(3, 2, 'ddaf', 'dsf', 'dfs', 1, 2);

-- --------------------------------------------------------

--
-- Struktur dari tabel `ttd_dokumen`
--

CREATE TABLE IF NOT EXISTS `ttd_dokumen` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `no_urut` int(11) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `nip` varchar(50) NOT NULL,
  `jabatan` varchar(50) NOT NULL,
  `id_jenis_dokumen` int(11) NOT NULL,
  `id_ta` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data untuk tabel `ttd_dokumen`
--

INSERT INTO `ttd_dokumen` (`id`, `no_urut`, `nama`, `nip`, `jabatan`, `id_jenis_dokumen`, `id_ta`) VALUES
(1, 1, 'Hery Julio Aristo.MM', '021399123888901', 'Directur PT.Citridia Solution Corp', 2, 2),
(2, 2, 'Nadia Soraya', '83423420121', 'hahahah', 1, 2);

-- --------------------------------------------------------

--
-- Struktur dari tabel `unit`
--

CREATE TABLE IF NOT EXISTS `unit` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kode` varchar(30) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `id_bidang` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data untuk tabel `unit`
--

INSERT INTO `unit` (`id`, `kode`, `nama`, `id_bidang`) VALUES
(1, '1.01.02', 'unit satu ke dua', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `urusan`
--

CREATE TABLE IF NOT EXISTS `urusan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kode` varchar(12) NOT NULL,
  `nama` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data untuk tabel `urusan`
--

INSERT INTO `urusan` (`id`, `kode`, `nama`) VALUES
(1, '1', 'Urusan Wajib'),
(2, '2', 'Urusan Normal 2');

-- --------------------------------------------------------

--
-- Struktur dari tabel `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(70) NOT NULL,
  `tgl_lahir` date NOT NULL,
  `kota` varchar(30) NOT NULL,
  `telepon` varchar(30) NOT NULL,
  `email` varchar(30) NOT NULL,
  `alamat` text NOT NULL,
  `username` varchar(30) NOT NULL,
  `password` varchar(50) NOT NULL,
  `id_akses` int(11) NOT NULL,
  `foto` varchar(100) NOT NULL,
  `keterangan` text NOT NULL,
  `hapus` int(11) NOT NULL,
  `ttd` text NOT NULL,
  `id_urusan` int(11) NOT NULL,
  `id_bidang` int(11) NOT NULL,
  `id_unit` int(11) NOT NULL,
  `id_sub_unit` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data untuk tabel `user`
--

INSERT INTO `user` (`id`, `nama`, `tgl_lahir`, `kota`, `telepon`, `email`, `alamat`, `username`, `password`, `id_akses`, `foto`, `keterangan`, `hapus`, `ttd`, `id_urusan`, `id_bidang`, `id_unit`, `id_sub_unit`) VALUES
(1, 'Drs. Herry Julio Aristo. S.Kom, MM, MT', '2014-08-19', 'malang', '085755598020', 'admin@gmail.com', 'ST. Giant Street 2019 B', 'admin', '*A58C4ACA961C127D47208AE3A837F00DAD02473A', 1, 'files/c19ed92a081c5f101bed6d44c0b83e5e_52203.jpg', '', 0, '', 3, 5, 4, 0),
(2, 'Danotila Pevita Arzani', '2014-08-20', '242349382', '0832432462789', '432423492348', 'keterangan', 'dina', '*95B4767A8938DF893FA7E6D61BB0ED36DDAD3F3D', 2, 'files/c19ed92a081c5f101bed6d44c0b83e5e_112732.jpg', '', 0, '', 0, 0, 0, 0),
(3, 'Nagita Soraya', '2014-08-28', 'malan', '01212121', 'speed_rcm99@yahoo.com', 'hahah', 'nata', '*2E8DA222C78F97D3F19833DEFC55D14A31E08275', 1, 'assets/images/avt.jpg', '', 0, '', 3, 5, 4, 1);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
